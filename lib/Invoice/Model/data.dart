import 'package:safalsathi/Invoice/Model/shipping_address.dart';

import 'order_date.dart';
import 'order_products.dart';

class Data {
  final int id;
  final OrderDate orderDate;
  final String orderStatus;
  final String orderNote;
  final List<OrderProducts> orderProducts;
  final ShippingAddress shippingAddress;

  Data.fromJsonMap(Map<String, dynamic> map)
      : id = map["id"],
        orderDate = OrderDate.fromJsonMap(map["order_date"]),
        orderStatus = map["order_status"],
        orderNote = map["order_note"],
        orderProducts = List<OrderProducts>.from(
            map["orderProducts"].map((it) => OrderProducts.fromJsonMap(it))),
        shippingAddress = ShippingAddress.fromJsonMap(map["shippingAddress"]);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['order_date'] = orderDate == null ? null : orderDate.toJson();
    data['order_status'] = orderStatus;
    data['order_note'] = orderNote;
    data['orderProducts'] = orderProducts != null
        ? this.orderProducts.map((v) => v.toJson()).toList()
        : null;
    data['shippingAddress'] =
        shippingAddress == null ? null : shippingAddress.toJson();
    return data;
  }

  getTotal() {
    double total = 0.0;
    for (int i = 0; i < orderProducts.length; i++) {
      total = total +
          (double.parse(orderProducts[i].pivot.price) *
              orderProducts[i].pivot.qty);
    }
    return "Rs. " + total.toString();
  }
}
