class Pivot {
  final int orderId;
  final int productId;
  final int qty;
  final String price;
  final int tax;
  final int discount;
  final int preBooking;
  final String colour;
  final String size;
  final String createdAt;
  final String updatedAt;

  Pivot.fromJsonMap(Map<String, dynamic> map)
      : orderId = map["order_id"],
        productId = map["product_id"],
        qty = map["qty"],
        price = map["price"],
        tax = map["tax"],
        discount = map["discount"],
        preBooking = map["prebooking"],
        colour = map["colour"],
        size = map["size"],
        createdAt = map["created_at"],
        updatedAt = map["updated_at"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_id'] = orderId;
    data['product_id'] = productId;
    data['qty'] = qty;
    data['price'] = price;
    data['tax'] = tax;
    data['discount'] = discount;
    data['prebooking'] = preBooking;
    data['colour'] = colour;
    data['size'] = size;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    return data;
  }

  String getPrice() {
//    int a = (qty * price) as int;
    return "Rs. $price";
  }

  String getQty() {
    return "X" + qty.toString();
  }

  getDiscount() {
//    String b = num.parse((((price as double - sale_price) / product_price) * 100)
//        .toStringAsFixed(2))
//        .toString();
    return discount == null ? "No Saving" : "$discount saved";
  }

  String getTaxRate() {
    return tax == null ? "0 %" : "$tax %";
  }

  String getTotal() {
    print(double.parse(price) * qty);
    return "Rs. ${double.parse(price) * qty}";
  }
}
