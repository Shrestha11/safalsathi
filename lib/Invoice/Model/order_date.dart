class OrderDate {
  final String date;
  final int timeZoneType;
  final String timezone;

  OrderDate.fromJsonMap(Map<String, dynamic> map)
      : date = map["date"],
        timeZoneType = map["timezone_type"],
        timezone = map["timezone"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['date'] = date;
    data['timezone_type'] = timeZoneType;
    data['timezone'] = timezone;
    return data;
  }
}
