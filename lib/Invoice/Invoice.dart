import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../app_styles.dart';

import 'Model/InvoiceResponse.dart';
import 'Model/order_products.dart';
import 'Model/shipping_address.dart';
import 'bloc/invoice_bloc.dart';
import 'bloc/invoice_event.dart';
import 'bloc/invoice_state.dart';

class Invoice extends StatefulWidget {
  final int id;

  Invoice({@required this.id});

  @override
  State<StatefulWidget> createState() {
    return InvoicesState(id);
  }
}

class InvoicesState extends State<Invoice> {
  final int id;
  final InvoiceBloc _invoiceBloc = InvoiceBloc();

  InvoicesState(this.id) {
    _invoiceBloc.add(Fetch(id));
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        body: BlocBuilder(
            bloc: _invoiceBloc,
            builder: (BuildContext context, InvoiceState _invoiceState) {
              if (_invoiceState is InitialInvoiceState) {
                return Text("Loading...");
              } else if (_invoiceState is Unauthenticated) {
                return Text("UNAUTHENTICATED");
              } else if (_invoiceState is LoadedStated) {
                return _buildInvoice(_invoiceState.invoiceResponse,context);
              } else {
                return Text("ERROR LoADING");
              }
            }),
      ),
    );
  }
}

Widget _buildInvoice(InvoiceResponse invoiceResponse,BuildContext context) {
  return Column(
    children: <Widget>[
      _productList(invoiceResponse.data.orderProducts),
      _ticket(invoiceResponse.data.id, invoiceResponse.data.orderDate.date,
          invoiceResponse.data.getTotal(),context),
      _address(invoiceResponse.data.shippingAddress),
    ],
  );
}

_address(ShippingAddress shippingAddress) {
  return Card(
    child: Row(
      children: <Widget>[
        Container(
          child: IntrinsicWidth(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Name",
                    style: greyText,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Email",
                    style: greyText,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Mobile",
                    style: greyText,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Country",
                    style: greyText,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "District",
                    style: greyText,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Area",
                    style: greyText,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Zone",
                    style: greyText,
                  ),
                ),
              ],
            ),
          ),
        ),
        Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                    "${shippingAddress.firstName} ${shippingAddress.lastName}"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("${shippingAddress.email}"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("${shippingAddress.mobile}"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("${shippingAddress.country}"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("${shippingAddress.district}"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("${shippingAddress.area}"),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text("${shippingAddress.zone}"),
              ),
            ],
          ),
        ),
      ],
    ),
  );
}

_ticket(int id, String orderDate, String total,context) {
  return Card(
    margin: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
    child: Row(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
            height: MediaQuery.of(context).size.width / 3,
            width: (2.5 * MediaQuery.of(context).size.width) / 10,
            decoration: BoxDecoration(color: Colors.redAccent),
            child: Center(
              child: Text(
                "Purchase \n Ticket",
                style: whitesmallTextStyle,
              ),
            )),
        Container(
          decoration: BoxDecoration(color: Colors.deepOrangeAccent),
          height: MediaQuery.of(context).size.width/ 3,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceAround,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Purchase Id:",
                  style: whitesmallTextStyle,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      "Date:",
                      style: whitesmallTextStyle,
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    Text(
                      "Total:",
                      style: whitesmallTextStyle,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: Container(
            decoration: BoxDecoration(color: Colors.deepOrangeAccent),
            height: MediaQuery.of(context).size.width / 3,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    id.toString(),
                    style: whitesmallTextStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(8.0, 8.0, 0.0, 8.0),
                  child: Text(
                    orderDate,
                    maxLines: 1,
                    style: whitesmallTextStyle,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    total,
                    style: whitesmallTextStyle,
                  ),
                ),
              ],
            ),
          ),
        ),
      ],
    ),
  );
}

_productList(List<OrderProducts> orderProducts) {
  return ListView.builder(
      physics: const NeverScrollableScrollPhysics(),
      shrinkWrap: true,
      itemCount: orderProducts.length,
      itemBuilder: (context, position) {
        return Card(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  orderProducts[position].name,
                  style: greyText,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  children: <Widget>[
                    orderProducts[position].pivot.colour == null
                        ? Container()
                        : Text(orderProducts[position].pivot.colour),
                    orderProducts[position].pivot.size == null
                        ? Container()
                        : Text(orderProducts[position].pivot.size),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      orderProducts[position].pivot.getPrice(),
                      style: largeTextStyle,
                    ),
                    Padding(
                      padding: const EdgeInsets.all(2.0),
                      child: Text(
                        orderProducts[position].pivot.getQty(),
                        style: smallTextStyle,
                      ),
                    )
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.panorama_fish_eye,
                          color: Colors.orangeAccent.shade700,
                          size: 13,
                        ),
                        Text(
                          "discount",
                          style: greyText,
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.panorama_fish_eye,
                          color: Colors.blue.shade500,
                          size: 13,
                        ),
                        Text(
                          "taxRate",
                          style: greyText,
                        ),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Icon(
                          Icons.panorama_fish_eye,
                          color: Colors.deepPurple.shade500,
                          size: 13,
                        ),
                        Text(
                          "Total",
                          style: greyText,
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(orderProducts[position].pivot.getDiscount(),
                        style: blackText),
                    Text(
                      orderProducts[position].pivot.getTaxRate(),
                      style: blackText,
                    ),
                    Text(
                      orderProducts[position].pivot.getTotal(),
                      style: blackText,
                    ),
                  ],
                ),
              ),
            ],
          ),
        );
      });
}
