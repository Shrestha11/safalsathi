import 'package:safalsathi/Invoice/Model/InvoiceResponse.dart';
import 'package:meta/meta.dart';

@immutable
abstract class InvoiceState {}

class InitialInvoiceState extends InvoiceState {}

class ErrorState extends InvoiceState {}

class LoadedStated extends InvoiceState {
  final InvoiceResponse invoiceResponse;

  LoadedStated(this.invoiceResponse);

  @override
  String toString() => 'loaded';
}

class Unauthenticated extends InvoiceState {}
