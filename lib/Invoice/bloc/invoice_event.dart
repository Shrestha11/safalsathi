import 'package:meta/meta.dart';

@immutable
abstract class InvoiceEvent {}

class Fetch extends InvoiceEvent {
 final int id;

  Fetch(this.id);

  @override
  String toString() => 'Fetch State';
}
