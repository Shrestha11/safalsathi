import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/Invoice/Model/InvoiceResponse.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';

import 'bloc.dart';
import 'package:safalsathi/common/DioBase.dart';

class InvoiceBloc extends Bloc<InvoiceEvent, InvoiceState> {
  SharedPreferences sharedPreferences;

  @override
  InvoiceState get initialState => InitialInvoiceState();

  @override
  Stream<InvoiceState> mapEventToState(
    InvoiceEvent event,
  ) async* {
    InvoiceResponse _invoiceResponse;
    loadSharedData();
    try {
      if (event is Fetch) {
        if (initialState is InitialInvoiceState) {
          _invoiceResponse = await _fetchDetail(event.id);
          yield LoadedStated(_invoiceResponse);
        } else if (initialState is LoadedStated) {
          yield (_invoiceResponse.data != null
              ? LoadedStated(_invoiceResponse)
              : ErrorState());
        } else
          yield ErrorState();
      }
    } catch (_) {
      yield ErrorState();
    }
  }

  void loadSharedData() {
    SharedPreferences.getInstance().then((SharedPreferences sp) {
      sharedPreferences = sp;
      var _testValue = sharedPreferences.getString("token");
    });
  }
}

Future<InvoiceResponse> _fetchDetail(int id) async {
  final String _endpoint = "/my-account/order/";
  try {
    Response response = await dioBASE(await getAuth()).get("$_endpoint/$id");
    return InvoiceResponse.fromJsonMap(response.data);
  } catch (error) {
    return null;
  }
}
