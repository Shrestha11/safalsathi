import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/UserAddress/model/data.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/common_ui.dart';

import '../app_styles.dart';
import 'bloc/address_bloc.dart';
import 'bloc/address_event.dart';
import 'bloc/address_state.dart';
import 'model/myAddressResponse.dart';

class Address extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AddressState();
  }
}

class _AddressState extends State<Address> {
  AddressBloc _addressBloc = AddressBloc();
  MyAddressResponse _myAddressResponse;
  bool isError = false;
  String errorMsg = "";

  @override
  void initState() {
    super.initState();
    _addressBloc.add(FetchAddress());
  }

  @override
  Widget build(BuildContext context) {
    //todo
    // ignore: unnecessary_statements
    isError ? buildSnackError(context, errorMsg) : null;
    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Address"),
        ),
        body: Column(
          children: <Widget>[
            _addAddressView(),
            _dataList(),
          ],
        ),
      ),
    );
  }

  _addAddressView() {
    return BlocBuilder(
        bloc: _addressBloc,
        builder: (BuildContext context, AddressState _addressState) {
          if (_addressState is Loading) {
            return Text(
              "Loading",
              style: greyText,
            );
          }
          return InkWell(
            onTap: () {
              WidgetsBinding.instance.addPostFrameCallback((_) {
                showDialog(
                    context: context,
                    builder: (_) {
                      return MyDialog(_addressBloc);
                    });
              });
            },
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Container(
                width: MediaQuery.of(context).size.width,
                child: Center(
                  child: Text(
                    "Add new address",
                    style: redText,
                  ),
                ),
              ),
            ),
          );
        });
  }

//
//  void _showDialog(BuildContext context) {
//    showDialog(
//      context: context,
//      builder: (BuildContext context) {
//        return
//      },
//    );
//  }

  _dataList() {
    return Expanded(
      child: BlocBuilder(
          bloc: _addressBloc,
          builder: (BuildContext context, AddressState _addressState) {
            print("AddressPage CurrentState : ${_addressState.toString()}");
            if (_addressState is Loading) {
              return buildLoadingWidget(context);
            }
            if (_addressState is Loaded) {
              _myAddressResponse = _addressState.myAddressResponse;
            }
            if (_addressState is SuccessState) {
              WidgetsBinding.instance.addPostFrameCallback(
                  (_) => buildSnackSuccess(context, _addressState.message));
              _addressBloc.add(FetchAddress());
              return addressWidget(_myAddressResponse.data);
            }
            if (_addressState is ErrorState) {
              errorMsg = _addressState.error;
              WidgetsBinding.instance.addPostFrameCallback(
                  (_) => buildSnackError(context, errorMsg));
              errorMsg = "";
              isError = false;
              return addressWidget(_myAddressResponse.data);
            }
            return addressWidget(_myAddressResponse.data);
          }),
    );
  }

  addressWidget(List<Data> dataList) {
    return ListView.builder(
      shrinkWrap: true,
      itemCount: dataList.length,
      itemBuilder: (context, position) {
        return Card(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(dataList[position].firstName),
                    Text(dataList[position].lastName),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  color: Colors.deepPurple,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(dataList[position].country == null
                        ? ""
                        : dataList[position].country),
                    Text(dataList[position].zone),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(dataList[position].district),
                    Text(dataList[position].area),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: MediaQuery.of(context).size.width,
                  height: 1.0,
                  color: Colors.deepPurple,
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(dataList[position].email),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(child: Text(dataList[position].mobile)),
//                    InkWell(
//                      onTap: () => _addressBloc.edit(dataList[position].id),
//                      child: Padding(
//                        padding: const EdgeInsets.all(8.0),
//                        child: Icon(
//                          Icons.mode_edit,
//                          color: Colors.deepPurple,
//                          size: 20.0,
//                        ),
//                      ),
//                    ),
                    InkWell(
                      onTap: () {
                        _addressBloc.add(DeleteAddress(dataList[position].id));
                      },
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Icon(
                          Icons.delete,
                          color: Colors.deepPurple,
                          size: 20.0,
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ],
          ),
        );
      },
    );
  }
}

class MyDialog extends StatefulWidget {
  final AddressBloc addressBloc;

  MyDialog(this.addressBloc);

  @override
  _MyDialogState createState() => new _MyDialogState(addressBloc);
}

class _MyDialogState extends State<MyDialog> {
  AddressBloc _addressBloc;
  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _emailnameController = TextEditingController();
  final _zoneController = TextEditingController();
  final _districtController = TextEditingController();
  final _areaController = TextEditingController();
  final _phoneController = TextEditingController();
  final _countryController = TextEditingController(text: "Nepal");
  static final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

//  final _emailValidator = GlobalKey<FormState>();
  String locationType = "Home";

  _MyDialogState(this._addressBloc);

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      title: Center(
        child: new Text(
          "Address Form.",
          style: highligttedTextStyle,
        ),
      ),
      children: <Widget>[
        Container(
          child: Form(
            key: _formKey,
            child: Column(
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 2.0, 8.0, 0.0),
                  child: TextFormField(
                    controller: _firstNameController,
                    decoration: new InputDecoration(hintText: 'First Name'),
                    validator: (String firstName) {
                      return validateFirstName(firstName);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 2.0, 8.0, 0.0),
                  child: TextFormField(
                    controller: _lastNameController,
                    decoration: new InputDecoration(hintText: 'Lasts Name'),
                    validator: (String lastName) {
                      return validateLastName(lastName);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 2.0, 8.0, 0.0),
                  child: TextFormField(
                    controller: _emailnameController,
                    decoration: new InputDecoration(hintText: 'Email'),
                    validator: (String email) {
                      return validateEmail(email);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 2.0, 8.0, 0.0),
                  child: TextFormField(
                    controller: _zoneController,
                    decoration: new InputDecoration(hintText: 'Zone'),
                    validator: (String zone) {
                      return validateZone(zone);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 2.0, 8.0, 0.0),
                  child: TextFormField(
                    controller: _districtController,
                    decoration: new InputDecoration(hintText: 'District'),
                    validator: (String district) {
                      return validateDistrict(district);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 2.0, 8.0, 0.0),
                  child: TextFormField(
                    controller: _areaController,
                    decoration: new InputDecoration(hintText: 'Area'),
                    validator: (String area) {
                      return validateArea(area);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 2.0, 8.0, 0.0),
                  child: TextFormField(
                    controller: _phoneController,
                    keyboardType:
                        TextInputType.numberWithOptions(decimal: true),
                    decoration: new InputDecoration(hintText: 'Phone'),
                    validator: (String phone) {
                      return validatePhone(phone);
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.fromLTRB(8.0, 2.0, 8.0, 0.0),
                  child: TextFormField(
                    controller: _countryController,
                    decoration: new InputDecoration(hintText: 'Phone'),
                    enabled: false,
                  ),
                ),
                BlocBuilder(
                    bloc: _addressBloc,
                    builder:
                        (BuildContext context, AddressState _addressState) {
                      if (_addressState is DialogDropDownUpdated) {
                        locationType =
                            _addressState.locationType == 0 ? "Home" : "Office";
                      }
                      return Container(
                        width: MediaQuery.of(context).size.width,
                        child: ButtonTheme(
                          alignedDropdown: true,
                          child: DropdownButton<String>(
                            iconSize: 18.0,
                            iconEnabledColor: Colors.deepPurple,
                            value: locationType,
                            onChanged: (String newValue) {
                              _addressBloc.setDropDown(newValue);
                            },
                            items: <String>['Home', 'Office']
                                .map<DropdownMenuItem<String>>((String value) {
                              return DropdownMenuItem<String>(
                                value: value,
                                child: Text(value),
                              );
                            }).toList(),
                          ),
                        ),
                      );
                    }),
                RaisedButton(
                  onPressed: () {
                    if (_formKey.currentState.validate()) {
                      Navigator.pop(context);

                      _addressBloc.add(Submit(
                          _firstNameController.text,
                          _lastNameController.text,
                          _emailnameController.text,
                          _zoneController.text,
                          _districtController.text,
                          _areaController.text,
                          _phoneController.text,
                          _countryController.text));
                    }
                  },
                  child: Text("Submit"),
                )
              ],
            ),
          ),
        ),
      ],
    );
  }
}
