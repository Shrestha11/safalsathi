import 'data.dart';

class MyAddressResponse {
  final List<Data> data;

  MyAddressResponse.fromJsonMap(Map<String, dynamic> map)
      : data = List<Data>.from(map["data"].map((it) => Data.fromJsonMap(it)));

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> datas = new Map<String, dynamic>();
    datas['data'] =
        data != null ? this.data.map((v) => v.toJson()).toList() : null;
    return datas;
  }
}
