
class Data {

  final int id;
  final int userId;
  final String firstName;
  final String lastName;
  final String email;
  final String country;
  final String district;
  final String area;
  final String zone;
  final int isDefault;
  final String locationType;
  final String mobile;
  final String phone;
  final String createdAt;
  final String updatedAt;

	Data.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		userId = map["user_id"],
		firstName = map["first_name"],
		lastName = map["last_name"],
		email = map["email"],
		country = map["country"],
		district = map["district"],
		area = map["area"],
		zone = map["zone"],
		isDefault = map["is_default"],
		locationType = map["location_type"],
		mobile = map["mobile"],
		phone = map["phone"],
		createdAt = map["created_at"],
		updatedAt = map["updated_at"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['user_id'] = userId;
		data['first_name'] = firstName;
		data['last_name'] = lastName;
		data['email'] = email;
		data['country'] = country;
		data['district'] = district;
		data['area'] = area;
		data['zone'] = zone;
		data['is_default'] = isDefault;
		data['location_type'] = locationType;
		data['mobile'] = mobile;
		data['phone'] = phone;
		data['created_at'] = createdAt;
		data['updated_at'] = updatedAt;
		return data;
	}
}
