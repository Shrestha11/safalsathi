import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/UserAddress/model/deleteResponse/deleteAddressResponse.dart';
import 'package:safalsathi/UserAddress/model/myAddressResponse.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/common_ui.dart';

import './bloc.dart';

class AddressBloc extends Bloc<AddressEvent, AddressState> {
  int responseCode;
  MyAddressResponse _myAddressResponse;
  AddressResponse _addressResponse;
  bool error = false;
  var errorMessage = "";

  // ignore: unused_field
  var _response;
  int locationType = 0;

  @override
  AddressState get initialState => InitialAddressState();

  @override
  Stream<AddressState> mapEventToState(
    AddressEvent event,
  ) async* {
    if (event is FetchAddress) {
      yield Loading();
      _myAddressResponse = await getUserAddress();
      yield Loaded(_myAddressResponse);
    }
    if (event is DeleteAddress) {
      _response = await delete(event.id).catchError((e) {
        error = true;
        errorMessage = e.toString();
      });
      if (error) {
        yield ErrorState(errorMessage);
        error = !error;
      } else
        add(FetchAddress());
    }
    if (event is AddAddress) {
      yield DialogAddAddress();
      add(FetchAddress());
    }
    if (event is DialogDropDownChange) {
      yield DialogDropDownUpdated(event.location);
    }
    if (event is Submit) {
      _addressResponse = await addNewAddress(
              event.firstName,
              event.lastName,
              event.email,
              event.area,
              event.district,
              event.zone,
              event.phone,
              locationType,
              event.country)
          .catchError((error) {
        this.error = true;
        errorMessage = error.toString();
      });
      if (error) {
        yield ErrorState(errorMessage);
        error = !error;
      } else
        yield SuccessState(_addressResponse.msg);
    }
  }

  Future<MyAddressResponse> getUserAddress() async {
    Response response = await dioBASE(await getAuth()).get("/my-account");
    return MyAddressResponse.fromJsonMap(response.data);
  }

  edit(int id) {}

  Future<AddressResponse> addNewAddress(
      String firstName,
      String lastName,
      String email,
      String area,
      String district,
      String zone,
      String phone,
      int locationType,
      String country) async {
    try {
      Response response = await dioBASE(await getAuth()).post(
        "/my-account/add-shipping-address",
        data: {
          "first_name": firstName,
          "last_name": lastName,
          "email": email,
          "area": area,
          "district": district,
          "zone": zone,
          "mobile": phone,
          "location_type": locationType,
          "country": country,
        },
      );
      return AddressResponse.fromJsonMap(response.data);
    } on DioError catch (e) {
      print("Response Code ${e.response.statusCode}");
      if (e.response.statusCode == 406) {
        return Future.error("Error:406");
      }
      return Future.error("ERROR CODE: ${e.response.statusCode}");
    } catch (e) {
      return Future.error("ERROR CODE: ${e.toString()}");
    }
  }

  Future<AddressResponse> delete(int id) async {
    try {
      Response response = await dioBASE(await getAuth())
          .delete("/my-account/delete-shipping-address/$id");
      if (response.statusCode != 200) {
        responseCode = response.statusCode;
      }
      return AddressResponse.fromJsonMap(response.data);
    } on DioError catch (e) {
      if (e.response.statusCode == 405) {
//        return Future.error(ErrorModel.fromJsonMap(e.response.data).error);
      } else
        return Future.error("ERROR CODE: ${e.response.statusCode}");
    }
  }

  addAddress() {}

  void setLocationType(String s) {}

  void setDropDown(String newValue) {
    if (newValue == 'Home') {
      locationType = 0;
      add(DialogDropDownChange(0));
    } else {
      locationType = 1;
      add(DialogDropDownChange(1));
    }
  }
}
