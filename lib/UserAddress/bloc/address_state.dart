import 'package:meta/meta.dart';
import 'package:safalsathi/UserAddress/model/myAddressResponse.dart';

@immutable
abstract class AddressState {}

class InitialAddressState extends AddressState {}

class Loading extends AddressState {
  @override
  String toString() => "Loading Address";
}

class ErrorState extends AddressState {
  final String error;

  ErrorState(this.error);

  @override
  String toString() => 'Error';
}

class Loaded extends AddressState {
  final MyAddressResponse myAddressResponse;

  Loaded(this.myAddressResponse);

  @override
  String toString() => 'Loaded Addresss';
}

class DialogAddAddress extends AddressState {
  @override
  String toString() => 'DialogAddAddress';
}

class DialogDropDownUpdated extends AddressState {
  final int locationType;

  DialogDropDownUpdated(this.locationType);

  @override
  String toString() => 'DialogDropDownUpdated';
}

class SuccessState extends AddressState {
  final String message;

  SuccessState(this.message);

  @override
  String toString() => 'Success State';
}
