import 'package:meta/meta.dart';

@immutable
abstract class AddressEvent {}

class FetchAddress extends AddressEvent {
  @override
  String toString() => "Address Fetch triggered";
}

class DeleteAddress extends AddressEvent {
  final int id;

  DeleteAddress(this.id);

  @override
  String toString() => 'Delete Address';
}

class AddAddress extends AddressEvent {
  @override
  String toString() => 'Add Address';
}

class ClickAddressEvent extends AddressEvent {
  @override
  String toString() => "One address is clicked";
}

class ClickAddressEvent2 extends AddressEvent {
  @override
  String toString() => "Addresss Clicked";
}

class DialogDropDownChange extends AddressEvent {
  final int location;

  DialogDropDownChange(this.location);

  @override
  String toString() => 'DialogDropDownUpdated';
}

class Submit extends AddressEvent {
  final String firstName, lastName, email, zone, district, area, phone, country;

  Submit(
    this.firstName,
    this.lastName,
    this.email,
    this.zone,
    this.district,
    this.area,
    this.phone,
    this.country,
  );

  @override
  String toString() => 'ValidateForm';
}
