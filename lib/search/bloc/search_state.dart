import 'package:meta/meta.dart';

@immutable
abstract class SearchState {}

class InitialSearchState extends SearchState {}

class LoadingState extends SearchState {}

class LoadedState extends SearchState {
  final List<String> listOfProducts;

  LoadedState(this.listOfProducts);
}
