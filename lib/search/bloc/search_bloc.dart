import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:shared_preferences/shared_preferences.dart';

import './bloc.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  @override
  SearchState get initialState => InitialSearchState();

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    List<String> listOfProducts;
    if (event is LoadSearchEvent) {
      yield LoadingState();
      try {
        listOfProducts = await prefs.get('mylist');
      } catch (e) {
        print("ERROR $e");
      }
      yield LoadedState(listOfProducts);
    }
  }
}
