import 'package:meta/meta.dart';

@immutable
abstract class SearchEvent {}

class LoadSearchEvent extends SearchEvent {}
