import 'package:autocomplete_textfield/autocomplete_textfield.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/common/color.dart';
import 'package:safalsathi/searchProducts/SearchPage.dart';

import 'bloc/search_bloc.dart';
import 'bloc/search_event.dart';
import 'bloc/search_state.dart';

class SearchPageStart extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return SearchPageStartState();
  }
}

class SearchPageStartState extends State<SearchPageStart> {
  SearchBloc _searchBloc;

  GlobalKey<AutoCompleteTextFieldState<String>> key = new GlobalKey();
  List<String> suggestions = [];
  String currentText = "";
  TextEditingController _textEditingController = TextEditingController();
  FocusNode _focusNode;

  @override
  void initState() {
    super.initState();
    _searchBloc = SearchBloc();
    _searchBloc.add(LoadSearchEvent());
    _textEditingController.addListener(
      () {
        currentText = _textEditingController.text;
      },
    );
    _focusNode = FocusNode();
    WidgetsBinding.instance
        .addPostFrameCallback((_) => _focusNode.requestFocus());
  }

  @override
  void dispose() {
    super.dispose();
    _focusNode.dispose();
    _searchBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
        bloc: _searchBloc,
        listener: (context, state) {
          if (state is LoadedState) {
            suggestions = state.listOfProducts;
          }
        },
        child: BlocBuilder(
          bloc: _searchBloc,
          builder: (context, state) {
            if (state is LoadingState) {
              return Center(child: Text("Loading"));
            }
            if (state is LoadedState) {
              SimpleAutoCompleteTextField textField =
                  SimpleAutoCompleteTextField(
                key: key,
                focusNode: _focusNode,
                controller: _textEditingController,
                suggestions: suggestions,
                style: Theme.of(context)
                    .textTheme
                    .body1
                    .copyWith(color: AppColors.BLACk),
//                style: TextStyle(color: AppColors.BLACk),
                submitOnSuggestionTap: true,
                textSubmitted: (text) {
                  _textEditingController.text = text;
                  return Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => SearchItem(text)));
                },
              );

//              SchedulerBinding.instance
//                  .addPostFrameCallback((_) => _focusNode.requestFocus());
              return Scaffold(
                backgroundColor: Colors.deepOrange,
                body: SafeArea(
                  child: Column(
                    children: <Widget>[
                      Card(
                        margin: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 8.0),
                        color: Colors.deepOrange,
                        child: Row(
                          children: <Widget>[
                            Expanded(
                              child: Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: ListTile(
                                    title: Container(
                                      child: Padding(
                                        padding: const EdgeInsets.fromLTRB(
                                            8.0, 0.0, 8.0, 0.0),
                                        child: textField,
                                      ),
                                      decoration: BoxDecoration(
                                          borderRadius: new BorderRadius.only(
                                              topLeft: new Radius.circular(15),
                                              topRight: new Radius.circular(15),
                                              bottomLeft:
                                                  new Radius.circular(15),
                                              bottomRight:
                                                  new Radius.circular(15)),
                                          color: Colors.white),
//                                      color: AppColors.WHITE,
                                    ),
                                  )),
                            ),
                            InkWell(
                              onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => SearchItem(currentText),
                                ),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Search",
                                  style: TextStyle(
                                    color: AppColors.WHITE,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                      Expanded(
                          child: Container(
                        color: AppColors.WHITE,
                      )),
                    ],
                  ),
                ),
              );
              _focusNode.requestFocus(FocusNode());
//                  return new ListTile(
//                    title: textField,
//                  );
            }
            return Container();
          },
        ));
  }
}
