import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/AccountPage/accountSettingInnerPage/model/UserImageResponse.dart';

import 'bloc/account_setting_bloc.dart';
import 'bloc/account_setting_event.dart';
import 'bloc/account_setting_state.dart';

class AccountSetting extends StatefulWidget {
  final UserImageResponse userImageResponse;

  AccountSetting(this.userImageResponse);

  @override
  State<StatefulWidget> createState() {
    return _AccountSettingState(userImageResponse);
  }
}

class _AccountSettingState extends State<AccountSetting> {
  AccountSettingBloc _accountSettingBloc = AccountSettingBloc();
  UserImageResponse userImageResponse;

  TextEditingController _firstNameEdit,
      _lastNameEdit,
      _mobileNumberEdit,
      _userNameEdit,
      _emailAddressEdit,
      _dateOfBirthEdit;
  File _userImage;
  String dropdownValue = 'Male';
  String dobString;

  _AccountSettingState(this.userImageResponse) {
    _firstNameEdit = TextEditingController(text: userImageResponse.firstName);
    _lastNameEdit = TextEditingController(text: userImageResponse.lastName);
    _mobileNumberEdit = TextEditingController(text: userImageResponse.phone);
    _userNameEdit = TextEditingController(text: userImageResponse.userName);
    _emailAddressEdit = TextEditingController(text: userImageResponse.email);
    _dateOfBirthEdit = TextEditingController(text: userImageResponse.dob);
    dobString = userImageResponse.dob;
  }

  @override
  void initState() {
    super.initState();
    _accountSettingBloc.add(FetchUserDetail());
  }

  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
        context: context,
        initialDate: DateTime.now(),
        firstDate: DateTime(1900, 8),
        lastDate: DateTime.now());
    if (picked != null) {
      dobString = "${picked.year}-${picked.month}-${picked.day}";
      _dateOfBirthEdit.text = dobString;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("Account Setting"),
        ),
        body: ListView(children: <Widget>[
          BlocBuilder(
            bloc: _accountSettingBloc,
            builder: (BuildContext context,
                AccountSettingState _accountSettingState) {
              if (_accountSettingState is ImageLoadedState) {
                _userImage = _accountSettingState.image;
              }
              if (_accountSettingState is DropDownUpdated) {
                dropdownValue =
                    _accountSettingState.gender == 0 ? 'Male' : 'Female';
              }
              return InkWell(
                  onTap: () => _accountSettingBloc.add(FetchGallery()),
                  child: Container(
                    width: MediaQuery.of(context).size.width / 2,
                    height: MediaQuery.of(context).size.width / 2,
                    child: _userImage == null
                        ? Container(
                            width: MediaQuery.of(context).size.width / 2,
                            height: MediaQuery.of(context).size.width / 2,
                            child: Image.asset(
                              'images/account.png',
                              fit: BoxFit.fitWidth,
                            ),
                          )
                        : Container(
                            width: MediaQuery.of(context).size.width / 2,
                            height: MediaQuery.of(context).size.width / 2,
                            decoration: new BoxDecoration(
                              shape: BoxShape.circle,
                              image: new DecorationImage(
                                fit: BoxFit.contain,
                                image: new FileImage(_userImage),
                              ),
                            ),
                          ),
                  ));
            },
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      controller: _firstNameEdit,
                    ),
                  ),
                ),
                Expanded(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: TextField(
                      controller: _lastNameEdit,
                    ),
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: _mobileNumberEdit,
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(controller: _userNameEdit),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: TextField(
              controller: _emailAddressEdit,
            ),
          ),
          Row(
            children: <Widget>[
              Expanded(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: TextField(
                    enabled: false,
                    controller: _dateOfBirthEdit,
                  ),
                ),
              ),
              RaisedButton.icon(
                icon: Icon(Icons.calendar_today),
                onPressed: () => _selectDate(context),
                label: Text("update"),
              ),
            ],
          ),
          BlocBuilder(
              bloc: _accountSettingBloc,
              builder: (BuildContext context,
                  AccountSettingState _accountSettingState) {
                _accountSettingBloc.setGender(userImageResponse.gender);

                if (_accountSettingState is DropDownUpdated) {
                  dropdownValue =
                      _accountSettingState.gender == 0 ? 'Male' : 'Female';
                }
                return Container(
                  width: MediaQuery.of(context).size.width,
                  child: ButtonTheme(
                    alignedDropdown: true,
                    child: DropdownButton<String>(
                      iconSize: 0.0,
                      value: dropdownValue,
                      onChanged: (String newValue) {
                        _accountSettingBloc.setDropDown(newValue);
                      },
                      items: <String>['Male', 'Female']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                  ),
                );
              }),
          RaisedButton(
            child: Text("Submit Changes"),
            onPressed: () => _accountSettingBloc.add(
              UpdatingUserDetail(
                  _firstNameEdit.text,
                  _lastNameEdit.text,
                  _mobileNumberEdit.text,
                  _userNameEdit.text,
                  _emailAddressEdit.text,
                  _dateOfBirthEdit.text),
            ),
          )
        ]));
  }
}
