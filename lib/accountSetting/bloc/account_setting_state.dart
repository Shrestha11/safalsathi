import 'dart:io';

import 'package:meta/meta.dart';

@immutable
abstract class AccountSettingState {}

class InitialAccountSettingState extends AccountSettingState {}

class LoadingState extends AccountSettingState {
  @override
  String toString() => 'LoadingState';
}

class LoadedState extends AccountSettingState {
  @override
  String toString() => 'LoadedState';
}

class ImageLoadedState extends AccountSettingState {
  final File image;

  ImageLoadedState(this.image) {
    print("IMAGE LOADED STATE");
  }

  @override
  String toString() => 'ImageLoadedState';
}

class ErrorMessage extends AccountSettingState {
  final String string;

  ErrorMessage(this.string);
}

class DropDownUpdated extends AccountSettingState {
  final int gender;

  DropDownUpdated(this.gender);

  @override
  String toString() => 'DropDownUpdated';
}
