import 'dart:async';
import 'dart:io';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:image_picker/image_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:safalsathi/accountSetting/model/accountUpdateResponse.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/common_ui.dart';

import './bloc.dart';

class AccountSettingBloc
    extends Bloc<AccountSettingEvent, AccountSettingState> {
  AccountUpdateResponse accountUpdateResponse,
      accountUpdateResponse2,
      accountUpdateResponse3;
  File userImageLoaded;
  bool imageLoaded = false;
  String dob;
  int gender;

  @override
  AccountSettingState get initialState => InitialAccountSettingState();

  @override
  Stream<AccountSettingState> mapEventToState(
    AccountSettingEvent event,
  ) async* {
    if (event is FetchUserDetail) {
      yield LoadedState();
    }
    if (event is UpdatingUserDetail) {
      accountUpdateResponse = await updateAccount(
          event.firstName,
          event.lastName,
          event.mobileNumber,
          event.emailAddress,
          event.userName);
      if (imageLoaded) {
        accountUpdateResponse2 = await updateImage(userImageLoaded);
      }
      dob = event.dateOfBirth;
      accountUpdateResponse3 = await updatePersonalInfo();
    }
    if (event is FetchGallery) {
      try {
        userImageLoaded = await pickImageFromGallery();
      } catch (e) {
        ErrorMessage(e.toString());
      }
      imageLoaded = true;
      yield ImageLoadedState(userImageLoaded);
    }
    if (event is DropDownChange) {
      yield DropDownUpdated(event.value);
    }
  }

  pickImageFromGallery() async {
    var permissionStat = await PermissionHandler()
        .checkPermissionStatus(PermissionGroup.storage);
    try {
      if (permissionStat != PermissionStatus.granted) {
        Map<PermissionGroup, PermissionStatus> permissions =
            await PermissionHandler()
                .requestPermissions([PermissionGroup.storage]);
        print("Permission String $permissions");
        return await ImagePicker.pickImage(source: ImageSource.gallery);
      }
    } catch (e) {
      print("ERROR $e");
      throw "No Permission";
    }
  }

  Future<AccountUpdateResponse> updateAccount(String firstName, String lastName,
      String phone, String email, String userName) async {
    try {
      Response response = await dioBASE(await getAuth())
          .post("/my-account/edit-account", data: {
        "first_name": firstName,
        "last_name": lastName,
        "phone": phone,
        "email": email,
        "user_name": userName
      });
      return AccountUpdateResponse.fromJsonMap(response.data);
    } catch (error) {
      return null;
    }
  }

  Future<AccountUpdateResponse> updateImage(File userImageLoaded) async {
//    FormData formData = new FormData({
//      "user_image": new UploadFileInfo(
//          new File(userImageLoaded.path), userImageLoaded.toString())
//    });
    FormData formData = new FormData.fromMap({
      "user_image": await MultipartFile.fromFile(userImageLoaded.path,
          filename: userImageLoaded.toString())
    });
    try {
      Response response =
          await dioBASE(await getAuth()).post("/user-image", data: formData);
      print("Response");
      return AccountUpdateResponse.fromJsonMap(response.data);
    } catch (error) {
      return null;
    }
  }

  void setDropDown(String newValue) {
    if (newValue == 'Male') {
      gender = 0;
      add(DropDownChange(0));
    } else {
      gender = 1;
      add(DropDownChange(1));
    }
  }

  Future<AccountUpdateResponse> updatePersonalInfo() async {
    try {
      Response response = await dioBASE(await getAuth())
          .post("/my-account/edit-personal-info", data: {
        "gender": gender,
        "dob": dob,
      });
      return AccountUpdateResponse.fromJsonMap(response.data);
    } catch (error) {
      return null;
    }
  }

  void setGender(int gender) {
    this.gender = gender;
  }
}
