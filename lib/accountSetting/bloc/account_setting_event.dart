import 'package:meta/meta.dart';

@immutable
abstract class AccountSettingEvent {}

class FetchUserDetail extends AccountSettingEvent {
  @override
  String toString() => 'FetchUserDetail';
}

class FetchGallery extends AccountSettingEvent {
  @override
  String toString() => 'FetchGallery';
}

class UpdatingUserDetail extends AccountSettingEvent {
  final String firstName;
  final String lastName;
  final String mobileNumber;
  final String userName;
  final String emailAddress;
  final String dateOfBirth;

  UpdatingUserDetail(this.firstName, this.lastName, this.mobileNumber,
      this.userName, this.emailAddress, this.dateOfBirth);

  @override
  String toString() => 'UpdatingUserDetail';
}

class DropDownChange extends AccountSettingEvent {
  final int value;

  DropDownChange(this.value);

  @override
  String toString() => 'DropDownUpdated';
}
