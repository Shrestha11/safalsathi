
class AccountUpdateResponse {

  final String msg;

	AccountUpdateResponse.fromJsonMap(Map<String, dynamic> map): 
		msg = map["msg"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['msg'] = msg;
		return data;
	}
}
