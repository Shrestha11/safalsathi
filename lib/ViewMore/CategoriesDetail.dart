import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/ViewMore/bloc/bloc.dart';
import 'package:safalsathi/common/model/products.dart';
import 'package:safalsathi/product_detail/productDetail.dart';
import 'package:safalsathi/searchProducts/Model/SearchResponse.dart';

import 'FilterDialog.dart';

class CategoriesPage extends StatelessWidget {
  final int productId;
  final String title;

  CategoriesPage(this.productId, this.title);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => CategoriesBloc(),
      child: Scaffold(
          appBar: AppBar(
            title: Text(title),
          ),
          body: CategoriesListing(productId)),
    );
  }
}

class CategoriesListing extends StatefulWidget {
  final int productId;

  CategoriesListing(this.productId);

  @override
  State<StatefulWidget> createState() {
    return CategoriesListingState();
  }
}

class CategoriesListingState extends State<CategoriesListing> {
  SearchResponse _esarchResponse;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    BlocProvider.of<CategoriesBloc>(context)
        .add(LoadSearchEvent(widget.productId));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: BlocProvider.of<CategoriesBloc>(context),
        builder: (context, state) {
          return BlocListener(
            bloc: BlocProvider.of<CategoriesBloc>(context),
            listener: (context, state) {
              if (state is SortDialogDisplayState) {
                showSortDialog(context);
              }
              if (state is FilterDialogDisplayState) {
                showModalBottomSheet(
                    shape: OutlineInputBorder(
                        borderRadius: BorderRadius.all(Radius.circular(4.0))),
                    isScrollControlled: false,
                    context: context,
                    builder: (BuildContext bc) {
                      // ignore: close_sinks

                      return BlocProvider.value(
                          value: BlocProvider.of<CategoriesBloc>(context),
                          child: DynamicDialog());
                    });
              }
            },
            child: BlocBuilder(
              bloc: BlocProvider.of<CategoriesBloc>(context),
              builder: (context, state) {
                if (state is LoadingSearch) {
                  return Center(child: CircularProgressIndicator());
                }
                if (state is LoadedSearch) {
                  _esarchResponse = state.categoriesResponse;
                }
                return _filterSort(context, _esarchResponse);
              },
            ),
          );
        });
  }
}

_filterSort(BuildContext context, SearchResponse categoriesResponse) {
  return Column(
    children: <Widget>[
      Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: <Widget>[
          InkWell(
            onTap: () =>
                BlocProvider.of<CategoriesBloc>(context).add(SortDialogClick()),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: Container(
                  width: (MediaQuery.of(context).size.width / 2) - 10.0,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.sort,
                        size: 25.0,
                      ),
                      Expanded(
                        child: Center(
                          child: Text(
                            "Sort",
//                            style: seconTextStyle,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
          InkWell(
            onTap: () => BlocProvider.of<CategoriesBloc>(context)
                .add(FilterDialogClick()),
            child: Card(
              child: Padding(
                padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: Container(
                  width: (MediaQuery.of(context).size.width / 2) - 10.0,
                  child: Row(
                    children: <Widget>[
                      Icon(
                        Icons.filter_list,
                        size: 25.0,
                      ),
                      Expanded(
                        child: Center(
                          child: Text(
                            "Filter",
//                            style: seconTextStyle,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
      Expanded(
        child: ListView.builder(
          shrinkWrap: true,
//          physics: ClampingScrollPhysics(),
          itemCount: categoriesResponse.products.products.length,
          itemBuilder: (context, position) {
            return _productListItem(
                categoriesResponse.products.products[position],
                context,
                position);
          },
        ),
      ),
    ],
  );
}

_productListItem(Products product, BuildContext context, int position) {
  return InkWell(
    onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
            builder: (context) => ProductDetail(
                  products: product,
                ))),
    child: Card(
      child: Row(
        children: <Widget>[
          Image.network(
            product.imgs[0].mediumUrl,
            height: MediaQuery.of(context).size.width / 2.5,
          ),
          Expanded(
            child: Container(
              height: MediaQuery.of(context).size.width / 2.5,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    product.name,
                    softWrap: true,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                  ),
                  Text(
                    "${product.stockQuantity.toString()} "
                    "Remaining",
//                    style: greyText,
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                            decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                border: new Border.all(
                                    color: Colors.grey, width: 1.0)),
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Row(
                                children: <Widget>[
                                  Text(product.ratings.getAverage()),
                                  Icon(
                                    Icons.star,
                                    color: Colors.orangeAccent,
                                    size: 20.0,
                                  )
                                ],
                              ),
                            )),
                      ),
                      Text("Out of "
                          "${product.ratings.total.toString()}"
                          " Ratings"),
                    ],
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          product.getsalePrice(),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          product.getPrice(),
                        ),
                      ),
                    ],
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    ),
  );
}
