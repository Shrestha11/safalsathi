import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/searchProducts/Model/SearchResponse.dart';
import 'package:safalsathi/searchProducts/Model/brands.dart';
import 'package:safalsathi/searchProducts/Model/searchFiltered/SearchFilterResponse.dart';

import 'bloc.dart';

class CategoriesBloc extends Bloc<CategoriesEvent, CategoriesState> {
  int categoryId;
  SearchResponse _categoriesResponse;
  List<Brands> brandList;
  List<String> colorList;
  List<String> sizeList;
  List<bool> brandSelected = [];
  List<bool> sizeSelected = [];
  List<bool> colorSelected = [];
  int minRangePrice = 10, maxRanagePrice = 100;
  int sortId = 0;

  @override
  CategoriesState get initialState => InitialCategoriesState();

  @override
  Stream<CategoriesState> mapEventToState(
    CategoriesEvent event,
  ) async* {
    if (event is LoadSearchEvent) {
      yield LoadingSearch();

      categoryId = event.categoryId;
      _categoriesResponse = await _fetchPosts(categoryId);
      brandList = _categoriesResponse.brands;
      colorList = _categoriesResponse.colors;
      sizeList = _categoriesResponse.sizes;
      for (int i = 0; i < brandList.length; i++) brandSelected.add(true);
      for (int i = 0; i < colorList.length; i++) colorSelected.add(true);
      for (int i = 0; i < sizeList.length; i++) sizeSelected.add(true);
      yield LoadedSearch(_categoriesResponse);
    }
    if (event is SortDialogClick) {
      yield SortDialogDisplayState();
    }
    if (event is FilterDialogClick) {
      yield FilterDialogDisplayState(
          brandList,
          colorList,
          sizeList,
          brandSelected,
          colorSelected,
          sizeSelected,
          minRangePrice,
          maxRanagePrice);
    }
    if (event is SortSelected) {
      sortId = event.id;
      SearchFilterResponse _filteredResponse = await _fetchFilteredSort(
          categoryId,
          sortId,
          brandList,
          sizeList,
          colorList,
          brandSelected,
          colorSelected,
          sizeSelected,
          minRangePrice,
          maxRanagePrice);
      _categoriesResponse.products.products =
          _filteredResponse.products.products;
      print("FILTERED RESPONSE ${_filteredResponse.toJson().toString()}");
      yield LoadedSearch(_categoriesResponse);
    }
    if (event is BrandSelected) {
      brandSelected[event.position] = true;
      yield BrandsUpdated(brandSelected);
    }
    if (event is BrandDeselected) {
      brandSelected[event.position] = false;
      yield BrandsUpdated(brandSelected);
    }
    if (event is SizeSelected) {
      sizeSelected[event.position] = true;
      yield SizeUpdated(sizeSelected);
    }
    if (event is SizeDeselected) {
      sizeSelected[event.position] = false;
      yield SizeUpdated(sizeSelected);
    }
    if (event is ColorSelected) {
      colorSelected[event.position] = true;
      yield ColorUpdated(colorSelected);
    }
    if (event is ColorDeselected) {
      colorSelected[event.position] = false;
      yield ColorUpdated(colorSelected);
    }
    if (event is PriceChanged) {
      minRangePrice = event.rangeValues.start.toInt();
      maxRanagePrice = event.rangeValues.end.toInt();
      yield PriceUpdated(minRangePrice, maxRanagePrice);
    }
    if (event is FilterApply) {
      SearchFilterResponse _filteredResponse = await _fetchFilteredSort(
          categoryId,
          sortId,
          brandList,
          sizeList,
          colorList,
          brandSelected,
          colorSelected,
          sizeSelected,
          minRangePrice,
          maxRanagePrice);
      _categoriesResponse.products.products =
          _filteredResponse.products.products;
      yield LoadedSearch(_categoriesResponse);
    }
  }
}

Future<SearchFilterResponse> _fetchFilteredSort(
    final int categoryId,
    final int sortId,
    final List<Brands> brandList,
    final List<String> sizeList,
    final List<String> colorList,
    final List<bool> brandSelected,
    final List<bool> colorSelected,
    final List<bool> sizeSelected,
    final int minRangePrice,
    final int maxRanagePrice) async {
  final String _endpoint = "/category/$categoryId/products";

  String sortString;
  switch (sortId) {
    case 0:
      sortString = "popular";
      break;
    case 1:
      sortString = "high-low";
      break;
    case 2:
      sortString = "low-high";
      break;
    case 3:
      sortString = "new";
      break;
    case 4:
      sortString = "old";
      break;
    case 5:
      sortString = "a-z";
      break;
    case 6:
      sortString = "z-a";
      break;
  }
  String brandFilterString = "";
  for (int i = 0; i < brandList.length; i++) {
    if (brandSelected[i] = true) {
      brandFilterString = "$brandFilterString&brand[$i]=${brandList[i].slug}";
    }
  }
  String sizeFilterString = "";
  for (int i = 0; i < sizeList.length; i++) {
    if (sizeSelected[i] = true) {
      sizeFilterString = "$sizeFilterString&size[$i]=${sizeList[i]}";
    }
  }
  String colorFilterString = "";
  for (int i = 0; i < colorList.length; i++) {
    if (colorSelected[i] = true) {
      colorFilterString = "$colorFilterString&colour[$i]=${colorList[i]}";
    }
  }
  String minPrice = (minRangePrice * 1000).toString();
  String maxPrice = (maxRanagePrice * 1000).toString();
  String priceFilterString = "&minprice=$maxPrice&minprice=$minPrice";
  try {
    Response response = await dioBASE().get(
        "$_endpoint?sort=$sortString$sizeFilterString$brandFilterString$colorFilterString$priceFilterString");
    return SearchFilterResponse.fromJsonMap(response.data);
  } catch (error) {
    return null;
  }
}

Future<SearchResponse> _fetchPosts(int a) async {
  final String _endpoint = "/category/$a/products";
  try {
    Response response = await dioBASE().get(_endpoint);
    return SearchResponse.fromJsonMap(response.data);
  } catch (error) {
    return null;
  }
}
