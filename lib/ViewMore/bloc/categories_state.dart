import 'package:meta/meta.dart';
import 'package:safalsathi/searchProducts/Model/SearchResponse.dart';
import 'package:safalsathi/searchProducts/Model/brands.dart';

@immutable
abstract class CategoriesState {}

class InitialCategoriesState extends CategoriesState {}

class LoadingSearch extends CategoriesState {
  @override
  String toString() => 'Loading Search';
}

class LoadedSearch extends CategoriesState {
  final SearchResponse categoriesResponse;

  LoadedSearch(this.categoriesResponse);

  @override
  String toString() => 'Loaded Search';
}

class SortDialogDisplayState extends CategoriesState {
  @override
  String toString() => 'Sort Dialog Display State';
}

class FilterDialogDisplayState extends CategoriesState {
  final List<Brands> brandList;
  final List<String> colorList;
  final List<String> sizeList;
  final List<bool> brandSelected;
  final List<bool> sizeSelected;
  final List<bool> colorSelected;
  final int minRangePrice, maxRangePrice;

  FilterDialogDisplayState(
      this.brandList,
      this.colorList,
      this.sizeList,
      this.brandSelected,
      this.colorSelected,
      this.sizeSelected,
      this.minRangePrice,
      this.maxRangePrice);

  @override
  String toString() => 'Filtered Dialog Display State';
}

class LoadingNextPage extends CategoriesState {
  @override
  String toString() => 'Loading Next Page';
}

class SortUpdated extends CategoriesState {
  @override
  String toString() => 'Sort Updated';
}

class FilterUpdated extends CategoriesState {
  @override
  String toString() => 'Filter Updated';
}

class PriceUpdated extends CategoriesState {
  final int minRangePrice, maxRangePrice;

  PriceUpdated(this.minRangePrice, this.maxRangePrice);

  @override
  String toString() => 'Price Updated';
}

class BrandsUpdated extends CategoriesState {
  final List<bool> brandSelected;

  BrandsUpdated(this.brandSelected);

  @override
  String toString() => 'Brand Updated';
}

class ColorUpdated extends CategoriesState {
  final List<bool> colorSelected;

  ColorUpdated(this.colorSelected);

  @override
  String toString() => 'ColorUpdated';
}

class SizeUpdated extends CategoriesState {
  final List<bool> sizeSelected;

  SizeUpdated(this.sizeSelected);

  @override
  String toString() => 'Size Updated';
}
