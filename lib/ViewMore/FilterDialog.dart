import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/searchProducts/Model/brands.dart';

import 'bloc/categories_bloc.dart';
import 'bloc/categories_event.dart';
import 'bloc/categories_state.dart';

class DynamicDialog extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DialogState();
  }
}

class _DialogState extends State<DynamicDialog> {
  double minValue = 10.0, maxValue = 99.0;
  List<Brands> brandList = [];
  List<String> sizeList = [];
  List<String> colorList = [];
  List<bool> brandSelected = [];
  List<bool> sizeSelected = [];
  List<bool> colorSelected = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: BlocProvider.of<CategoriesBloc>(context),
        builder: (context, categoriesState) {
          if (categoriesState is FilterDialogDisplayState) {
            minValue = categoriesState.minRangePrice.toDouble();
            maxValue = categoriesState.maxRangePrice.toDouble();
            brandList = categoriesState.brandList;
            sizeList = categoriesState.sizeList;
            colorList = categoriesState.colorList;
            brandSelected = categoriesState.brandSelected;
            sizeSelected = categoriesState.sizeSelected;
            colorSelected = categoriesState.colorSelected;
          }
          if (categoriesState is PriceUpdated) {
            minValue = categoriesState.minRangePrice.toDouble();
            maxValue = categoriesState.maxRangePrice.toDouble();
          }
          if (categoriesState is SizeUpdated) {
            sizeSelected = categoriesState.sizeSelected;
          }
          if (categoriesState is BrandsUpdated) {
            brandSelected = categoriesState.brandSelected;
          }
          if (categoriesState is ColorUpdated) {
            colorSelected = categoriesState.colorSelected;
          }
          return SingleChildScrollView(
              child: Column(
            children: <Widget>[
              Center(
                  child: Text(
                "Apply Filter",
//                        style: redText,
              )),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Center(
                            child: Text(
                          "Price",
//                                  style: greyText,
                        )),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                        ),
                      ],
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("MinPrice: \nRs.${minValue * 1000}"),
                        Text("MaxPrice: \nRs.${maxValue * 1000}"),
                      ],
                    ),
                    RangeSlider(
                        values: RangeValues(minValue, maxValue),
                        min: 10,
                        max: 100,
                        onChanged: (RangeValues rangeValues) {
                          BlocProvider.of<CategoriesBloc>(context)
                              .add(PriceChanged(rangeValues));
                        }),
                  ],
                ),
              ), //Price
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Brands",
//                              style: greyText,
                        ),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                        ),
                      ],
                    ),
                    brandList == null
                        ? Container()
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: brandList.length,
                            itemBuilder: (context, position) {
                              return Row(
                                children: <Widget>[
                                  Checkbox(
                                    value: brandSelected[position],
                                    onChanged: (bool value) {
                                      if (value) {
                                        BlocProvider.of<CategoriesBloc>(context)
                                            .add(BrandSelected(position));
                                      } else {
                                        BlocProvider.of<CategoriesBloc>(context)
                                            .add(BrandDeselected(position));
                                      }
                                    },
                                  ),
                                  Text(brandList[position].name),
                                ],
                              );
                            },
                          ),
                  ],
                ),
              ), //Brands
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Sizes",
                        ),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                        ),
                      ],
                    ),
                    sizeList == null
                        ? Container()
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: sizeList.length,
                            itemBuilder: (context, position) {
                              return Row(
                                children: <Widget>[
                                  Checkbox(
                                    value: sizeSelected[position],
                                    onChanged: (bool value) {
                                      if (value) {
                                        BlocProvider.of<CategoriesBloc>(context)
                                            .add(SizeSelected(position));
//                                    Navigator.pop(context);
                                      } else {
                                        BlocProvider.of<CategoriesBloc>(context)
                                            .add(SizeDeselected(position));
//                                    Navigator.pop(context);
                                      }
                                    },
                                  ),
                                  Text(sizeList[position]),
                                ],
                              );
                            },
                          ),
                  ],
                ),
              ), //Sizes
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Color",
//                          style: greyText,
                        ),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                        ),
                      ],
                    ),
                    colorList == null
                        ? Container()
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: colorList.length,
                            itemBuilder: (context, position) {
                              return Row(
                                children: <Widget>[
                                  Checkbox(
                                      value: colorSelected[position],
                                      onChanged: (bool value) {
                                        if (value) {
                                          BlocProvider.of<CategoriesBloc>(
                                                  context)
                                              .add(ColorSelected(position));
//                                        Navigator.pop(context);
                                        } else {
                                          BlocProvider.of<CategoriesBloc>(
                                                  context)
                                              .add(ColorDeselected(position));
//                                        Navigator.pop(context);
                                        }
                                      }),
                                  Text(colorList[position]),
                                ],
                              );
                            },
                          ),
                  ],
                ),
              ),
              RaisedButton(
                child: Text("Filter Products"),
                onPressed: () =>
                    BlocProvider.of<CategoriesBloc>(context).add(FilterApply()),
              )
            ],
          ));
        });
  }
}

void showSortDialog(BuildContext context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                  leading: new Icon(
                    Icons.radio_button_unchecked,
                    size: 18,
                  ),
                  title: new Text('Popularity'),
                  dense: true,
                  onTap: () {
                    BlocProvider.of<CategoriesBloc>(context)
                        .add(SortSelected(0));
                    Navigator.of(context).pop(true);
                  },
                ),
                new ListTile(
                  leading: new Icon(
                    Icons.radio_button_unchecked,
                    size: 18,
                  ),
                  title: new Text('Price: High to Low'),
                  dense: true,
                  onTap: () {
                    BlocProvider.of<CategoriesBloc>(context)
                        .add(SortSelected(1));
                    Navigator.of(context).pop(true);
                  },
                ),
                new ListTile(
                  leading: new Icon(
                    Icons.radio_button_unchecked,
                    size: 18,
                  ),
                  dense: true,
                  title: new Text('Price: Low to High'),
                  onTap: () {
                    BlocProvider.of<CategoriesBloc>(context)
                        .add(SortSelected(2));
                    Navigator.of(context).pop(true);
                  },
                ),
                new ListTile(
                  leading: new Icon(
                    Icons.radio_button_unchecked,
                    size: 18,
                  ),
                  title: new Text('Newest'),
                  dense: true,
                  onTap: () {
                    BlocProvider.of<CategoriesBloc>(context)
                        .add(SortSelected(3));
                    Navigator.of(context).pop(true);
                  },
                ),
                new ListTile(
                  leading: new Icon(
                    Icons.radio_button_unchecked,
                    size: 18,
                  ),
                  title: new Text('Oldest'),
                  dense: true,
                  onTap: () {
                    BlocProvider.of<CategoriesBloc>(context)
                        .add(SortSelected(4));
                    Navigator.of(context).pop(true);
                  },
                ),
                new ListTile(
                  leading: new Icon(
                    Icons.radio_button_unchecked,
                    size: 18,
                  ),
                  title: new Text('A-Z'),
                  dense: true,
                  onTap: () {
                    BlocProvider.of<CategoriesBloc>(context)
                        .add(SortSelected(5));
                    Navigator.of(context).pop(true);
                  },
                ),
                new ListTile(
                  leading: new Icon(
                    Icons.radio_button_unchecked,
                    size: 18,
                  ),
                  title: new Text('Z-a'),
                  dense: true,
                  onTap: () {
                    BlocProvider.of<CategoriesBloc>(context)
                        .add(SortSelected(6));
                    Navigator.of(context).pop(true);
                  },
                ),
              ],
            ),
          ),
        );
      });
}
