import 'package:meta/meta.dart';

@immutable
abstract class CheckoutEvent {}

class LoadDatabase extends CheckoutEvent {
  @override
  String toString() => 'LoadDatabase';
}

class CouponCheck extends CheckoutEvent {
  @override
  String toString() => 'Coupon Check';
}

class CheckOutProduct extends CheckoutEvent {
  final String firstName, lastName, email, country, zone, district, area, phone;

  CheckOutProduct(this.firstName, this.lastName, this.email, this.country,
      this.zone, this.district, this.area, this.phone);

  @override
  String toString() => 'Check Out Products';
}

class LoadShippingAddress extends CheckoutEvent {
  @override
  String toString() => 'LoadShippingAddress';
}

class LoadDefaultAddress extends CheckoutEvent {
  @override
  String toString() => 'Load Default Address';
}

class DropDownClickLocationType extends CheckoutEvent {
  final String newValue;

  DropDownClickLocationType(this.newValue);

  @override
  String toString() => 'DropDown Item Change';
}

class DropDownPaymentTypeClick extends CheckoutEvent {
  final String newValue;

  DropDownPaymentTypeClick(this.newValue);

  @override
  String toString() => 'DropDown Item Change';
}

class DefaultAddressErrorEvent extends CheckoutEvent {
  final String error;

  DefaultAddressErrorEvent(this.error);

  @override
  String toString() => 'Default Address Error Event';
}

class CheckingCoupon extends CheckoutEvent {
  final String couponId;

  CheckingCoupon(this.couponId);

  @override
  String toString() => 'CheckingCoupon';
}

class Checked extends CheckoutEvent {
  @override
  String toString() => 'Checked';
}

class Verified extends CheckoutEvent {
  @override
  String toString() => 'Verified';
}

class InvalidCouponEvent extends CheckoutEvent {
  final String msg;

  InvalidCouponEvent(this.msg);

  @override
  String toString() => 'Invalid';
}

class LoadShippingPlaces extends CheckoutEvent {
  @override
  String toString() => 'Load Shiping Places';
}

class DropDownShippingLocationClick extends CheckoutEvent {
  final String selectedItem;

  DropDownShippingLocationClick(this.selectedItem);

  @override
  String toString() => 'Drop Down Click Shippng Location';
}

class LoadShippingPlacesError extends CheckoutEvent {
  final String errorMsg;

  LoadShippingPlacesError(this.errorMsg);

  @override
  String toString() => 'Load Shipping Places Error';
}
