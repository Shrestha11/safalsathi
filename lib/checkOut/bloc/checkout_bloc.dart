import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/checkOut/Model/CartContents.dart';
import 'package:safalsathi/checkOut/Model/CheckOutResponse/OrderResponse.dart';
import 'package:safalsathi/checkOut/Model/CouponResponse/CouponResponse.dart';
import 'package:safalsathi/checkOut/Model/PostCheckoutModel.dart';
import 'package:safalsathi/checkOut/Model/ShippingPlaces/ShippingPlacesAndPaymentMethod.dart';
import 'package:safalsathi/checkOut/Model/defaultAddressResponse/DefaultAddress.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/Database/DatabaseHelper.dart';
import 'package:safalsathi/common/Database/databaseModel.dart';
import 'package:safalsathi/common/model/products.dart';

import 'package:safalsathi/common/DioBase.dart';
import 'bloc.dart';

class CheckoutBloc extends Bloc<CheckoutEvent, CheckoutState> {
  DatabaseHelper _databaseHelper = DatabaseHelper();
  List<Cart> cartList;
  double calculatedPrice, taxTotalAmount, finalPrice, taxAmount;
  bool isCouponApplied = false, isDefaultAddressUsed = false;
  int defaultAddressId;
  String usedCouponId = "";
  int usedCouponOnProductId;

  DefaultAddress _defaultAddress;
  ShippingPlacesAndPaymentMethod shippingPlacesAndPaymentMethod;
  CouponResponse _couponResponse;
  int locationType = 0;
  String shippingAddress = "";
  List<String> shippingPlaces = [];
  List<String> paymentType = [];
  int selectedIndexAddress = 0;
  int selectedIndexPaymentMethod = 0;
  String selectedShippingAddressNameDropDown = "";
  String selectedShippingAddressNamePost = "";
  int selectedPaymentMethodId;
  List<int> listOfItemId = [];
  OrderResponse _orderResponse;

  @override
  CheckoutState get initialState => InitialCheckoutState();

  @override
  Stream<CheckoutState> mapEventToState(
    CheckoutEvent event,
  ) async* {
    if (event is LoadDatabase) {
      yield (LoadingDatabaseState());
      try {
        cartList = await _databaseHelper.fetchAll();
        calculatedPrice = 0;
        taxAmount = 0;
        finalPrice = 0;
        for (int i = 0; i < cartList.length; i++) {
          Products _products = decode(cartList[i].detail);
          int taxPercent = _products.tax == null ? 0 : _products.tax;
          taxAmount = taxAmount +
              ((taxPercent * (_products.salesPrice * cartList[i].qty)) / 100);
          calculatedPrice =
              calculatedPrice + (_products.salesPrice * cartList[i].qty);
        }
        finalPrice = calculatedPrice + taxAmount;
        yield (LoadedDatabaseState(cartList, "Rs. $finalPrice"));
        for (int i = 0; i < cartList.length; i++) {
          listOfItemId.add(cartList[i].cartId);
        }
      } catch (e) {
        yield (ErrorLoadingDatabaseState(e.toString()));
      }
      add(LoadShippingAddress());
    }
    if (event is LoadShippingAddress) {
      yield (LoadingDefaultAddress());
      _defaultAddress = await _fetchDefaultAddress().catchError((e) {
        print("ERROR LoadingShippingAddress $e");
        add(DefaultAddressErrorEvent(e.toString()));
        isDefaultAddressUsed = false;
      });
      isDefaultAddressUsed = true;
      defaultAddressId = _defaultAddress.data.id;
      yield LoadedDefaultAddress(_defaultAddress);
      add(LoadShippingPlaces());
    }
    if (event is DropDownClickLocationType) {
      locationType = event.newValue == "Home" ? 0 : 1;

      yield DropDownLocationTypeUpdated(event.newValue);
    }
    if (event is DropDownShippingLocationClick) {
      for (int i = 0; i < shippingPlaces.length; i++) {
        if (shippingPlaces[i] == event.selectedItem) {
          selectedIndexAddress = i;
          selectedShippingAddressNameDropDown =
              shippingPlaces[selectedIndexAddress];
          selectedShippingAddressNamePost =
              shippingPlacesAndPaymentMethod.shippingPlaces[i].place;
          break;
        }
      }
      yield DropDownShippingLocationUpdated(
          selectedShippingAddressNameDropDown);
    }
    if (event is DropDownPaymentTypeClick) {
      for (int i = 0; i < paymentType.length; i++) {
        if (paymentType[i] == event.newValue) {
          selectedIndexPaymentMethod = i;
          break;
        }
      }
      selectedPaymentMethodId = shippingPlacesAndPaymentMethod
          .paymentMethods[selectedIndexPaymentMethod].id;
      yield DropDownPaymentTypeUpdated(paymentType[selectedIndexPaymentMethod]);
    }

    if (event is LoadShippingPlaces) {
      shippingPlacesAndPaymentMethod =
          await _fetchShippingPlaces().catchError((e) {
        add(LoadShippingPlacesError(e.toString()));
      });

      for (int i = 0;
          i < shippingPlacesAndPaymentMethod.shippingPlaces.length;
          i++) {
        shippingPlaces.add(
            "${shippingPlacesAndPaymentMethod.shippingPlaces[i].place}"
            "  Rs. ${shippingPlacesAndPaymentMethod.shippingPlaces[i].amount}");
      }
      for (int i = 0;
          i < shippingPlacesAndPaymentMethod.paymentMethods.length;
          i++) {
        paymentType
            .add("${shippingPlacesAndPaymentMethod.paymentMethods[i].name}");
      }
      selectedShippingAddressNamePost =
          shippingPlacesAndPaymentMethod.shippingPlaces[0].place;
      selectedPaymentMethodId =
          shippingPlacesAndPaymentMethod.shippingPlaces[0].id;
      yield ShippingPlacesAndPaymentMethodState(shippingPlaces, paymentType);
    }

    if (event is CheckingCoupon) {
      yield CouponChecking();
      _couponResponse =
          await _checkCoupon(event.couponId, listOfItemId).catchError((e) {
        add(InvalidCouponEvent(e.toString()));
        isCouponApplied = false;
        usedCouponId = "";
      });
      isCouponApplied = true;
      usedCouponId = _couponResponse.code;
      usedCouponOnProductId = _couponResponse.productId;
      yield CouponValid(_couponResponse);
    }
    if (event is InvalidCouponEvent) {
      isCouponApplied = false;
      usedCouponId = "";
      yield CouponInValid(event.msg);
    }

    if (event is CheckOutProduct) {
      print("State is Checkout Products");
      List<CartContents> cartContentList = [];
      for (int i = 0; i < cartList.length; i++) {
        Products _products = decode(cartList[i].detail);
        CartContents cartContents;
        if (usedCouponOnProductId == _products.id) {
          cartContents = CartContents(cartList[i].cartId, cartList[i].qty,
              _products.salesPrice, cartList[i].size, usedCouponId);
        } else {
          cartContents = CartContents(cartList[i].cartId, cartList[i].qty,
              _products.salesPrice, cartList[i].size);
        }
        cartContentList.add(cartContents);
      }
      _orderResponse = await order(
              event.zone,
              event.district,
              event.area,
              locationType,
              event.phone,
              selectedShippingAddressNamePost,
              selectedPaymentMethodId,
              cartContentList,
              event.firstName,
              event.lastName,
              event.email,
              event.country,
              isDefaultAddressUsed ? defaultAddressId : 0,
              couponResponse: isCouponApplied ? _couponResponse : null)
          .catchError((e) {
        print(e);
        add(DefaultAddressErrorEvent(e.toString()));
      });
      if(_orderResponse.success == "Order successfully placed!"){
        _databaseHelper.removeAllItem();
        yield BackToHome();
      }


    }
    if (event is DefaultAddressErrorEvent) {
      yield DefaultAddressError(event.error);
    }
  }
}

Future<OrderResponse> order(
    String zone,
    String district,
    String area,
    int locationType,
    String phone,
    String selectedShippingAddressNamePost,
    int selectedPaymentMethodId,
    List<CartContents> cartContentList,
    String firstName,
    String lastName,
    String email,
    String country,
    int defaultAddressid,
    {CouponResponse couponResponse}) async {
  String _endpoint = "/checkout";

  var cartContentString = [];
  for (int i = 0; i < cartContentList.length; i++) {
    cartContentString.add(json.encode(cartContentList[i]));
  }
  PostCheckOutModel postCheckOutModel = PostCheckOutModel(
      area,
      locationType.toString(),
      phone,
      selectedShippingAddressNamePost,
      selectedPaymentMethodId.toString(),
      cartContentList,
      firstName,
      lastName,
      email,
      country,
      district,
      zone,
      defaultAddressid == 0 ? null : defaultAddressid.toString(),
      couponResponse);

  var postCheckOutModelString = json.encode(postCheckOutModel);
  Response response;
  try {
    response = await dioBASE(await getAuth()).post(
      _endpoint,
      data: postCheckOutModelString,
    );
    print("sssssssss $response");
    return OrderResponse.fromJsonMap(response.data);
  } catch (e) {
    return Future.error(e.toString());
  }
}

Future<ShippingPlacesAndPaymentMethod> _fetchShippingPlaces() async {
  String _endpoint = "/shipping-places";
  try {
    Response response = await dioBASE(await getAuth()).get(_endpoint);
    return ShippingPlacesAndPaymentMethod.fromJsonMap(response.data);
  } on DioError catch (e) {
    return Future.error("Error:${e.toString()}");
  } catch (error) {
    return Future.error("Error:${error.toString()}");
  }
}

Future<DefaultAddress> _fetchDefaultAddress() async {
  String _endpoint = "/my-account/default-address";
  try {
    Response response = await dioBASE(await getAuth()).get(_endpoint);
    print(response);
    return DefaultAddress.fromJsonMap(response.data);
  } on DioError catch (e) {
    print("DIO ERROR $e");

    if (e.response.statusCode == 404) {
      return Future.error("No Default Address Found");
    }
    return Future.error("ERROR CODE: ${e.response.statusCode}");
  } catch (error) {
    print("DIO ERROR $error");

    return Future.error("ERROR CODE: ${error.response.statusCode}");
  }
}

Future<CouponResponse> _checkCoupon(
    String couponId, List<int> listofItemId) async {
  for (int i = 0; i < listofItemId.length; i++) {}

  String _endpoint = "/check-coupon";
  try {
    Response response = await dioBASE(await getAuth()).post(_endpoint, data: {
      "coupon_code": couponId,
      "product_id": listofItemId,
    });
    return CouponResponse.fromJsonMap(response.data);
  } on DioError catch (e) {
    if (e.response.statusCode == 406) {
      return Future.error("Coupon Expired");
    }

    if (e.response.statusCode == 401) {
      return Future.error("User UnAuthencicated");
    }
    if (e.response.statusCode == 404) {
      return Future.error("Coupon Doesnot Exists");
    }
    return Future.error("ERROR CODE: ${e.response.statusCode.toString()}");
  } catch (error) {
    return Future.error("ERROR CODE: ${error.toString()}");
  }
}
