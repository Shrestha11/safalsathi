import 'package:safalsathi/checkOut/Model/CouponResponse/CouponResponse.dart';
import 'package:safalsathi/checkOut/Model/defaultAddressResponse/DefaultAddress.dart';
import 'package:safalsathi/common/Database/databaseModel.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CheckoutState {}

class InitialCheckoutState extends CheckoutState {}

class LoadingDatabaseState extends CheckoutState {
  @override
  String toString() => 'LoadingDatabaseState';
}

class LoadedDatabaseState extends CheckoutState {
  final List<Cart> cartList;
  final String finalPrice;

  LoadedDatabaseState(this.cartList, this.finalPrice);

  @override
  String toString() => 'LoadedDatabaseState';
}

class ErrorLoadingDatabaseState extends CheckoutState {
  final String errorMsg;

  ErrorLoadingDatabaseState(this.errorMsg);

  @override
  String toString() => 'ErrorLoadingDatabaseState';
}

class ErrorState extends CheckoutState {
  ErrorState(String finalPrice);

  @override
  String toString() => 'ErrorState';
}

class LoadingDefaultAddress extends CheckoutState {
  @override
  String toString() => 'Loading Default Address';
}

class LoadedDefaultAddress extends CheckoutState {
  final DefaultAddress defaultAddress;

  LoadedDefaultAddress(this.defaultAddress);

  @override
  String toString() => 'Loaded Default Address';
}

class DefaultAddressError extends CheckoutState {
  final String msg;

  DefaultAddressError(this.msg);

  @override
  String toString() => 'Defailt Address Error';
}

class DropDownLocationTypeUpdated extends CheckoutState {
  final String newValue;

  DropDownLocationTypeUpdated(this.newValue);

  @override
  String toString() => 'Drop DOwn Item Change';
}

class DropDownShippingLocationUpdated extends CheckoutState {
  final String newValue;

  DropDownShippingLocationUpdated(this.newValue);

  @override
  String toString() => 'Drop Down Address Item Change';
}

class DropDownPaymentTypeUpdated extends CheckoutState {
  final String newValue;

  DropDownPaymentTypeUpdated(this.newValue);

  @override
  String toString() => 'Drop Down Address Item Change';
}

class CouponChecking extends CheckoutState {
  @override
  String toString() => 'Coupon Checking';
}
class BackToHome extends CheckoutState {
  @override
  String toString() => 'Home';
}

class CouponValid extends CheckoutState {
  final CouponResponse couponResponse;

  CouponValid(this.couponResponse);

  @override
  String toString() => 'Coupon is Valid';
}

class CouponInValid extends CheckoutState {
  final msg;

  CouponInValid(this.msg);

  @override
  String toString() => 'Coupon Is InValid';
}

class ShippingPlacesAndPaymentMethodState extends CheckoutState {
  final List<String> shippingPlaces;
  final List<String> paymentType;

  ShippingPlacesAndPaymentMethodState(this.shippingPlaces, this.paymentType);

  @override
  String toString() => 'ShippingPlaces';
}
