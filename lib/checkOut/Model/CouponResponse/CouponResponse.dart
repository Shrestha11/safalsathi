class CouponResponse {
  final int id;
  final int productId;
  final String code;
  final int discountValue;

  CouponResponse.fromJsonMap(Map<String, dynamic> map)
      : id = map["id"],
        productId = map["product_id"],
        code = map["code"],
        discountValue = map["discount_value"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id.toString();
    data['product_id'] = productId.toString();
    data['code'] = code;
    data['discount_value'] = discountValue.toString();
    return data;
  }
}
