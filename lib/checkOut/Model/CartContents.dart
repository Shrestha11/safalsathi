class CartContents {
  int productId;
  int qty;
  int price;
  String coupon;
  String size;

  CartContents(this.productId, this.qty, this.price, this.size, [this.coupon]);

  Map<String, dynamic> toJson() => {
        "product_id": productId == null ? null : productId.toString(),
        "qty": qty == null ? null : qty.toString(),
        "price": price == null ? null : price.toString(),
        "coupon": coupon == null ? null : coupon,
        "coupon": coupon == "" ? null : coupon,
        "size": size == null ? null : size,
      };
}
