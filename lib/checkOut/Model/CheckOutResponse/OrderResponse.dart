
class OrderResponse {

  final String success;

	OrderResponse.fromJsonMap(Map<String, dynamic> map): 
		success = map["success"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['success'] = success;
		return data;
	}
}
