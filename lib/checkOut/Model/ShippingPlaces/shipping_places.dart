
class ShippingPlacess {

  final int id;
  final String place;
  final String amount;
  final String createdAt;
  final String updatedAt;

	ShippingPlacess.fromJsonMap(Map<String, dynamic> map):
		id = map["id"],
		place = map["place"],
		amount = map["amount"],
		createdAt = map["created_at"],
		updatedAt = map["updated_at"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['place'] = place;
		data['amount'] = amount;
		data['created_at'] = createdAt;
		data['updated_at'] = updatedAt;
		return data;
	}
}
