
class PaymentMethods {

  final int id;
  final String name;
  final String companyName;
  final String createAt;
  final String updatedAt;
  final String image;

	PaymentMethods.fromJsonMap(Map<String, dynamic> map):
		id = map["id"],
		name = map["name"],
		companyName = map["company_name"],
		createAt = map["created_at"],
		updatedAt = map["updated_at"],
		image = map["image"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['name'] = name;
		data['company_name'] = companyName;
		data['created_at'] = createAt;
		data['updated_at'] = updatedAt;
		data['image'] = image;
		return data;
	}
}
