
import 'package:safalsathi/checkOut/Model/ShippingPlaces/payment_methods.dart';
import 'package:safalsathi/checkOut/Model/ShippingPlaces/shipping_places.dart';

class ShippingPlacesAndPaymentMethod {

  final List<ShippingPlacess> shippingPlaces;
  final List<PaymentMethods> paymentMethods;

	ShippingPlacesAndPaymentMethod.fromJsonMap(Map<String, dynamic> map): 
		shippingPlaces = List<ShippingPlacess>.from(map["shipping_places"].map((it) => ShippingPlacess.fromJsonMap(it))),
		paymentMethods = List<PaymentMethods>.from(map["payment_methods"].map((it) => PaymentMethods.fromJsonMap(it)));

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['shipping_places'] = shippingPlaces != null ?
			this.shippingPlaces.map((v) => v.toJson()).toList()
			: null;
		data['payment_methods'] = paymentMethods != null ?
			this.paymentMethods.map((v) => v.toJson()).toList()
			: null;
		return data;
	}
}
