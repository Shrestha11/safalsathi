import 'CartContents.dart';
import 'CouponResponse/CouponResponse.dart';

class PostCheckOutModel {
  String area;
  String locationType;
  String mobile;
  String shippingAddress;
  String paymentMethodId;
  List<CartContents> cartContents;
  CouponResponse coupon;
  String firstName;
  String lastName;
  String email;
  String country;
  String district;
  String zone;
  String addressId;

  PostCheckOutModel(
      this.area,
      this.locationType,
      this.mobile,
      this.shippingAddress,
      this.paymentMethodId,
      this.cartContents,
      this.firstName,
      this.lastName,
      this.email,
      this.country,
      this.district,
      this.zone,
      this.addressId,
      [this.coupon]);

  Map<String, dynamic> toJson() => {
        "area": area == null ? null : area.toString(),
        "locationType": locationType == null ? null : locationType.toString(),
        "mobile": mobile == null ? null : mobile.toString(),
        "shipping_address": shippingAddress == null ? null : shippingAddress,
        "payment_method_id":
            paymentMethodId == null ? null : paymentMethodId,
        "cartContents": cartContents == null ? null : cartContents,
        "coupon": coupon == null ? null : coupon,
        "first_name": firstName == null ? null : firstName,
        "last_name": lastName == null ? null : lastName,
        "email": email == null ? null : email,
        "country": country == null ? null : country,
        "district": district == null ? null : district,
        "zone": zone == null ? null : zone,
        "address_id": addressId == null ? null : addressId,
      };
}
