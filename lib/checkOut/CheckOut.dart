import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/MyCart/MyCart.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/Database/databaseModel.dart';
import 'package:safalsathi/common/model/products.dart';
import 'package:safalsathi/common/common_ui.dart';

import '../app_styles.dart';

import 'bloc/checkout_bloc.dart';
import 'bloc/checkout_event.dart';
import 'bloc/checkout_state.dart';

class CheckOut extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _CheckOutState();
  }
}

class _CheckOutState extends State<CheckOut> {
  CheckoutBloc _checkoutBloc = CheckoutBloc();
  List<Cart> cartList;
  String finalPrice;

  Products products;
  var _firstNameController = TextEditingController();
  var _lastNameController = TextEditingController();
  var _emailNameController = TextEditingController();
  var _zoneController = TextEditingController();
  var _districtController = TextEditingController();
  var _areaController = TextEditingController();
  var _phoneController = TextEditingController();
  var _countryController = TextEditingController(text: "Nepal");
  var _couponController = TextEditingController();
  GlobalKey<FormState> _validationKey = new GlobalKey();
  GlobalKey<FormState> _couponKey = new GlobalKey();
  String dropdownValue = "Home";
  String shippingPlaceValue = "";
  String paymentTypeValue = "";
  bool isShippingPaymentLoaded = false;
  List<String> _shippingPlacesNameList = [];
  List<String> _paymentTypeList = [];
  bool isDefault = false;

  @override
  void initState() {
    super.initState();
    _checkoutBloc.add(LoadDatabase());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("CheckOut"),
      ),
      body: BlocListener(
        bloc: _checkoutBloc,
        listener: (context, _checkoutState) {
          print("CUrrent State $_checkoutState");
          if (_checkoutState is LoadedDatabaseState) {
            buildSnackSuccess(context, "Loading Success");
          }
          if (_checkoutState is LoadedDatabaseState) {
            cartList = _checkoutState.cartList;
            finalPrice = _checkoutState.finalPrice;
          }
          if (_checkoutState is DropDownLocationTypeUpdated) {
            dropdownValue = _checkoutState.newValue;
          }
          if (_checkoutState is DefaultAddressError) {
            buildSnackError(context, _checkoutState.msg);
          }
          if (_checkoutState is LoadedDefaultAddress) {
            isDefault = true;
            buildSnackSuccess(context, "Default Address Found");
            _firstNameController = TextEditingController(
                text: _checkoutState.defaultAddress.data.firstName);
            _lastNameController = TextEditingController(
                text: _checkoutState.defaultAddress.data.lastName);
            _emailNameController = TextEditingController(
                text: _checkoutState.defaultAddress.data.email);
            _zoneController = TextEditingController(
                text: _checkoutState.defaultAddress.data.zone);
            _districtController = TextEditingController(
                text: _checkoutState.defaultAddress.data.district);
            _areaController = TextEditingController(
                text: _checkoutState.defaultAddress.data.area);
            _phoneController = TextEditingController(
                text: _checkoutState.defaultAddress.data.phone);
          }
          if (_checkoutState is CouponInValid) {
            buildSnackError(context, _checkoutState.msg);
          }
          if (_checkoutState is ShippingPlacesAndPaymentMethodState) {
            isShippingPaymentLoaded = true;
            _shippingPlacesNameList = _checkoutState.shippingPlaces;
            shippingPlaceValue = _shippingPlacesNameList[0];
            _paymentTypeList = _checkoutState.paymentType;
            paymentTypeValue = _paymentTypeList[0];
          }
          if (_checkoutState is DropDownShippingLocationUpdated) {
            shippingPlaceValue = _checkoutState.newValue;
          }

          if (_checkoutState is DropDownPaymentTypeUpdated) {
            paymentTypeValue = _checkoutState.newValue;
          }
          if (_checkoutState is CouponValid) {
            buildSnackSuccess(context,
                "${_checkoutState.couponResponse.code} is Added to Product Id: ${_checkoutState.couponResponse.id}");
          }
          if(_checkoutState is BackToHome){
            Navigator.of(context).popUntil((route) => route.isFirst);
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => MyCart(
                ),
              ),
            );
          }
        },
        child: BlocBuilder(
            bloc: _checkoutBloc,
            builder: (context, _cartState) {
              if (_cartState is LoadingDatabaseState) {
                return buildLoadingWidget(context);
              }
              if (_cartState is ErrorLoadingDatabaseState) {
               // return buildErrorWidget(context, _cartState.errorMsg);
              }
              return SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Card(
                      margin: EdgeInsets.fromLTRB(0.0, 4.0, 0.0, 4.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Products",
                              style: blackH3Text,
                            ),
                          ),
                          cartList == null
                              ? Text("EMPTY CART")
                              : ListView.builder(
                                  shrinkWrap: true,
                                  physics: NeverScrollableScrollPhysics(),
                                  itemCount: cartList.length,
                                  itemBuilder: (context, position) {
                                    products =
                                        decode(cartList[position].detail);
                                    return Card(
                                      child: Row(
                                        children: <Widget>[
                                          Stack(
                                            children: <Widget>[
                                              Image.network(
                                                products.imgs[0].mediumUrl,
                                                width: MediaQuery.of(context).size.width / 3.5,
                                              ),
                                              Positioned(
                                                  right: 0.0,
                                                  bottom: 0.0,
                                                  child: Text(
                                                    cartList[position].getqty(),
                                                    style: blackh6Text,
                                                  ))
                                            ],
                                          ),
                                          Container(
                                            width: ((2 *MediaQuery.of(context).size.width ) / 3),
                                            child: Column(
                                              crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                              mainAxisAlignment:
                                                  MainAxisAlignment.spaceEvenly,
                                              children: <Widget>[
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          8.0, 4.0, 8.0, 4.0),
                                                  child: Text(
                                                    products.name,
                                                    style: blackH1Text,
                                                    maxLines: 2,
                                                    overflow:
                                                        TextOverflow.ellipsis,
                                                  ),
                                                ),
                                                Row(
                                                  children: <Widget>[
                                                    Card(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(2.0),
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .fromLTRB(
                                                                4.0,
                                                                2.0,
                                                                4.0,
                                                                4.0),
                                                        child: Text(
                                                          cartList[position]
                                                              .size==null?"":cartList[position].size,
                                                          style: greyH6Text,
                                                        ),
                                                      ),
                                                    ),
                                                    Card(
                                                      shape:
                                                          RoundedRectangleBorder(
                                                        borderRadius:
                                                            BorderRadius
                                                                .circular(2.0),
                                                      ),
                                                      child: Padding(
                                                        padding:
                                                            const EdgeInsets
                                                                    .fromLTRB(
                                                                4.0,
                                                                2.0,
                                                                4.0,
                                                                2.0),
                                                        child: Text(
                                                          cartList[position]
                                                                      .color ==
                                                                  null
                                                              ? ""
                                                              : cartList[
                                                                      position]
                                                                  .color,
                                                          style: greyH6Text,
                                                        ),
                                                      ),
                                                    ),
                                                  ],
                                                ),
                                                Padding(
                                                  padding:
                                                      const EdgeInsets.fromLTRB(
                                                          8.0, 4.0, 8.0, 4.0),
                                                  child: Text(
                                                    products.getsalePriceQty(
                                                        cartList[position].qty),
                                                    style: blackH3Text,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  },
                                ),
                          Row(
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Total",
                                  style: blackH3Text,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  finalPrice == null ? "" : finalPrice,
                                  style: greyH4Text,
                                ),
                              )
                            ],
                          )
                        ],
                      ),
                    ),
                    Card(
                      margin: EdgeInsets.fromLTRB(0.0, 4.0, 0.0, 0.0),
                      child: Form(
                        key: _couponKey,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Row(
                                children: <Widget>[
                                  Text(
                                    "Coupon",
                                    style: blackH3Text,
                                  ),
                                ],
                              ),
                            ),
                            Row(
                              children: <Widget>[
                                Expanded(
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      controller: _couponController,
                                    ),
                                  ),
                                ),
                                ButtonTheme(
                                  buttonColor: Colors.red,
                                  child: RaisedButton(
                                    onPressed: () => _checkoutBloc.add(
                                        CheckingCoupon(_couponController.text)),
                                    child: Text(
                                      "Verify",
                                      style: TextStyle(color: Colors.white),
                                    ),
                                  ),
                                )
                              ],
                            )
                          ],
                        ),
                      ),
                    ),
                    Card(
                        margin: EdgeInsets.fromLTRB(0.0, 4.0, 0.0, 4.0),
                        child: Form(
                          key: _validationKey,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text("Shipping"),
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                      child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      controller: _firstNameController,
                                      validator: (String email) {
                                        return validateFirstName(email);
                                      },
                                      decoration: InputDecoration(
                                        labelText: "FirstName",
                                        border: textFieldBorder,
                                        contentPadding: EdgeInsets.all(16),
                                      ),
                                    ), //First Name
                                  )),
                                  Expanded(
                                      child: Padding(
                                    padding: const EdgeInsets.all(8.0),

                                    child: TextFormField(
                                      controller: _lastNameController,
                                      validator: (String email) {
                                        return validateLastName(email);
                                      },
                                      decoration: InputDecoration(
                                        labelText: "LastName",
                                        border: textFieldBorder,
                                        contentPadding: EdgeInsets.all(16),
                                      ),
                                    ), //Last Name
                                  )),
                                ],
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: TextFormField(
                                  controller: _emailNameController,
                                  validator: (String email) {
                                    return validateEmail(email);
                                  },
                                  decoration: InputDecoration(
                                    labelText: "Email",
                                    border: textFieldBorder,
                                    contentPadding: EdgeInsets.all(16),
                                  ),
                                ),
                              ), //email
                              Row(
                                children: <Widget>[
                                  Expanded(
                                      child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      enabled: false,
                                      decoration: InputDecoration(
                                        labelText: "Nepal",
                                        border: textFieldBorder,
                                        contentPadding: EdgeInsets.all(16),
                                      ),
                                    ), //Country
                                  )),
                                  Expanded(
                                      child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      controller: _zoneController,
                                      validator: (String email) {
                                        return validateZone(email);
                                      },
                                      decoration: InputDecoration(
                                        labelText: "Zone",
                                        border: textFieldBorder,
                                        contentPadding: EdgeInsets.all(16),
                                      ),
                                    ), //Zone
                                  )),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                      child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      controller: _districtController,
                                      validator: (String email) {
                                        return validateDistrict(email);
                                      },
                                      decoration: InputDecoration(
                                        labelText: "District",
                                        border: textFieldBorder,
                                        contentPadding: EdgeInsets.all(16),
                                      ),
                                    ), //district
                                  )),
                                  Expanded(
                                      child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      controller: _areaController,
                                      validator: (String email) {
                                        return validateArea(email);
                                      },
                                      decoration: InputDecoration(
                                        labelText: "Area",
                                        border: textFieldBorder,
                                        contentPadding: EdgeInsets.all(16),
                                      ),
                                    ), //area
                                  )),
                                ],
                              ),
                              Row(
                                children: <Widget>[
                                  Expanded(
                                      child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: TextFormField(
                                      controller: _phoneController,
                                      validator: (String email) {
                                        return validatePhone(email);
                                      },
                                      decoration: InputDecoration(
                                        labelText: "Phone",
                                        border: textFieldBorder,
                                        contentPadding: EdgeInsets.all(16),
                                      ),
                                    ), //mobile
                                  )),
                                ],
                              ),
                              Container(
                                width:MediaQuery.of(context).size.width ,
                                child: ButtonTheme(
                                  alignedDropdown: true,
                                  child: DropdownButton<String>(
                                    iconSize: 0.0,
                                    value: dropdownValue,
                                    onChanged: (String newValue) {
                                      _checkoutBloc.add(
                                          DropDownClickLocationType(newValue));
                                    },
                                    items: <String>['Home', 'Office']
                                        .map<DropdownMenuItem<String>>(
                                            (String value) {
                                      return DropdownMenuItem<String>(
                                        value: value,
                                        child: Text(value),
                                      );
                                    }).toList(),
                                  ),
                                ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width ,
                                child: isShippingPaymentLoaded == false
                                    ? Text("LOADING")
                                    : ButtonTheme(
                                        alignedDropdown: true,
                                        child: DropdownButton<String>(
                                          iconSize: 0.0,
                                          value: shippingPlaceValue,
                                          onChanged: (String newValue) {
                                            _checkoutBloc.add(
                                                DropDownShippingLocationClick(
                                                    newValue));
                                          },
                                          items: _shippingPlacesNameList
                                              .map<DropdownMenuItem<String>>(
                                                  (String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                        ),
                                      ),
                              ),
                              Container(
                                width: MediaQuery.of(context).size.width ,
                                child: isShippingPaymentLoaded == false
                                    ? Text("LOADING")
                                    : ButtonTheme(
                                        alignedDropdown: true,
                                        child: DropdownButton<String>(
                                          iconSize: 0.0,
                                          value: paymentTypeValue,
                                          onChanged: (String newValue) {
                                            _checkoutBloc.add(
                                                DropDownPaymentTypeClick(
                                                    newValue));
                                          },
                                          items: _paymentTypeList
                                              .map<DropdownMenuItem<String>>(
                                                  (String value) {
                                            return DropdownMenuItem<String>(
                                              value: value,
                                              child: Text(value),
                                            );
                                          }).toList(),
                                        ),
                                      ),
                              ),
                              ButtonTheme(
                                minWidth:MediaQuery.of(context).size.width ,
                                height: 50,
                                buttonColor: Colors.red,
                                child: RaisedButton(
                                    elevation: 0.0,
                                    child: Text(
                                      "CheckOut",
                                      style: whitesmallTextStyle,
                                    ),
                                    onPressed: () {
                                      if (_validationKey.currentState
                                          .validate()) {
                                        _checkoutBloc.add(CheckOutProduct(
                                            _firstNameController.text,
                                            _lastNameController.text,
                                            _emailNameController.text,
                                            _countryController.text,
                                            _zoneController.text,
                                            _districtController.text,
                                            _areaController.text,
                                            _phoneController.text));
//                                        _addressBloc.dispatch(Submit(
//                                            _firstNameController.text,
//                                            _lastNameController.text,
//                                            _emailnameController.text,
//                                            _zoneController.text,
//                                            _districtController.text,
//                                            _areaController.text,
//                                            _phoneController.text,
//                                            _countryController.text));
                                      }
                                    }),
                              ),
                            ],
                          ),
                        )),
                  ],
                ),
              );
            }),
      ),
    );
  }
}
