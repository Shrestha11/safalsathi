import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:safalsathi/AccountPage/bloc/account_page_bloc.dart';
import 'package:safalsathi/AccountPage/bloc/account_page_event.dart';

import 'package:safalsathi/AccountPage/bloc/account_page_state.dart';

import 'package:safalsathi/Login/LoginPage.dart';

//import 'package:smart_bazaar/Bargains/BargainListing.dart';


//import 'package:smart_bazaar/MainLogin/LoginRegister.dart';

import 'package:safalsathi/Login/LoginPage.dart';

import 'package:safalsathi/OrderHistory/purchaseHistory.dart';
import 'package:safalsathi/UserAddress/addressPage.dart';
//import 'package:smart_bazaar/aboutUs/dart.dart';
import 'package:safalsathi/changePassword/EditAccountSetting.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/home_screen/home.dart';
import 'package:safalsathi/legals/screens/legalsmain.dart';
import 'package:safalsathi/wishList/wishList.dart';

import 'accountSettingInnerPage/AccountSettingDetail.dart';
import 'accountSettingInnerPage/AccountSettingInner.dart';
import 'accountSettingInnerPage/bloc/account_detail_bloc.dart';
import 'accountSettingInnerPage/bloc/account_detail_event.dart';
import 'bloc/account_page_state.dart';

class Account extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AccountState();
  }
}

class _AccountState extends State<Account> {
  bool isNotificationOn = false;
  AccountPageBloc _accountPageBloc = AccountPageBloc();

  @override
  void initState() {
    super.initState();
    _accountPageBloc.add(Init());
  }

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => AccountDetailBloc(),
      child: BlocListener(
        bloc: _accountPageBloc,
        listener: (context, state) {
          if (state is ErrorState) {
            print("ERROR");
          }

          if (state is LoadedAccountState) {}
//        if (state is NotificationUpdated) isNotificationOn = state.isChecked;
        },
        child: BlocBuilder(
          bloc: _accountPageBloc,
          builder: (context, state) {
            if (state is LoadingState) {
              return Center(child: Text("Loading"));
            }
            if (state is LoggedInState) {
              BlocProvider.of<AccountDetailBloc>(context)
                  .add(LoadAccountDetail());
              return LoadedAccountState(_accountPageBloc, state.enterState);
            }
            //return LoadedAccountState(_accountPageBloc);
            return Container();
          },
        ),
      ),
    );
  }
}

class LoadedAccountState extends StatelessWidget {
  final AccountPageBloc accountPageBloc;

  final bool enterState;

  LoadedAccountState(this.accountPageBloc, this.enterState);

  @override
  Widget build(BuildContext context) {
    return Scaffold(

   /*   body: SingleChildScrollView(
        child: Column(
          children: <Widget>[

            AccountSettingUserImage(),
            NotificationView(),

            enterState == false?
            InkWell(
              onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                      builder: (context) => LoginPage())),
              child: Card(
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                      child: Text("Login"),
                    ),
                    Padding(
                      padding: const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                      child: Icon(
                        Icons.arrow_forward_ios,
                        color: Colors.grey,
                        size: 18.0,


                      ),
                    ),
              ],
                  ),),)

                : AccountSettingDetail(),*/




      body: SafeArea(
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Card(
                child: Row(
                  children: <Widget>[
                    Expanded(
                      child: enterState == false
                          ? InkWell(
                              onTap: () => Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => LoginPage(),
                                ),
                              ),
                              child: Container(
                                  child: Center(
                                    child: Text("Login"),
                                  ),
                                  height: 150.0),
                            )
                          : AccountSettingDetail(),
                    ),
                    Container(
                      child: AccountSettingUserImage(),
                      height: 150.0,
                      width: 150.0,
                    ),
                  ],
                ),
              ),
             // NotificationView(),


              enterState == true
                  ? InkWell(
                      onTap: () => Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => EditAccountSetting())),
                      child: Card(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  8.0, 12.0, 8.0, 12.0),
                              child: Text("Password"),
                            ),
                            Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  8.0, 12.0, 8.0, 12.0),
                              child: Icon(
                                Icons.arrow_forward_ios,
                                color: Colors.grey,
                                size: 18.0,
                              ),
                            ),
                          ],
                        ),
                      ),
                    )
                  : Container(),
              Card(
                margin: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: Column(
                  children: <Widget>[
                    InkWell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                            child: Text("My Address"),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                            child: Icon(
                              Icons.pin_drop,
                              color: Colors.grey,
                              size: 18.0,
                            ),
                          )
                        ],
                      ),
                      onTap: () => enterState == true
                          ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => Address()))
                          : buildSnackError(context, "User Not Logged in"),
                    ),
                    Container(
                      width: double.infinity,
                      height: 0.5,
                      color: Colors.grey.shade300,
                    ),
                    /*InkWell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                            child: Text("Bargains"),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                            child: Icon(
                              Icons.money_off,
                              color: Colors.grey,
                              size: 18.0,
                            ),
                          )
                        ],
                      ),
                     /* onTap: () => enterState == true
                          ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => BargainListing()))
                          : buildSnackError(context, "User Not Logged in"),*/
                    ),*/
                    Container(
                      width: double.infinity,
                      height: 0.5,
                      color: Colors.grey.shade300,
                    ),
                    InkWell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                            child: Text("WishList"),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                            child: Icon(
                              Icons.favorite_border,
                              color: Colors.grey,
                              size: 18.0,
                            ),
                          )
                        ],
                      ),
                      onTap: () => enterState == true
                          ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => WishList()))
                          : buildSnackError(context, "User Not Logged in"),
                    ),
                    Container(
                      width: double.infinity,
                      height: 0.5,
                      color: Colors.grey.shade300,
                    ),
                    InkWell(
                      onTap: () => enterState == true
                          ? Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => OrderHistory()))
                          : buildSnackError(context, "User Not Logged in"),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                            child: Text("My Orders"),
                          ),
                          Padding(
                            padding:
                                const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                            child: Icon(
                              Icons.shopping_basket,
                              color: Colors.grey,
                              size: 18.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 0.5,
                      color: Colors.grey.shade300,
                    ),
//                  InkWell(
//                    onTap: () => launchURL(registerAsSeller),
//                    child: Row(
//                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                      children: <Widget>[
//                        Padding(
//                          padding:
//                              const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
//                          child: Text("Register As Seller"),
//                        ),
//                        Padding(
//                          padding:
//                              const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
//                          child: Icon(
//                            Icons.verified_user,
//                            color: Colors.grey,
//                            size: 18.0,
//                          ),
//                        ),
//                      ],
//                    ),
//                  ),
                  ],
                ),
              ),
              Card(
                margin: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                child: Column(
                  children: <Widget>[
                    InkWell(
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Padding(
                            padding: EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                            child: Text("Privacy Policy"),
                          ),
                          Padding(
                            padding: EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                            child: Icon(
                              Icons.assignment,
                              color: Colors.grey,
                              size: 18.0,
                            ),
                          ),
                        ],
                      ),
                      onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LegalsMain(),
                        ),
                      ),
                    ),
                    Container(
                      width: double.infinity,
                      height: 0.5,
                      color: Colors.grey.shade300,
                    ),
                    InkWell(
                      /*onTap: () => Navigator.push(context,
                          MaterialPageRoute(builder: (context) => AboutUs())),*/
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(
                                  8.0, 12.0, 8.0, 12.0),
                              child: Text("About Us"),
                            ),
                          ),
                          Center(
                              child: Text(
                            "Version 1",
//    style: smallestText,
                          )),
                          Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Icon(
                              Icons.info_outline,
                              color: Colors.grey,
                              size: 18.0,
                            ),
                          ),
                        ],
                      ),
                    ),
                    enterState == true
                        ? Container(
                            width: double.infinity,
                            height: 0.5,
                            color: Colors.grey.shade300,
                          )
                        : Container(),
                    enterState == true
                        ? InkWell(
                            onTap: () {
                              logOut();
                              Navigator.pushAndRemoveUntil(
                                  context,
                                  MaterialPageRoute(
                                      builder: (BuildContext context) =>
                                          Home()),
                                  ModalRoute.withName('/'));
                            },
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.fromLTRB(
                                      8.0, 12.0, 8.0, 12.0),
                                  child: Text("Sign Out"),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Icon(
                                    Icons.power_settings_new,
                                    color: Colors.grey,
                                    size: 18.0,
                                  ),
                                ),
                              ],
                            ),
                          )
                        : Container()
                  ],
                ),
              ),

//                InkWell(
//                    onTap: () => _showSortDialog(context, _accoutBloc),
//                    child: Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: Text("Change Password"),
//                    )),
            ],
          ),
        ),
      ),
    );
  }
}

class NotificationView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return NotificationViewState();
  }
}

class NotificationViewState extends State<NotificationView> {
  bool isNotificationOn = false;

  @override
  void initState() {
    super.initState();
    /*OneSignal.shared.getPermissionSubscriptionState().then((e) {
      isNotificationOn = e.subscriptionStatus.subscribed;
    });*/
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(8.0, 0.0, 8.0, 0.0),
            child: Text("Notification"),
          ),
          Switch(
              onChanged: (status) {
                setState(
                  () {
                    isNotificationOn = status;
                    load(isNotificationOn);
                  },
                );
                setSharedPrefOnOff(status);
                //setNotificationOnOff();
              },
              value: isNotificationOn),
        ],
      ),
    );
  }

  Future<void> load(bool isNotificationOn) async {
    print("Value $isNotificationOn");
    //OneSignal.shared.setSubscription(isNotificationOn);
    print("Value $isNotificationOn");

//    OneSignal.shared.su
  }
}
