import 'package:meta/meta.dart';

@immutable
abstract class AccountDetailEvent {}

class LoadAccountDetail extends AccountDetailEvent {}
