import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/AccountPage/accountSettingInnerPage/model/UserImageResponse.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/error_handler.dart';
import 'package:safalsathi/common/generic_response.dart';
import 'package:safalsathi/common/string.dart';

import './bloc.dart';

class AccountDetailBloc extends Bloc<AccountDetailEvent, AccountDetailState> {
  @override
  AccountDetailState get initialState => InitialAccountDetailState();

  @override
  Stream<AccountDetailState> mapEventToState(
    AccountDetailEvent event,
  ) async* {
    AccountDetailRepository accountDetailRepository = AccountDetailRepository();
    // TODO: Add Logic
    if (event is LoadAccountDetail) {
      yield LoadingState();
      try {
        ApiResponse _apiResponse =
            await accountDetailRepository.loadUserImage();
        if (_apiResponse.apiCall == ApiResultStatus.ERROR) {
          print("TEST 1");
          yield LoadingState();
        } else if (_apiResponse.apiCall == ApiResultStatus.SUCCESS) {
          try {
            yield LoadedState(
                UserImageResponse.fromJsonMap(_apiResponse.responseData));
          } catch (e) {
            print("TEST 2");
            yield LoadingState();
          }
        } else if (_apiResponse.apiCall == ApiResultStatus.NO_NETWORK) {
          yield LoadingState();
        }
      } catch (e) {
        yield LoadingState();
      }
    }
  }
}

class AccountDetailRepository {
  AccountDetailApiProvider _accountDetailApiProvider =
      AccountDetailApiProvider();

  Future<ApiResponse> loadUserImage() {
    return _accountDetailApiProvider.getUserImage();
  }
}

class AccountDetailApiProvider {
  Future<ApiResponse> getUserImage() async {
    try {
      Response response = await dioBASE(await getAuth()).get(userImageUrl);
      print("Success $response");
      return ApiResponse(
          responseData: response.data,
          apiCall: ApiResultStatus.SUCCESS,
          message: "No Error");
    } on DioError catch (e) {
      print("DIO ERROR $e");
      if (e.type == DioErrorType.DEFAULT)
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.NO_NETWORK,
            message:
                "Make sure wifi or cellular data is turned on, Then try again");
//      else
      return ApiResponse(
          responseData: e,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(e));
    } catch (error) {
      print("DIO Api Error");

      return ApiResponse(
          responseData: error,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(error));
    }
  }
}
