import 'package:meta/meta.dart';
import 'package:safalsathi/AccountPage/accountSettingInnerPage/model/UserImageResponse.dart';

@immutable
abstract class AccountDetailState {}

class InitialAccountDetailState extends AccountDetailState {}

class LoadingState extends AccountDetailState {}

class LoadedState extends AccountDetailState {
  final UserImageResponse userImageResponse;

  LoadedState(this.userImageResponse);
}

