class UserImageResponse {
  final String image;
  final int id;
  final String firstName;
  final String lastName;
  final String userName;
  final String email;
  final String phone;
  var gender;
  final String dob;

  UserImageResponse.fromJsonMap(Map<String, dynamic> map)
      : image = map["image"],
        id = map["id"],
        firstName = map["first_name"],
        lastName = map["last_name"],
        userName = map["user_name"],
        email = map["email"],
        phone = map["phone"],
        gender = map["gender"],
        dob = map["dob"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['image'] = image;
    data['id'] = id;
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['user_name'] = userName;
    data['email'] = email;
    data['phone'] = phone;
    data['gender'] = gender == "" ? 0 : gender;
    data['dob'] = dob;
    return data;
  }
}
