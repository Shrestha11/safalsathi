import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/AccountPage/accountSettingInnerPage/bloc/account_detail_bloc.dart';
import 'package:safalsathi/AccountPage/accountSettingInnerPage/bloc/account_detail_event.dart';
import 'package:safalsathi/AccountPage/accountSettingInnerPage/bloc/account_detail_state.dart';
import 'package:safalsathi/AccountPage/accountSettingInnerPage/model/UserImageResponse.dart';
import 'package:safalsathi/accountSetting/account_setting.dart';

class AccountSettingDetail extends StatefulWidget {
  //final UserImageResponse userImageResponse;
  //AccountSettingDetail(this.userImageResponse);
  @override
  State<StatefulWidget> createState() {
    return AccountSettingState();
  }
}

class AccountSettingState extends State<AccountSettingDetail> {
  AccountDetailBloc _accountDetailBloc = AccountDetailBloc();
  String name, phone, email;
  UserImageResponse userImageResponse;

  @override
  void initState() {
    _accountDetailBloc.add(LoadAccountDetail());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: BlocProvider.of<AccountDetailBloc>(context),
      listener: (context, state) {
        if (state is LoadedState) {

         // name = state.userImageResponse.firstName +
           //   state.userImageResponse.lastName;
         // name = state.userImageResponse.userName;

          userImageResponse = state.userImageResponse;
         // name = state.userImageResponse.firstName +
           //   state.userImageResponse.lastName;
          name = state.userImageResponse.userName;

          phone = state.userImageResponse.phone;
          email = state.userImageResponse.email;
        }
      },
      child: BlocBuilder(
        bloc: BlocProvider.of<AccountDetailBloc>(context),
        builder: (context, state) {
          return InkWell(
           /* onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                  maintainState: false,
                  builder: (context) => AccountSetting(userImageResponse)),
            ),*/
            child: Column(
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding:
                            const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                        child:
                            Text(name == null ? "" : name),
                        //  child:Text(widget.userImageResponse.firstName),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding:
                            const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                        child: Text(phone == null ? "" : phone,
                            maxLines: 1, overflow: TextOverflow.ellipsis),
                      ),
                    ),
                  ],
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding:
                            const EdgeInsets.fromLTRB(8.0, 12.0, 8.0, 12.0),
                        child: Text(
                          email == null ? "" : email,
                          maxLines: 1,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
