import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/AccountPage/accountSettingInnerPage/bloc/account_detail_state.dart';
import 'package:safalsathi/AccountPage/accountSettingInnerPage/model/UserImageResponse.dart';
import 'package:safalsathi/common/string.dart';

import 'bloc/account_detail_bloc.dart';

class AccountSettingUserImage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return AccountSettingUserImageState();
  }
}

class AccountSettingUserImageState extends State<AccountSettingUserImage> {
  UserImageResponse userImageResponse;

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: BlocProvider.of<AccountDetailBloc>(context),
      listener: (context, state) {
        if (state is LoadedState) {
          userImageResponse = state.userImageResponse;
        }
      },
      child: BlocBuilder(
        bloc: BlocProvider.of<AccountDetailBloc>(context),
        builder: (context, state) {
          if (state is LoadingState)
            return Container(
              decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                      fit: BoxFit.fitHeight,
                      image: new AssetImage('images/account.png'))),
            );

          if (state is LoadedState) {
            return Container(
              decoration: new BoxDecoration(
                  shape: BoxShape.circle,
                  image: new DecorationImage(
                      fit: BoxFit.fill, image: new NetworkImage(tempImage2))),
            );
          }
          return Container(
            decoration: new BoxDecoration(
                shape: BoxShape.circle,
                image: new DecorationImage(
                    fit: BoxFit.fill, image: new NetworkImage(tempImage2))),
          );
        },
      ),
    );
  }
}
