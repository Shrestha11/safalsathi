import 'package:meta/meta.dart';
import 'package:safalsathi/AccountPage/accountSettingInnerPage/model/UserImageResponse.dart';

@immutable
abstract class AccountPageState {}

class InitialAccountPageState extends AccountPageState {}

class LoggedInState extends AccountPageState {

  bool enterState;
  LoggedInState(this.enterState);
}

class NotificationUpdated extends AccountPageState {
  final bool isChecked;

  NotificationUpdated(this.isChecked);
}

class NotLoggedInState extends AccountPageState {}

class LoadingState extends AccountPageState {}

class ErrorState extends AccountPageState {}
