import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/AccountPage/accountSettingInnerPage/model/UserImageResponse.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/common_ui.dart';

import './bloc.dart';

class AccountPageBloc extends Bloc<AccountPageEvent, AccountPageState> {
  @override
  AccountPageState get initialState => InitialAccountPageState();
  UserImageResponse _userImageResponse;

  @override
  Stream<AccountPageState> mapEventToState(
    AccountPageEvent event,
  ) async* {
    if (event is Init) {
      //check logged in status
      String msg = await getAuth();
      print("IS USER LOGGED IN ???? $msg");
      if (await getAuth() != null) {
        bool enterState = true;
        // _userImageResponse = await _loadUserImage();
        yield LoggedInState(enterState);
      } else {
        bool enterState = false;
        yield LoggedInState(enterState);
      }
    }
    if (event is TriggerNotification) {
      yield LoadingState();
      try {
        //Disable Notification
        yield NotificationUpdated(event.isChecked);
      } catch (e) {
        print(e.toString());
      }
    }
  }
}

Future<UserImageResponse> _loadUserImage() async {
  Response response = await dioBASE(await getAuth()).get("/user-image");
  print("RESPO $response");
  return UserImageResponse.fromJsonMap(response.data);
}
