import 'package:meta/meta.dart';

@immutable
abstract class AccountPageEvent {}

class Init extends AccountPageEvent {}

class TriggerNotification extends AccountPageEvent {
  final bool isChecked;

  TriggerNotification(this.isChecked);
}
