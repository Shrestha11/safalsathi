import 'package:dio/dio.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/error_handler.dart';
import 'package:safalsathi/common/generic_response.dart';
import 'package:safalsathi/common/string.dart';

class HomeRepository {
  HomeDataApiProvider _apiProvider = HomeDataApiProvider();

  Future<ApiResponse> getHome() {
    return _apiProvider.getHome();
  }

  Future<ApiResponse> loadPage(int currentPage) {
    return _apiProvider.loadPage(currentPage);
  }
}

class HomeDataApiProvider {
  Future<ApiResponse> getHome() async {
    try {
      Response response = await dioBASE().get(urlHome);
      return ApiResponse(
          responseData: response.data,
          apiCall: ApiResultStatus.SUCCESS,
          message: "No Error");
    } on DioError catch (e) {
      print("DIO ERROR $e");
      if (e.type == DioErrorType.DEFAULT)
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.NO_NETWORK,
            message:
                "Make sure wifi or cellular data is turned on, Then try again");
//      else
      return ApiResponse(
          responseData: e,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(e));
    } catch (error) {
      print("DIO Api Error");

      return ApiResponse(
          responseData: error,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(error));
    }
  }

  Future<ApiResponse> loadPage(int currentPage) async {
    try {
      Response response =
          await dioBASE().get(productSectionUrl + currentPage.toString());
      print("LOAD PAGE RESPONSE $response");
      return ApiResponse(
          responseData: response.data,
          apiCall: ApiResultStatus.SUCCESS,
          message: "No Error");
    } on DioError catch (e) {
      print("DIO ERROR $e");
      if (e.type == DioErrorType.DEFAULT)
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.NO_NETWORK,
            message:
                "Make sure wifi or cellular data is turned on, Then try again");
//      else
      return ApiResponse(
          responseData: e,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(e));
    } catch (error) {
      return ApiResponse(
          responseData: error,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(error));
    }
  }
}
