import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:safalsathi/common/generic_response.dart';
import 'package:safalsathi/home_main/model/HomeDataResponse.dart';
import 'package:safalsathi/home_main/model/loadProductSection/ProductSectionList.dart';
import 'package:safalsathi/home_main/repository/home_repository.dart';

import './bloc.dart';

class HomeBloc extends Bloc<HomeEvent, HomeState> {
  HomeRepository _homeRepository = HomeRepository();
  int currentPage = 1;
  bool isLoading = false;
  int lastPage = 1;

  @override
  HomeState get initialState => InitialHomeState();

  @override
  Stream<HomeState> mapEventToState(
    HomeEvent event,
  ) async* {
    if (event is LoadHomeEvent) {
      yield LoadingHomeState();
      try {
        ApiResponse _apiResponse = await _homeRepository.getHome();
        if (_apiResponse.apiCall == ApiResultStatus.ERROR) {
          yield ErrorHomeState(_apiResponse.message);
        } else if (_apiResponse.apiCall == ApiResultStatus.SUCCESS) {
          try {
            yield LoadedHomeState(
                HomeDataResponse.fromJsonMap(_apiResponse.responseData));
          } catch (e) {
            print("ERROR PARSING $e");
          }
        } else if (_apiResponse.apiCall == ApiResultStatus.NO_NETWORK) {
          yield ErrorHomeState(_apiResponse.message);
        }
      } catch (e) {
        yield ErrorHomeState(e.toString());
      }
    }
    if (event is RetryEvent) {
//      yield LoadingHomeState();
      try {
        ApiResponse _apiResponse = await _homeRepository.getHome();
        if (_apiResponse.apiCall == ApiResultStatus.ERROR) {
          yield RefreshFailed(_apiResponse.message);
        } else if (_apiResponse.apiCall == ApiResultStatus.SUCCESS) {
          //remove all updated list
          currentPage = 1;
          yield LoadedHomeState(
              HomeDataResponse.fromJsonMap(_apiResponse.responseData));
        } else if (_apiResponse.apiCall == ApiResultStatus.NO_NETWORK) {


          yield ErrorHomeState(_apiResponse.message);
        }
      } catch (e) {
        yield ErrorHomeState(e.toString());
      }
    }
    /*if (event is LoadNextPage) {
      if (!isLoading && currentPage <= lastPage) {
        yield ProductSectionLoading();

        isLoading = true;
        try {
          ApiResponse _apiResponse =
              await _homeRepository.loadPage(currentPage);
          print("RESPONSE CODE ${_apiResponse.apiCall}");
          if (_apiResponse.apiCall == ApiResultStatus.ERROR) {
            print("GOING HERE? ERRROR");
            yield ErrorHomeState(_apiResponse.message);
          } else if (_apiResponse.apiCall == ApiResultStatus.SUCCESS) {
            print("GOING HERE? SUCCESS");

            ProductSectionList _productSectionList;
            try {
              _productSectionList =
                  ProductSectionList.fromJsonMap(_apiResponse.responseData);
              lastPage = _productSectionList.product_sections.last_page;
              yield ProductSectionAdded(
                  _productSectionList.product_sections.data);
            } catch (e) {
              yield ErrorHomeState(e.toString());

              print("ERROR $e");
            }
          } else if (_apiResponse.apiCall == ApiResultStatus.NO_NETWORK) {
            print("GOING HERE? NO NETWORK");
            isLoading = false;
            yield ErrorHomeState(_apiResponse.message);
          }
        } catch (e) {
          isLoading = false;
          print("FINAL ERROR CATCH$e");
          yield ErrorHomeState(e.toString());
        }
        isLoading = false;
        currentPage = currentPage + 1;
      } else
        yield EndOfPageState();
    }*/
  }
}
