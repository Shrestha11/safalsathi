import 'package:meta/meta.dart';
import 'package:safalsathi/home_main/model/HomeDataResponse.dart';
import 'package:safalsathi/home_main/model/loadProductSection/data.dart';

@immutable
abstract class HomeState {}

class InitialHomeState extends HomeState {}

class LoadingHomeState extends HomeState {}

class LoadedHomeState extends HomeState {
  final HomeDataResponse homeDataResponse;

  LoadedHomeState(this.homeDataResponse);
}

class ProductSectionLoading extends HomeState {}

class EndOfPageState extends HomeState {}

class ProductSectionAdded extends HomeState {
  final List<Data> dataList;

  ProductSectionAdded(this.dataList);
}

class ErrorHomeState extends HomeState {
  final String error;

  ErrorHomeState(this.error);
}

class RefreshFailed extends HomeState {
  final String msg;

  RefreshFailed(this.msg);
}

