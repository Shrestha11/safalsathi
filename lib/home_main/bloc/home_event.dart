import 'package:meta/meta.dart';

@immutable
abstract class HomeEvent {}

class LoadHomeEvent extends HomeEvent {}

class RetryEvent extends HomeEvent {}

class LoadNextPage extends HomeEvent{}
