import 'package:safalsathi/common/model/products.dart';

class Superstore {
  final List<Products> products;

  Superstore.fromJsonMap(Map<String, dynamic> map)
      : products = List<Products>.from(
            map["products"].map((it) => Products.fromJsonMap(it)));

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['products'] =
        products != null ? this.products.map((v) => v.toJson()).toList() : null;
    return data;
  }
}
