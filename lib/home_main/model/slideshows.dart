
class Slideshows {

  final int id;
  final String name;
  final String link;
  final int status;
  final int option;
  final int priority;
  final String image;
  final String created_at;
  final String updated_at;

	Slideshows.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		name = map["name"],
		link = map["link"],
		status = map["status"],
		option = map["option"],
		priority = map["priority"],
		image = map["image"],
		created_at = map["created_at"],
		updated_at = map["updated_at"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['name'] = name;
		data['link'] = link;
		data['status'] = status;
		data['option'] = option;
		data['priority'] = priority;
		data['image'] = image;
		data['created_at'] = created_at;
		data['updated_at'] = updated_at;
		return data;
	}
}
