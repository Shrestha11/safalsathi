import 'package:safalsathi/common/model/products.dart';

class Products_all {
  final List<Products> latestProducts;
  final List<Products> featuredProducts;
  final List<Products> recentlyViewedProducts;

  Products_all.fromJsonMap(Map<String, dynamic> map)
      : latestProducts = List<Products>.from(
            map["latestProducts"].map((it) => Products.fromJsonMap(it))),
        featuredProducts = List<Products>.from(
            map["featuredProducts"].map((it) => Products.fromJsonMap(it))),
        recentlyViewedProducts = List<Products>.from(
            map["recentlyViewedProducts"]
                .map((it) => Products.fromJsonMap(it)));

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['latestProducts'] = latestProducts != null
        ? this.latestProducts.map((v) => v.toJson()).toList()
        : null;
    data['featuredProducts'] = featuredProducts != null
        ? this.featuredProducts.map((v) => v.toJson()).toList()
        : null;
    data['recentlyViewedProducts'] = recentlyViewedProducts != null
        ? this.recentlyViewedProducts.map((v) => v.toJson()).toList()
        : null;
    return data;
  }
}
