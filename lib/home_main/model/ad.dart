
class Ad {

  final String image;
  final String link;

	Ad.fromJsonMap(Map<String, dynamic> map): 
		image = map["image"],
		link = map["link"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['image'] = image;
		data['link'] = link;
		return data;
	}
}
