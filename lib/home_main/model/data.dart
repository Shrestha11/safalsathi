import 'package:safalsathi/home_main/model/products_all.dart';
import 'package:safalsathi/home_main/model/slideshows.dart';
import 'package:safalsathi/home_main/model/superstore.dart';

import 'categories.dart';
import 'deal_of_the_day.dart';

class Data {
  final List<Slideshows> slideshows;
  final Deal_of_the_day deal_of_the_day;
  final Products_all products_all;
  final Superstore superstore;
  final List<Categories> categories;
  final String logo;

//  final List<Products_sections> products_sections;

  Data.fromJsonMap(Map<String, dynamic> map)
      : slideshows = List<Slideshows>.from(
            map["slideshows"].map((it) => Slideshows.fromJsonMap(it))),
        deal_of_the_day = Deal_of_the_day.fromJsonMap(map["deal_of_the_day"]),
        products_all = Products_all.fromJsonMap(map["products_all"]),
        superstore = Superstore.fromJsonMap(map["superstore"]),
        categories = List<Categories>.from(
            map["categories"].map((it) => Categories.fromJsonMap(it))),
        logo = map["logo"];

//		products_sections = List<Products_sections>.from(map["products_sections"].map((it) => Products_sections.fromJsonMap(it)));

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['slideshows'] = slideshows != null
        ? this.slideshows.map((v) => v.toJson()).toList()
        : null;
    data['deal_of_the_day'] =
        deal_of_the_day == null ? null : deal_of_the_day.toJson();
    data['products_all'] = products_all == null ? null : products_all.toJson();
    data['superstore'] = superstore == null ? null : superstore.toJson();
    data['categories'] = categories != null
        ? this.categories.map((v) => v.toJson()).toList()
        : null;
    data['logo'] = logo;
//		data['products_sections'] = products_sections != null ?
//			this.products_sections.map((v) => v.toJson()).toList()
//			: null;
    return data;
  }
}
