import 'package:safalsathi/home_main/model/loadProductSection/product_sections.dart';

class ProductSectionList {

  Product_sections product_sections;

	ProductSectionList.fromJsonMap(Map<String, dynamic> map): 
		product_sections = Product_sections.fromJsonMap(map["product_sections"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['product_sections'] = product_sections == null ? null : product_sections.toJson();
		return data;
	}
}
