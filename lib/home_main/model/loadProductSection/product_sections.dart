import 'package:safalsathi/home_main/model/loadProductSection/data.dart';

class Product_sections {
  int current_page;
  List<Data> data;
  String first_page_url;
  int from;
  int last_page;
  String last_page_url;
  String next_page_url;
  String path;
  int per_page;
  Object prev_page_url;
  int to;
  int total;

  Product_sections.fromJsonMap(Map<String, dynamic> map)
      : current_page = map["current_page"],
        data = List<Data>.from(map["data"].map((it) => Data.fromJsonMap(it))),
        first_page_url = map["first_page_url"],
        from = map["from"],
        last_page = map["last_page"],
        last_page_url = map["last_page_url"],
        next_page_url = map["next_page_url"],
        path = map["path"],
        per_page = map["per_page"],
        prev_page_url = map["prev_page_url"],
        to = map["to"],
        total = map["total"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> datas = new Map<String, dynamic>();
    datas['current_page'] = current_page;
    datas['data'] =
        data != null ? this.data.map((v) => v.toJson()).toList() : null;
    datas['first_page_url'] = first_page_url;
    datas['from'] = from;
    datas['last_page'] = last_page;
    datas['last_page_url'] = last_page_url;
    datas['next_page_url'] = next_page_url;
    datas['path'] = path;
    datas['per_page'] = per_page;
    datas['prev_page_url'] = prev_page_url;
    datas['to'] = to;
    datas['total'] = total;
    return datas;
  }
}
