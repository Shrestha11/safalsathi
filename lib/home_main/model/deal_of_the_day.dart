import 'package:safalsathi/common/model/products.dart';

class Deal_of_the_day {
  final List<Products> deals_product;
  final String deals_date;
  final int category_id;

  Deal_of_the_day.fromJsonMap(Map<String, dynamic> map)
      : deals_product = List<Products>.from(
            map["deals_product"].map((it) => Products.fromJsonMap(it))),
        deals_date = map["deals_date"],
        category_id = map["category_id"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['deals_product'] = deals_product != null
        ? this.deals_product.map((v) => v.toJson()).toList()
        : null;
    data['deals_date'] = deals_date;
    data['category_id'] = category_id;
    return data;
  }
}
