

import 'package:safalsathi/common/model/products.dart';

import 'ad.dart';

class Products_sections {

  final List<Products> products;
  final String title;
  final int category_id;
  final Ad ad;

	Products_sections.fromJsonMap(Map<String, dynamic> map): 
		products = List<Products>.from(map["products"].map((it) => Products.fromJsonMap(it))),
		title = map["title"],
		category_id = map["category_id"],
		ad = Ad.fromJsonMap(map["ad"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['products'] = products != null ? 
			this.products.map((v) => v.toJson()).toList()
			: null;
		data['title'] = title;
		data['category_id'] = category_id;
		data['ad'] = ad == null ? null : ad.toJson();
		return data;
	}
}
