
import 'data.dart';

class HomeDataResponse {

  final Data data;

	HomeDataResponse.fromJsonMap(Map<String, dynamic> map): 
		data = Data.fromJsonMap(map["data"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> datas = new Map<String, dynamic>();
		datas['data'] = data == null ? null : data.toJson();
		return datas;
	}
}
