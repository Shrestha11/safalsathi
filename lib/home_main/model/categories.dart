import 'dart:convert';

List<Categories> welcomeFromJson(String str) => List<Categories>.from(
    json.decode(str).map((x) => Categories.fromJsonMap(x)));

String welcomeToJson(List<Categories> data) =>
    json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Categories {
  int id;
  String name;
  String slug;
  String description;
  int parentId;
  String image;
  String createdAt;
  String updatedAt;
  List<SubCategory> subCategory;
  String products;

  Categories({
    this.id,
    this.name,
    this.slug,
    this.description,
    this.parentId,
    this.image,
    this.createdAt,
    this.updatedAt,
    this.subCategory,
    this.products,
  });

  factory Categories.fromJsonMap(Map<String, dynamic> json) => Categories(
        id: json["id"],
        name: json["name"],
        slug: json["slug"],
        description: json["description"] == null ? null : json["description"],
        parentId: json["parent_id"],
        image: json["image"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        subCategory: List<SubCategory>.from(
            json["subCategory"].map((x) => SubCategory.fromJsonMap(x))),
        products: json["products"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "slug": slug,
        "description": description == null ? null : description,
        "parent_id": parentId,
        "image": image,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "subCategory": List<dynamic>.from(subCategory.map((x) => x.toJson())),
        "products": products,
      };
}

class SubCategory {
  int id;
  String name;
  String slug;
  String description;
  int parentId;
  String image;
  String createdAt;
  String updatedAt;
  List<SubCategory> subCategory;
  String img;
  String products;

  SubCategory({
    this.id,
    this.name,
    this.slug,
    this.description,
    this.parentId,
    this.image,
    this.createdAt,
    this.updatedAt,
    this.subCategory,
    this.img,
    this.products,
  });

  factory SubCategory.fromJsonMap(Map<String, dynamic> json) => SubCategory(
        id: json["id"],
        name: json["name"],
        slug: json["slug"],
        description: json["description"] == null ? null : json["description"],
        parentId: json["parent_id"],
        image: json["image"],
        createdAt: json["created_at"],
        updatedAt: json["updated_at"],
        subCategory: List<SubCategory>.from(
            json["subCategory"].map((x) => SubCategory.fromJsonMap(x))),
        img: json["img"] == null ? null : json["img"],
        products: json["products"] == null ? null : json["products"],
      );

  Map<String, dynamic> toJson() => {
        "id": id,
        "name": name,
        "slug": slug,
        "description": description == null ? null : description,
        "parent_id": parentId,
        "image": image,
        "created_at": createdAt,
        "updated_at": updatedAt,
        "subCategory": List<dynamic>.from(subCategory.map((x) => x.toJson())),
        "img": img == null ? null : img,
        "products": products == null ? null : products,
      };
}
