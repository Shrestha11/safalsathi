
class Pivot {

  final int deal_id;
  final int product_id;

	Pivot.fromJsonMap(Map<String, dynamic> map): 
		deal_id = map["deal_id"],
		product_id = map["product_id"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['deal_id'] = deal_id;
		data['product_id'] = product_id;
		return data;
	}
}
