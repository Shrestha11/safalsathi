import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';
import 'package:safalsathi/ViewMore/CategoriesDetail.dart';
import 'package:safalsathi/common/color.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/model/products.dart';
import 'package:safalsathi/common/string.dart';
import 'package:safalsathi/home_main/model/categories.dart';

import 'package:safalsathi/categoryItem/CategoryItem.dart';

import 'package:safalsathi/product_detail/productDetail.dart';
import 'package:safalsathi/search/SearchPage.dart';

import 'bloc/home_bloc.dart';
import 'bloc/home_event.dart';
import 'bloc/home_state.dart';
import 'home_main_widget.dart';
import 'model/HomeDataResponse.dart';
import 'model/loadProductSection/data.dart';

class HomeMain extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return HomePageState();
  }
}

class HomePageState extends State<HomeMain> {
  HomeBloc homeBloc = HomeBloc();
  //bool isLoading = true;
  //bool isEndOfPage = false;
  RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  ScrollController _scrollController = new ScrollController();
  List<Data> _dataProductList;
  HomeDataResponse _homeDataResponse;

  @override
  void dispose() {
    _scrollController.dispose();
    _refreshController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    homeBloc.add(LoadHomeEvent());

   /* _scrollController.addListener(
      () {
        if (_scrollController.position.pixels ==
            _scrollController.position.maxScrollExtent) {
          _getMoreData();
        }
      },
    );*/
  }

 /* void _getMoreData() {
    homeBloc.add(LoadNextPage());
  }*/

  void _reload() {
    homeBloc.add(RetryEvent());
  }

  @override
  Widget build(BuildContext context2) {
    return  BlocListener(
      bloc: homeBloc,
      listener: (context, state) {
        if (state is LoadedHomeState) {
          _refreshController.refreshCompleted();
          _homeDataResponse = state.homeDataResponse;
          _dataProductList = [];
        }
     //   if (state is ProductSectionLoading) isLoading = true;
       // if (state is EndOfPageState) isEndOfPage = true;
       /* if (state is ProductSectionAdded) {
          isLoading = false;
          if (_dataProductList == null || _dataProductList.length == 0)
            _dataProductList = state.dataList;
          else
            _dataProductList.addAll(state.dataList);
        }*/
        if (state is RefreshFailed) {
          _refreshController.refreshFailed();
          //TOAST MESSAGE xc \
        }
      },
      child: BlocBuilder(
        bloc: homeBloc,
        builder: (context, state) {
          if (state is LoadingHomeState) {
            return buildLoadingWidget(context);
          }
          if (state is ErrorHomeState) {
            return Center(
              child: ListView(
                shrinkWrap: true,
                children: <Widget>[
                  Center(
                    child: Text(
                      "Oops, No Internet Connection",
                      style: Theme.of(context).textTheme.headline,
                    ),
                  ),
                  SizedBox(
                    height: 25.0,
                  ),
                  Center(
                      child: Text(
                          "Please Make sure you are connected to the internet")),
                  SizedBox(
                    height: 25.0,
                  ),
                  Center(
                    child: RaisedButton(
                      shape: RoundedRectangleBorder(
                        borderRadius: new BorderRadius.circular(12.0),
                      ),
                      onPressed: () => homeBloc.add(RetryEvent()),
                      child: Padding(
                        padding: const EdgeInsets.all(12.0),
                        child: Text(
                          "TRY AGAIN",
                        ),
                      ),
                    ),
                  )
                ],
              ),
            );
          }
          return

          Column(
            mainAxisSize: MainAxisSize.max,
          children:<Widget>[
          Stack(
          children: <Widget>[
            topBar(),

            Positioned(
                right:30,
                top:30,

                child:InkWell(

                onTap: ()=>Navigator.push(
          context,
          MaterialPageRoute(builder: (context) => SearchPageStart()),
          ),

          child:Icon(Icons.search),
                )
            ),
            Positioned(
              left:10,
              top:30,
              child:Container(
                height: 40.0,
                child: Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.fromLTRB(16.0, 8.0, 8.0, 8.0),
                      child: Image.asset('images/safal.jpeg'),
                    ),
                   /* Expanded(
                      child: Container(),
                    ),*/
                  ],
                ),
              ),
            ),

        ]
          ),

            Stack(
            children: <Widget>[
             // backGround(context),
             // topBar(),


              SafeArea(

                child:Container(
                  height:488,//MediaQuery.of(context).size.height,
                  width:MediaQuery.of(context).size.width,

                  child: Column(
                    children: <Widget>[
                     // CustomAppBar(),

                    //  search(context),

                    //  search(context),
                    //CustomSearchBar(_homeDataResponse.data.categories),
                      Expanded(
                        child: SmartRefresher(
                          controller: _refreshController,
                          onRefresh: _reload,
                          enablePullDown: true,
                          header: WaterDropHeader(),
                          child: ListView(
                            addAutomaticKeepAlives: false,
                            shrinkWrap: true,
                            controller: _scrollController,
                            children: <Widget>[
                             // SizedBox(height:40),
                              _homeDataResponse.data.slideshows.length == 0
                                  ? Container()
                                  : carousel(_homeDataResponse.data.slideshows,
                                      context),
                              SizedBox(height:20),
                              categories(_homeDataResponse.data.categories),
                              LoadProductListing(
                                  _homeDataResponse
                                      .data.deal_of_the_day.deals_product,
                                  "Deal Of The Day",
                                  0,
                                  'images/fire.png'),
                              LoadProductListing(
                                  _homeDataResponse.data.superstore.products,
                                  "Super Store"),
                              LoadProductListing(
                                _homeDataResponse
                                    .data.products_all.recentlyViewedProducts,
                                "Recently Viewed Products",
                              ),
                              LoadProductListing(
                                _homeDataResponse
                                    .data.products_all.latestProducts,
                                "Latest Products",
                              ),
                              LoadProductListing(
                                  _homeDataResponse
                                      .data.products_all.featuredProducts,
                                  "Featured Products"),

                              ListView.builder(
                               itemCount:
                               _dataProductList == null
                                    ? 0
                                   :
                                _dataProductList.length,
                                shrinkWrap: true,
                                physics: ClampingScrollPhysics(),
                                itemBuilder: (context, i) {
                                  return LoadProductListing(

                                      _dataProductList[i].products,
                                      "${_dataProductList[i].title}",
                                      _dataProductList[i].category_id);
                                },
                              ),
                              /*isEndOfPage
                                  ? Container(
                                      color: AppColors.GREY,
                                      child: Center(
                                        child: Text(
                                          "END OF PAGE",
                                          style: Theme.of(context)
                                              .textTheme
                                              .title
                                              .copyWith(color: AppColors.WHITE),
                                        ),
                                      ))
                                  : isLoading
                                      ? Row(
                                          children: <Widget>[
                                            Expanded(
                                              child: Container(),
                                            ),
                                            CircularProgressIndicator(),
                                            Expanded(
                                              child: Container(),
                                            ),
                                          ],
                                        )
                                      : Container(),*/
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),

              ),
            ],
          ),
          ],
          );

        },
      ),
    );
  }
}
search(BuildContext context){
  return
   InkWell(
     onTap: ()=>Navigator.push(
       context,
       MaterialPageRoute(builder: (context) => SearchPageStart()),
     ),

  child: Icon(Icons.search));
}
topBar(){
  return Container(
    height: 80,

    decoration: new BoxDecoration(
      gradient: new LinearGradient(
        begin: FractionalOffset.topLeft,
        end: FractionalOffset.bottomRight,
        // colors: [AppColors.GRADIENT_START, AppColors.GRADIENT_END],
        colors:[Colors.deepOrange,Colors.orange],
      ),
    ),
  );
}
categories(List<Categories> categories) {
  return Padding(
    padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 8.0),
    child: Container(
        height: 42.0,
        child: ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: categories.length,
            itemBuilder: (BuildContext context, int index) {
              return GestureDetector(
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => CategoryItem(
                          categories[index].subCategory,
                          categories[index].name),
                    ),
                  );
                },
                child: Container(
                    child: Column(children: <Widget>[
                      Expanded(
                      child:Card(
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(20.0),
                        ),
                        color:Colors.deepOrange ,
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            categories[index].name,
                            textAlign: TextAlign.end,
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ),
              ),
                    ])),
              );
            })),
  );
}

class LoadProductListing extends StatelessWidget {
  final List<Products> products;
  final String name;
  final String imageLocationURI;
  final int productId;

  LoadProductListing(this.products, this.name,
      [this.productId = 0, this.imageLocationURI = ""]);

  @override
  Widget build(BuildContext context) {
    return products.length == 0
        ? Container()
        : Column(
            children: <Widget>[
              hotDeals3(name, context, imageLocationURI),
              productLists(products, context)
            ],
          );
  }

  hotDeals3(String s, BuildContext context, String s2) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text(s,
                style: Theme.of(context)
                    .textTheme
                    .display1
                    .copyWith(color: Colors.black)),
          ),
          Expanded(
            child: Container(),
          ),
          productId == 0
              ? Container()
              :
          Container(
                  height: 25.0,
                  child: RaisedButton(
                   onPressed: () => Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => CategoriesPage(
                            /*dealOfTheDay.category_id*/
                            productId,
                            name),
                      ),
                    ),
                    color:Colors.red,
                    padding: new EdgeInsets.all(0.0),
                    textColor: AppColors.WHITE,
                    child: Text(
                      "View All",
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                ),
        ],
      ),
    );
  }

  productLists(List<Products> dealsProduct, BuildContext context2) {
    return Container(
      height: 280.0,
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: dealsProduct.length,
        itemBuilder: (BuildContext context, int index) {
          return ProductItemDisplay(dealsProduct[index]);
        },
      ),
    );
  }
}

class ProductItemDisplay extends StatelessWidget {
  final Products products;

  ProductItemDisplay(this.products);

  @override
  Widget build(BuildContext context) {
    int discountValue =
        discountPrice(products.productPrice, products.salesPrice);
    return InkWell(
      child: Container(
        width: 200.0,
       //height:50,
        child: Stack(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(2.0),
              child: Card(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child:
                     /* products.negotiable == 0
                          ? Banner(
                              message: bargain,
                              location: BannerLocation.topStart,
                              color: AppColors.BANNER_COLOR,
                              child: Container(
                                child: Center(
                                    child: Padding(
                                        padding: const EdgeInsets.all(2.0),
                                        child: LoadImageAsync(
                                            products.imgs[0].smallUrl)
//                                    Image.network(
//                                  products.imgs[0].smallUrl,
//                                    fit: BoxFit.fitHeight,
//                                  ),
                                        )),
                              ),
                            )
                          :*/
                      Container(
                        height: MediaQuery.of(context).size.width,
                        width: MediaQuery.of(context).size.width,
                             // child: Center(
                               //   child: Padding(
                                //padding: const EdgeInsets.all(2.0),
                                child: Image.network(
                                  products.imgs[0].smallUrl,
                                  fit: BoxFit.fitHeight,
                                ),
                              )
                              //),
                            //),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Text(
                        products.name,
                        maxLines: 1,
                        textAlign: TextAlign.left,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.body1,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Text(prices(products.productPrice),
                          style: Theme.of(context).textTheme.body2.copyWith(
                              decoration: TextDecoration.lineThrough,
                              color: Colors.grey)),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                      child: Text(prices(products.salesPrice),
                          style: Theme.of(context).textTheme.display1),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        recentlyViewed(products.view),
                        style: Theme.of(context).textTheme.display2,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            discountValue != 0
                ? Positioned(
                    right: 0.0,
                    top: 0.0,
                    child: Stack(
                      children: <Widget>[
                        Image.asset(
                          'images/price_tag.png',
                          height: 42.0,
                          width: 42.0,
                        ),
                        Positioned(
                          top: 17.0,
                          right: 20.0,
                          child: RotationTransition(
                            turns: new AlwaysStoppedAnimation(315 / 360),
                            child: Text(
                              '$discountValue%',
                              style: Theme.of(context)
                                  .textTheme
                                  .display2
                                  .copyWith(
                                      color: AppColors.WHITE, fontSize: 8.0),
                            ),
                          ),
                        )
                      ],
                    ),
                  )
                : Container()
          ],
        ),
      ),
      onTap: () => Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) => ProductDetail(
            products: products,
          ),
        ),
      ),
    );
  }
}
