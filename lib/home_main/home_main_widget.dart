import 'dart:ui';

import 'package:carousel_pro/carousel_pro.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:safalsathi/common/model/imgs.dart';

import 'model/slideshows.dart';

carousel(List<Slideshows> slideshows, BuildContext context) {
  var list2 = [];
  for (int i = 0; i < slideshows.length; i++) {
    var a;
    try {
      a = Image.network(slideshows[i].image,height: 150,fit: BoxFit.fitWidth);
    } catch (e) {
      a = Image.asset('images/fire.png');
    }
    list2.add(a);
  }
  Widget imageCarousel =
     // Card(
      //child: new
      Container(
    height: MediaQuery.of(context).size.width* (144 / 500),
    width: MediaQuery.of(context).size.width,
       // height:100,
        //width: MediaQuery.of(context).size.width,


    child: Carousel(
      autoplay: true,
      //boxFit: BoxFit.fill,
      autoplayDuration: Duration(seconds: 4),
      images: list2,
      dotSize: 2.0,
      dotBgColor: Colors.transparent,
      showIndicator: true,
      defaultImage: Icons.add,
    ),
  );
  //);
  return imageCarousel;
}

detailCarousel(List<Imgs> slideshows, BuildContext context) {
  var list2 = [];
  for (int i = 0; i < slideshows.length; i++) {
    var a;
    try {
      a = Image.network(slideshows[i].largeUrl);
    } catch (e) {
      a = Image.asset('images/fire.png');
    }
    list2.add(a);
  }
  Widget imageCarousel = Card(
      child: new Container(
    height: MediaQuery.of(context).size.width,
    width: MediaQuery.of(context).size.width,
    child: Carousel(
      autoplay: true,
      boxFit: BoxFit.fill,
      autoplayDuration: Duration(seconds: 4),
      images: list2,
      dotSize: 2.0,
      dotBgColor: Colors.transparent,
      showIndicator: true,
      defaultImage: Icons.add,
    ),
  ));
  return imageCarousel;
}
