import 'dart:ui';

import 'package:flutter/material.dart';

final blackH1Text = TextStyle(color: Colors.black, fontSize: 18.0);
final greyH6Text = TextStyle(color: Colors.grey, fontSize: 10.0);
final greyH5Text = TextStyle(color: Colors.grey, fontSize: 12.0);
final greyH5TextBold =
    TextStyle(color: Colors.grey, fontSize: 12.0, fontWeight: FontWeight.w500);
final greyH4Text = TextStyle(color: Colors.grey, fontSize: 14.0);
final blackH5Text = TextStyle(color: Colors.black, fontSize: 12.0);
final blackH5TextBold =
    TextStyle(color: Colors.black, fontSize: 12.0, fontWeight: FontWeight.w500);
final blackH4Text = TextStyle(color: Colors.black, fontSize: 14.0);
final blackH3Text = TextStyle(color: Colors.black, fontSize: 16.0);

final blackh6Text =
    TextStyle(backgroundColor: Colors.grey.shade300, fontSize: 10.0);

final sampleTextStyle = TextStyle(
  color: Colors.white,
  backgroundColor: Colors.black,
);
final whitesmallTextStyle = TextStyle(
  color: Colors.white,
);
final greyText = TextStyle(
  color: Colors.grey,
);

final greySmallText = TextStyle(
  color: Colors.grey,
  fontSize: 12,
);
const primaryColorBlue =
    const Color(0xFF0038c3); // Second `const` is optional in assignments.

final greenTextSmall =
    TextStyle(color: Colors.green, fontWeight: FontWeight.w600);
final greenDiscountSmall = TextStyle(
  fontSize: 12,
  color: Colors.green,
  fontWeight: FontWeight.w900,
);
final redText = TextStyle(
  fontSize: 18,
  color: Colors.redAccent,
  fontWeight: FontWeight.w900,
);
final themeSearchBar = TextStyle(
  fontSize: 12,
  color: Colors.white,
  backgroundColor: Colors.black,
  fontWeight: FontWeight.w900,
);
final blackText = TextStyle(color: Colors.black);

final smallTextStyle = TextStyle(
  fontSize: 10.0,
  color: Colors.black,
  fontWeight: FontWeight.w600,
);
final highligttedTextStyle = TextStyle(
  fontSize: 16.0,
  color: Colors.black,
  fontWeight: FontWeight.w600,
);
final redButtonStyle = TextStyle(
  fontSize: 12.0,
  color: Colors.white,
  fontWeight: FontWeight.w600,
);
final largeTextStyle = TextStyle(
  fontSize: 28.0,
  color: Colors.black,
  fontWeight: FontWeight.w500,
);
final seconhighligttedTextStyle = TextStyle(
  fontSize: 13.0,
  color: Colors.black,
  fontWeight: FontWeight.w900,
);
final seconTextStyle = TextStyle(
  fontSize: 13.0,
  color: Colors.black,
  fontWeight: FontWeight.w400,
);
final strikethoughDiscount = TextStyle(
    fontSize: 12.0,
    color: Colors.grey,
    fontWeight: FontWeight.w500,
    decoration: TextDecoration.lineThrough);
final detailPagetextStyle =
    TextStyle(color: Colors.black, fontWeight: FontWeight.w500, fontSize: 18.0);

final textFieldBorder = OutlineInputBorder(
  borderRadius: BorderRadius.circular(8),
  borderSide: BorderSide(
    width: 1.0,
    style: BorderStyle.solid,
  ),
);
