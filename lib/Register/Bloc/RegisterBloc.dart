import 'package:dio/dio.dart';
import 'package:safalsathi/Register/Model/LoginResponse.dart';
import 'package:safalsathi/Register/repository/RegisterRepository.dart';
//import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:rxdart/rxdart.dart';
import 'package:shared_preferences/shared_preferences.dart';

class RegisterBloc {
  final registerRepository = RegisterRepository();
  final BehaviorSubject<LoginResponse> _subject =
      BehaviorSubject<LoginResponse>();

  postRegister(String username, String email, String phone, String password,
      String passwordConfirmation) async {
    Response response = await registerRepository.postRegister(
        username, email, phone, password, passwordConfirmation);
    LoginResponse loginResponse = LoginResponse.fromJsonMap(response.data);
    addStringToSF(loginResponse.data.accessToken);
    //OneSignal.shared.setExternalUserId(loginResponse.userId.toString());

    _subject.sink.add(loginResponse);
  }

  dispose() {
    _subject.close();
  }

  BehaviorSubject<LoginResponse> get subject => _subject;

  addStringToSF(String accessToken) async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.setString('token', "Bearer" + accessToken);
  }
}

final registerBloc = RegisterBloc();
