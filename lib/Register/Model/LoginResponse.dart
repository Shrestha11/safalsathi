import 'package:safalsathi/Register/Model/data.dart';

class LoginResponse {
  final Data data;
  final int userId;

  LoginResponse.fromJsonMap(Map<String, dynamic> map)
      : data = Data.fromJsonMap(map["data"]),
        userId = map["user_id"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> datas = new Map<String, dynamic>();
    datas['data'] = data == null ? null : data.toJson();
    datas['user_id'] = userId;
    return datas;
  }
}
