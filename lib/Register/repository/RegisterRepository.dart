import 'package:dio/dio.dart';
import 'package:safalsathi/Register/resource/RegisterDataApiProvider.dart';

class RegisterRepository {
  RegisterDataApiProvider _apiProvider = RegisterDataApiProvider();

  Future<Response> postRegister(String username, String email, String phone,
      String password, String passwordConfirm) {
    return _apiProvider.postLogin(
        username, email, phone, password, passwordConfirm);
  }
}
