import 'package:dio/dio.dart';
import 'package:safalsathi/common/DioBase.dart';

class RegisterDataApiProvider {
 // final String _endpoint = "http://welcome.nextnepal.com.np/api/register";
  final String _endpoint = "/register";
  final Dio _dio = Dio();

  Future<Response> postLogin(String username, String email, String phone,
      String password, String passwordConfirm) async {
    try {
     /* Response response = await _dio.post(_endpoint, data: {
        "user_name": username,
        "email": email,
        "phone": phone,
        "password": password,
        "password_confirmation": passwordConfirm
      });*/
      Response response = await dioBASE().post(_endpoint, data: {
        "user_name": username,
        "email": email,
        "phone": phone,
        "password": password,
        "password_confirmation": passwordConfirm
      });
      print("rrrrr $response");
      return response;
    } catch (error) {
      print("ffffffff $error");
      return null;
    }
  }
}
