import 'package:safalsathi/common/common_ui.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'Bloc/RegisterBloc.dart';
import 'Model/LoginResponse.dart';

class RegisterPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Register();
  }
}

class _Register extends State<RegisterPage> {
  GlobalKey<FormState> _key = new GlobalKey();
  String email, password, userName, phone, passwordConfirmation;
  bool _validate = false;
  final myPasswordController = TextEditingController();

  @override
  void dispose() {
    myPasswordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
     /* appBar: new AppBar(
          backgroundColor: Colors.transparent,
          elevation: 0.0,
          iconTheme: new IconThemeData(color: Colors.deepPurple)),*/
      body: StreamBuilder<LoginResponse>(
          stream: registerBloc.subject.stream,
          builder: (context, AsyncSnapshot<LoginResponse> snapshot) {
            if (snapshot.hasError) {
              return Text("");
                //buildErrorWidget(context, snapshot.error);
            } else if (snapshot.hasData) {
              return Form(
                key: _key,
                autovalidate: _validate,
                child: formUi(),
              );
            } else
              return
                Stack(
                    children: <Widget>[
                Container(
                child: ClipRRect(
                    // make sure we apply clip it properly
                    child: BackdropFilter(
                    filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
            child: Container(
            alignment: Alignment.center,
            color: Colors.grey.withOpacity(0.1),
            ),
            ),
            ),
            decoration: BoxDecoration(
            image: DecorationImage(
            image: ExactAssetImage("images/loginWall.jpeg"),
            fit: BoxFit.cover,
            ),
            ),
            ),
                Form(
                key: _key,
                autovalidate: _validate,
                child: formUi(),
              ),],);
          }),
    );
  }

  Widget formUi() {
    return ListView(
      children: <Widget>[
        Center(
          child: Container(
            child: new Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height:80),
                    Padding(
                      padding: const EdgeInsets.only(top: 0.0, bottom: 0.0),
                      child: new Text(
                        "SafalSathi",
                        style: new TextStyle(fontSize: 30.0,color:Colors.white),
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new TextFormField(

                    keyboardType: TextInputType.text,
                    autofocus: false,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "UserName",
                      fillColor: Colors.grey,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                        const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      focusColor: Colors.white,
                      border: OutlineInputBorder(
                        borderSide:
                        const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      labelStyle: TextStyle(color:Colors.white),
                    ),
                    onSaved: (String val) {
                      userName = val;
                    },
                  ),
                ),

                new SizedBox(
                  height: 15.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new TextFormField(
                    validator: validateEmail,
                    keyboardType: TextInputType.emailAddress,
                    autofocus: false,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "Email",
                      fillColor: Colors.grey,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                        const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      focusColor: Colors.white,
                      border: OutlineInputBorder(
                        borderSide:
                        const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      labelStyle: TextStyle(color:Colors.white),
                    ),
                    onSaved: (String val) {
                      email = val;
                    },
                  ),
                ),

                new SizedBox(
                  height: 15.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new TextFormField(
                    validator: validatePhone,
                    keyboardType: TextInputType.phone,
                    autofocus: false,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "Phone",
                      fillColor: Colors.grey,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                        const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      focusColor: Colors.white,
                      border: OutlineInputBorder(
                        borderSide:
                        const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      labelStyle: TextStyle(color:Colors.white),
                    ),
                    onSaved: (String val) {
                      phone = val;
                    },
                  ),
                ),

                new SizedBox(
                  height: 15.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new TextFormField(
                    validator: validatePassword,
                    controller: myPasswordController,
                    autofocus: false,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "Password",
                      fillColor: Colors.grey,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                        const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      focusColor: Colors.white,
                      border: OutlineInputBorder(
                        borderSide:
                        const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      labelStyle: TextStyle(color:Colors.white),
                    ),
                    onSaved: (String val) {
                      password = val;
                    },
                  ),
                ),

                new SizedBox(
                  height: 15.0,
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new TextFormField(
                    validator: validateRePassword,

                    autofocus: false,
                    style: TextStyle(color: Colors.white),
                    decoration: InputDecoration(
                      labelText: "Confirm Password",
                      fillColor: Colors.grey,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                        const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      focusColor: Colors.white,
                      border: OutlineInputBorder(
                        borderSide:
                        const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      labelStyle: TextStyle(color:Colors.white),
                    ),
                    onSaved: (String val) {
                      passwordConfirmation = val;
                    },
                  ),
                ),

                new Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(
                            left: 20.0, right: 5.0, top: 10.0),
                        child: GestureDetector(
                          onTap: () {
                            fetchPost();
                          },
                          child: new Container(
                              alignment: Alignment.center,
                              height: 60.0,
                              decoration: new BoxDecoration(
                                  color: Colors.white,
                                  borderRadius: new BorderRadius.circular(9.0)),
                              child: new Text("Register",
                                  style: new TextStyle(
                                      fontSize: 20.0, color: Colors.red))),
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      ],
    );
  }

  String validateEmail(String value) {
    String pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regExp = new RegExp(pattern);
    if (value.length == 0) {
      return "Email is Required";
    } else if (!regExp.hasMatch(value)) {
      return "Invalid Email";
    } else {
      return null;
    }
  }

  String validatePassword(String value) {
    if (value.length < 6) {
      return "Min Length of Password is 8";
    } else {
      return null;
    }
  }

  String validateRePassword(String value) {
    if (value.length < 6) {
      return "Min Length of Password is 8";
    } else if (value != myPasswordController.text) {
      return "Password Doesnot match with Password.";
    } else
      return null;
  }

  void fetchPost() {
    if (_key.currentState.validate()) {
      // No any error in validation
      _key.currentState.save();
print("ssssssssssssss");
      registerBloc.postRegister(
          userName, email, phone, password, passwordConfirmation);
    } else {
      // validation error
      setState(() {
        _validate = true;
      });
    }
  }

  String validatePhone(String value) {
    if (value.length < 10 || value.length > 10) {
      return "Invalid Phone Number!";
    } else
      return null;
  }
}
