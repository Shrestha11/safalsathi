import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:safalsathi/AccountPage/AccountSetting.dart';
import 'package:safalsathi/MyCart/MyCart.dart';
//import 'package:smart_bazaar/AccountPage/AccountSetting.dart';
//import 'package:smart_bazaar/MyCart/MyCart.dart';
import 'package:safalsathi/common/color.dart';
import 'package:safalsathi/home_main/home_main.dart';
//import 'package:smart_bazaar/servicesHome/services.dart';

import 'package:safalsathi/app_styles.dart';

class Home extends StatefulWidget {
  Home({Key key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> with TickerProviderStateMixin {
  GlobalKey _key = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return AnnotatedRegion<SystemUiOverlayStyle>(
      value: SystemUiOverlayStyle(
          statusBarColor: Colors.transparent,
          statusBarIconBrightness: Brightness.dark),
      child: new MaterialApp(
          theme: ThemeData(
//            buttonColor: Colors.blue,
              accentColor: AppColors.PRIMARY_RED,
              buttonTheme: ButtonThemeData(
                buttonColor: AppColors.WHITE,
                shape: RoundedRectangleBorder(),
                textTheme: ButtonTextTheme.accent,
              ),
              primaryIconTheme: IconThemeData(color: Colors.black),
              textTheme: TextTheme(
                display1: TextStyle(
                  color: Colors.red.shade900,
                  fontWeight: FontWeight.w600,
                  fontSize: 20.0,
                ),
                body1: TextStyle(
                    color: Colors.black,
                    fontWeight: FontWeight.w500,
                    fontSize: 16.0),
                body2: TextStyle(
                  color: Colors.black,
                  fontSize: 10.0,
                ),
                display2: TextStyle(
                  color: AppColors.PRIMARY_BLUE,
                  fontWeight: FontWeight.w600,
                  fontSize: 8.0,
                ),
                display4: TextStyle(
                    fontFamily: 'Baloo',
                    color: Colors.white,
                    fontSize: 20.0,
                    shadows: [
                      Shadow(
                          // bottomLeft
                          offset: Offset(-1.0, -1.0),
                          color: Colors.black),
                      Shadow(
                          // bottomRight
                          offset: Offset(1.0, -1.0),
                          color: Colors.black),
                      Shadow(
                          // topRight
                          offset: Offset(1.0, 1.0),
                          color: Colors.black),
                      Shadow(
                          // topLeft
                          offset: Offset(-1.0, 1.0),
                          color: Colors.black),
                    ]),
              ),
              cardColor: Colors.white,
              primaryColor: Colors.deepOrange,
              iconTheme: IconThemeData(color: Colors.white)),
          home: DefaultTabController(
            length: 3,
            key: _key,
            child: new Scaffold(
              bottomNavigationBar: Container(
                decoration: new BoxDecoration(
                  gradient: new LinearGradient(
                    begin: FractionalOffset.topLeft,
                    end: FractionalOffset.bottomRight,

                 // colors:[Colors.deepOrange,Colors.orange]
                    colors:[AppColors.DEEPORANGE,AppColors.ORANGE]
                    //colors: [AppColors.GRADIENT_START, AppColors.GRADIENT_END],
                  ),
                ),
                child: new TabBar(
                  tabs: [
                    Tab(icon: Icon(Icons.home)),
                    //Tab(icon: Icon(Icons.offline_bolt)),
                    Tab(icon: Icon(Icons.shopping_cart)),
                    Tab(icon: Icon(Icons.account_circle))
                  ],
                  isScrollable: false,
//                  indicatorSize: TabBarIndicatorSize.label,
                  indicatorPadding: EdgeInsets.all(5.0),
                  indicatorColor: AppColors.PRIMARY_RED,

//                  onTap: (index) => setState(() {}),
                ),
              ),
              body: TabBarView(
                physics: NeverScrollableScrollPhysics(),
                children: [
                  HomeMain(),
                // Container(),
                 MyCart(),
                 Account(),
                 // ServicesHome(),
                  //MyCart(),
                  //Account(),
                ],
              ),
            ),
          )),
    );
  }
}
