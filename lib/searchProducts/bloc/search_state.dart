import 'package:meta/meta.dart';
import 'package:safalsathi/searchProducts/Model/SearchResponse.dart';
import 'package:safalsathi/searchProducts/Model/brands.dart';

@immutable
abstract class SearchState {}

class InitialSearchState extends SearchState {}

class LoadingSearch extends SearchState {
  @override
  String toString() => 'Loading Search';
}

class LoadedSearch extends SearchState {
  final SearchResponse searchResponse;

  LoadedSearch(this.searchResponse);

  @override
  String toString() => 'Loaded Search';
}

class LoadingError extends SearchState {
  final String error;

  LoadingError(this.error);
}

class SortDialogDisplayState extends SearchState {
  @override
  String toString() => 'Sort Dialog Display State';
}

class FilterDialogDisplayState extends SearchState {
  final List<Brands> brandList;
  final List<String> colorList;
  final List<String> sizeList;
  final List<bool> brandSelected;
  final List<bool> sizeSelected;
  final List<bool> colorSelected;
  final int minRangePrice, maxRangePrice;

  FilterDialogDisplayState(
      this.brandList,
      this.colorList,
      this.sizeList,
      this.brandSelected,
      this.colorSelected,
      this.sizeSelected,
      this.minRangePrice,
      this.maxRangePrice);

  @override
  String toString() => 'Filtered Dialog Display State';
}

class LoadingNextPage extends SearchState {
  @override
  String toString() => 'Loading Next Page';
}

class SortUpdated extends SearchState {
  @override
  String toString() => 'Sort Updated';
}

class FilterUpdated extends SearchState {
  @override
  String toString() => 'Filter Updated';
}

class PriceUpdated extends SearchState {
  final int minRangePrice, maxRangePrice;

  PriceUpdated(this.minRangePrice, this.maxRangePrice);

  @override
  String toString() => 'Price Updated';
}

class BrandsUpdated extends SearchState {
  final List<bool> brandSelected;

  BrandsUpdated(this.brandSelected);

  @override
  String toString() => 'Brand Updated';
}

class ColorUpdated extends SearchState {
  final List<bool> colorSelected;

  ColorUpdated(this.colorSelected);

  @override
  String toString() => 'ColorUpdated';
}

class SizeUpdated extends SearchState {
  final List<bool> sizeSelected;

  SizeUpdated(this.sizeSelected);

  @override
  String toString() => 'Size Updated';
}
