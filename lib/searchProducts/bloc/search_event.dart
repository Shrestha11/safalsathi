import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

@immutable
abstract class SearchEvent {}

class LoadSearchEvent extends SearchEvent {
  final String searchValue;

  LoadSearchEvent(this.searchValue);

  @override
  String toString() => 'Load Search Event';
}

class LoadNextPage extends SearchEvent {
  final int nextPage;

  LoadNextPage(this.nextPage);

  @override
  String toString() => 'Load Next Page';
}

class SortDialogClick extends SearchEvent {
  @override
  String toString() => 'Sort Click';
}

class SortSelected extends SearchEvent {
  final int id;

  SortSelected(this.id);

  @override
  String toString() => 'Sort Selected';
}

class FilterDialogClick extends SearchEvent {
  @override
  String toString() => 'FilterDialogClick';
}

class FilterDialogSelected extends SearchEvent {
  @override
  String toString() => 'Filter Dialog Selected';
}

class BrandSelected extends SearchEvent {
  final int position;

  BrandSelected(this.position);

  @override
  String toString() => 'Brand Selected';
}

class BrandDeselected extends SearchEvent {
  final int position;

  BrandDeselected(this.position);

  @override
  String toString() => 'Brand Deselected';
}

class ColorSelected extends SearchEvent {
  final int position;

  ColorSelected(this.position);

  @override
  String toString() => 'Color Selected';
}

class ColorDeselected extends SearchEvent {
  final int position;

  ColorDeselected(this.position);

  @override
  String toString() => 'Color Deselected';
}

class PriceChanged extends SearchEvent {
  final RangeValues rangeValues;

  PriceChanged(this.rangeValues);

  @override
  String toString() => 'Price Changed';
}

class SizeSelected extends SearchEvent {
  final int position;

  SizeSelected(this.position);

  @override
  String toString() => 'Size Selected';
}

class SizeDeselected extends SearchEvent {
  final int position;

  SizeDeselected(this.position);

  @override
  String toString() => 'Size Deselceted';
}

class FilterApply extends SearchEvent {
  @override
  String toString() => 'Filter Apply';
}
