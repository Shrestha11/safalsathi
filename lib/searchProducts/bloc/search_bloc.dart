import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/generic_response.dart';
import 'package:safalsathi/searchProducts/Model/Filter/FilterResponse.dart';
import 'package:safalsathi/searchProducts/Model/SearchResponse.dart';
import 'package:safalsathi/searchProducts/Model/brands.dart';
import 'package:safalsathi/searchProducts/SearchProductRepository.dart';

import './bloc.dart';

class SearchBloc extends Bloc<SearchEvent, SearchState> {
  String searchValue;
  SearchResponse _searchResponse;
  List<Brands> brandList;
  List<String> colorList;
  List<String> sizeList;
  List<bool> brandSelected = [];
  List<bool> sizeSelected = [];
  List<bool> colorSelected = [];
  int minRangePrice = 10, maxRangePrice = 100;
  int sortId = 0;

  @override
  SearchState get initialState => InitialSearchState();

  @override
  Stream<SearchState> mapEventToState(
    SearchEvent event,
  ) async* {
    SearchProductRepository _searchProductRepository =
        SearchProductRepository();
    if (event is LoadSearchEvent) {
      yield LoadingSearch();
      searchValue = event.searchValue;
      try {
        ApiResponse _apiResponse =
            await _searchProductRepository.getSearch(searchValue);
        switch (_apiResponse.apiCall) {
          case ApiResultStatus.SUCCESS:
            _searchResponse =
                SearchResponse.fromJsonMap(_apiResponse.responseData);
            if (_searchResponse.products.products.length == 0) {
              yield LoadingError("NO PRODUCT FOUND");
            }
            yield LoadedSearch(_searchResponse);
            break;
          case ApiResultStatus.ERROR:
            yield LoadingError("Loading Error");
            break;
          case ApiResultStatus.NO_NETWORK:
            yield LoadingError("Error Netowork");
            break;
          case ApiResultStatus.STAT_ERR:
            yield LoadingError("Loading Error");
            break;
        }
      } catch (e) {
        yield LoadingError(e.toString());
      }
      brandList = _searchResponse.brands;
      colorList = _searchResponse.colors;
      sizeList = _searchResponse.sizes;
      for (int i = 0; i < brandList.length; i++) brandSelected.add(true);
      for (int i = 0; i < colorList.length; i++) colorSelected.add(true);
      for (int i = 0; i < sizeList.length; i++) sizeSelected.add(true);
      yield LoadedSearch(_searchResponse);
    }
    if (event is SortDialogClick) {
      yield SortDialogDisplayState();
    }
    if (event is FilterDialogClick) {
      yield FilterDialogDisplayState(
          brandList,
          colorList,
          sizeList,
          brandSelected,
          colorSelected,
          sizeSelected,
          minRangePrice,
          maxRangePrice);
    }
    if (event is SortSelected) {
      sortId = event.id;
      FilterResponse _filteredResponse = await _fetchFilteredSort(
          searchValue,
          sortId,
          brandList,
          sizeList,
          colorList,
          brandSelected,
          colorSelected,
          sizeSelected,
          minRangePrice,
          maxRangePrice);
      _searchResponse.products.products =
          _filteredResponse.products.productList;
      yield LoadedSearch(_searchResponse);
    }
    if (event is BrandSelected) {
      brandSelected[event.position] = true;
      yield BrandsUpdated(brandSelected);
    }
    if (event is BrandDeselected) {
      brandSelected[event.position] = false;
      yield BrandsUpdated(brandSelected);
    }
    if (event is SizeSelected) {
      sizeSelected[event.position] = true;
      yield SizeUpdated(sizeSelected);
    }
    if (event is SizeDeselected) {
      sizeSelected[event.position] = false;
      yield SizeUpdated(sizeSelected);
    }
    if (event is ColorSelected) {
      colorSelected[event.position] = true;
      yield ColorUpdated(colorSelected);
    }
    if (event is ColorDeselected) {
      colorSelected[event.position] = false;
      yield ColorUpdated(colorSelected);
    }
    if (event is PriceChanged) {
      minRangePrice = event.rangeValues.start.toInt();
      maxRangePrice = event.rangeValues.end.toInt();
      yield PriceUpdated(minRangePrice, maxRangePrice);
    }
    if (event is FilterApply) {
      FilterResponse _filteredResponse = await _fetchFilteredSort(
          searchValue,
          sortId,
          brandList,
          sizeList,
          colorList,
          brandSelected,
          colorSelected,
          sizeSelected,
          minRangePrice,
          maxRangePrice);
      _searchResponse.products.products =
          _filteredResponse.products.productList;
      yield LoadedSearch(_searchResponse);
    }
  }
}

Future<FilterResponse> _fetchFilteredSort(
    String searchQuery,
    int sortId,
    List<Brands> brandList,
    List<String> sizeList,
    List<String> colorList,
    List<bool> brandSelected,
    List<bool> colorSelected,
    List<bool> sizeSelected,
    int minRangePrice,
    int maxRanagePrice) async {
  final String _endpoint = "/search?query=$searchQuery";

  String sortString;
  switch (sortId) {
    case 0:
      sortString = "popular";
      break;
    case 1:
      sortString = "high-low";
      break;
    case 2:
      sortString = "low-high";
      break;
    case 3:
      sortString = "new";
      break;
    case 4:
      sortString = "old";
      break;
    case 5:
      sortString = "a-z";
      break;
    case 6:
      sortString = "z-a";
      break;
  }
  String brandFilterString = "";
  for (int i = 0; i < brandList.length; i++) {
    if (brandSelected[i] = true) {
      brandFilterString = "$brandFilterString&brand[$i]=${brandList[i].slug}";
    }
  }
  String sizeFilterString = "";
  for (int i = 0; i < sizeList.length; i++) {
    if (sizeSelected[i] = true) {
      sizeFilterString = "$sizeFilterString&size[$i]=${sizeList[i]}";
    }
  }
  String colorFilterString = "";
  for (int i = 0; i < colorList.length; i++) {
    if (colorSelected[i] = true) {
      colorFilterString = "$colorFilterString&colour[$i]=${colorList[i]}";
    }
  }
  String minPrice = (minRangePrice * 1000).toString();
  String maxPrice = (maxRanagePrice * 1000).toString();
  String priceFilterString = "&minprice=$maxPrice&minprice=$minPrice";
  try {
    Response response = await dioBASE().get(
        "$_endpoint?sort=$sortString$sizeFilterString$brandFilterString$colorFilterString$priceFilterString");
    return FilterResponse.fromJsonMap(response.data);
  } catch (error) {
    return null;
  }
}
