import 'package:safalsathi/searchProducts/Model/searchFiltered/products.dart';

class SearchFilterResponse {

  final ProductsPaged products;

	SearchFilterResponse.fromJsonMap(Map<String, dynamic> map): 
		products = ProductsPaged.fromJsonMap(map["products"]);

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['products'] = products == null ? null : products.toJson();
		return data;
	}
}
