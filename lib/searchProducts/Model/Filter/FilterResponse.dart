import 'filter_page_response.dart';

class FilterResponse {
  final FilterPageResponse products;

  FilterResponse.fromJsonMap(Map<String, dynamic> map)
      : products = FilterPageResponse.fromJsonMap(map["products"]);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['products'] = products == null ? null : products.toJson();
    return data;
  }
}
