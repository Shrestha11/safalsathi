import 'package:safalsathi/common/model/products.dart';

class FilterPageResponse {
  final int currentPage;
  final List<Products> productList;
  final String firstPageUrl;
  final int from;
  final int lastPage;
  final String lastPageUrl;
  final Object nextPageUrl;
  final String path;
  final int perPage;
  final Object perPageUrl;
  final int to;
  final int total;

  FilterPageResponse.fromJsonMap(Map<String, dynamic> map)
      : currentPage = map["current_page"],
        productList = List<Products>.from(
            map["data"].map((it) => Products.fromJsonMap(it))),
        firstPageUrl = map["first_page_url"],
        from = map["from"],
        lastPage = map["last_page"],
        lastPageUrl = map["last_page_url"],
        nextPageUrl = map["next_page_url"],
        path = map["path"],
        perPage = map["per_page"],
        perPageUrl = map["prev_page_url"],
        to = map["to"],
        total = map["total"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = currentPage;
    data['data'] =
        data != null ? this.productList.map((v) => v.toJson()).toList() : null;
    data['first_page_url'] = firstPageUrl;
    data['from'] = from;
    data['last_page'] = lastPage;
    data['last_page_url'] = lastPageUrl;
    data['next_page_url'] = nextPageUrl;
    data['path'] = path;
    data['per_page'] = perPage;
    data['prev_page_url'] = perPageUrl;
    data['to'] = to;
    data['total'] = total;
    return data;
  }
}
