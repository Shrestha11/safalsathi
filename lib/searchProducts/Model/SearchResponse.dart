import 'package:safalsathi/searchProducts/Model/search_products.dart';

import 'brands.dart';

class SearchResponse {
  SearchProducts products;
  final List<Brands> brands;
  final List<String> colors;
  final List<String> sizes;
  final String title;

  SearchResponse.fromJsonMap(Map<String, dynamic> map)
      : products = SearchProducts.fromJsonMap(map["products"]),
        brands = List<Brands>.from(
            map["brands"].map((it) => Brands.fromJsonMap(it))),
        colors = List<String>.from(map["colors"]),
        sizes = List<String>.from(map["sizes"]),
        title = map["title"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['products'] = products == null ? null : products.toJson();
    data['brands'] =
        brands != null ? this.brands.map((v) => v.toJson()).toList() : null;
    data['colors'] = colors;
    data['sizes'] = sizes;
    data['title'] = title;
    return data;
  }
}
