
class Brands {

  final int id;
  final int userId;
  final String name;
  final String companyName;
  final String slug;
  final String document;
  final Object description;
  final int status;
  final String createdAt;
  final String updatedAt;

	Brands.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		userId = map["user_id"],
		name = map["name"],
		companyName = map["company_name"],
		slug = map["slug"],
		document = map["document"],
		description = map["description"],
		status = map["status"],
		createdAt = map["created_at"],
		updatedAt = map["updated_at"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['user_id'] = userId;
		data['name'] = name;
		data['company_name'] = companyName;
		data['slug'] = slug;
		data['document'] = document;
		data['description'] = description;
		data['status'] = status;
		data['created_at'] = createdAt;
		data['updated_at'] = updatedAt;
		return data;
	}
}
