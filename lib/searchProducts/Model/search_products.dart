import 'package:safalsathi/common/model/products.dart';

class SearchProducts {
  final int currentPage;
  List<Products> products;
  final String firstPageUrl;
  final int from;
  final int lastPage;
  final String lastPageUrl;
  final String nextPageUrl;
  final String path;
  final int perPage;
  final String prevPageUrl;
  final int to;
  final int total;

  SearchProducts.fromJsonMap(Map<String, dynamic> map)
      : currentPage = map["current_page"],
        products = List<Products>.from(
            map["data"].map((it) => Products.fromJsonMap(it))),
        firstPageUrl = map["first_page_url"],
        from = map["from"],
        lastPage = map["last_page"],
        lastPageUrl = map["last_page_url"],
        nextPageUrl = map["next_page_url"],
        path = map["path"],
        perPage = map["per_page"],
        prevPageUrl = map["prev_page_url"],
        to = map["to"],
        total = map["total"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['current_page'] = currentPage;
    data['data'] =
        data != null ? this.products.map((v) => v.toJson()).toList() : null;
    data['first_page_url'] = firstPageUrl;
    data['from'] = from;
    data['last_page'] = lastPage;
    data['last_page_url'] = lastPageUrl;
    data['next_page_url'] = nextPageUrl;
    data['path'] = path;
    data['per_page'] = perPage;
    data['prev_page_url'] = prevPageUrl;
    data['to'] = to;
    data['total'] = total;
    return data;
  }
}
