import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/common/color.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/model/products.dart';
import 'package:safalsathi/product_detail/productDetail.dart';

import 'package:safalsathi/searchProducts/Model/SearchResponse.dart';
import 'Model/brands.dart';
import 'bloc/search_bloc.dart';
import 'bloc/search_event.dart';
import 'bloc/search_state.dart';

class SearchItem extends StatefulWidget {
  final String currentText;

  SearchItem(this.currentText);

  @override
  State<StatefulWidget> createState() {
    return _SearchItemState();
  }
}

class _SearchItemState extends State<SearchItem> {
  SearchBloc _searchBloc = SearchBloc();
  SearchResponse _searchResponse;

  @override
  void initState() {
    super.initState();
    _searchBloc.add(LoadSearchEvent(widget.currentText));
  }

  @override
  void dispose() {
    super.dispose();
    _searchBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: AppColors.LIGHT_GREY2,
        body: Column(
          children: <Widget>[
            Container(
              color: AppColors.WHITE,
              child: SafeArea(
                child: Column(children: <Widget>[
                  InkWell(
                    onTap: () => Navigator.pop(context),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Icon(
                            Icons.arrow_back,
                            color: AppColors.BLACk,
                          ),
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Container(
                              color: AppColors.WHITE,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  widget.currentText,
                                  style: TextStyle(fontSize: 16.0),
                                ),
                              ),
                            ),
                          ),
                        ),
                        InkWell(
                          /*onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => SearchContentProductListing(searchTerm),
              ),
          ),*/
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Text(
                              "Search",
                              style: TextStyle(
                                color: AppColors.BANNER_COLOR,
                              ),
                            ),
                          ),
                        )
                      ],
                    ),
                  ),
                ]),
              ),
            ),
            Expanded(
              child: BlocListener(
                bloc: _searchBloc,
                listener: (context, _categoriesState) {
                  if (_categoriesState is SortDialogDisplayState) {
                    _showSortDialog(context, _searchBloc);
                  }
                  if (_categoriesState is FilterDialogDisplayState) {
                    showModalBottomSheet(
                      shape: OutlineInputBorder(
                          borderRadius: BorderRadius.all(Radius.circular(4.0))),
                      isScrollControlled: false,
                      context: context,
                      builder: (BuildContext bc) {
                        return DynamicDialog(_searchBloc);
                      },
                    );
                  }
                },
                child: BlocBuilder(
                    bloc: _searchBloc,
                    builder: (BuildContext context, SearchState _searchState) {
                      if (_searchState is InitialSearchState) {
                        return Center(child: Text("Loading"));
                      }
                      if (_searchState is LoadingSearch) {
                        return Center(child: Text("Loading"));
                      }
                      if (_searchState is LoadingError) {
                        return Center(child: Text(_searchState.error));
                      }
                      if (_searchState is LoadedSearch) {
                        _searchResponse = _searchState.searchResponse;
                      }
                      if (_searchResponse.products.products.length == 0) {
                        return Center(child: Text("No Product Found."));
                      }
                      return _filterSort(_searchBloc, context, _searchResponse);
                    }),
              ),
            ),
          ],
        ));
  }

  void _showSortDialog(BuildContext context, SearchBloc _categoriesBloc) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('Popularity'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(0));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('Price: High to Low'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(1));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    dense: true,
                    title: new Text('Price: Low to High'),
                    onTap: () {
                      _categoriesBloc.add(SortSelected(2));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('Newest'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(3));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('Oldest'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(4));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('A-Z'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(5));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('Z-a'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(6));
                      Navigator.of(context).pop(true);
                      Navigator.of(context).pop(true);
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  _filterSort(SearchBloc _searchBlocBloc, BuildContext context,
      SearchResponse searchResponse) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: InkWell(
                onTap: () => _searchBlocBloc.add(SortDialogClick()),
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                    child: Container(
//                    width: (width / 2) - 10.0,
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.sort,
                            color: AppColors.BLACk,
                            size: 25.0,
                          ),
                          Expanded(
                            child: Center(
                              child: Text(
                                "Sort",
//                              style: seconTextStyle,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
            Expanded(
              child: InkWell(
                onTap: () => _searchBlocBloc.add(FilterDialogClick()),
                child: Card(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                    child: Container(
//                    width: (width / 2) - 10.0,
                      child: Row(
                        children: <Widget>[
                          Icon(
                            Icons.filter_list,
                            size: 25.0,
                            color: AppColors.BLACk,
                          ),
                          Expanded(
                            child: Center(
                              child: Text(
                                "Filter",
//                              style: seconTextStyle,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        ListView.builder(
          shrinkWrap: true,
          physics: AlwaysScrollableScrollPhysics(),
          itemCount: searchResponse.products.products.length,
          itemBuilder: (context, position) {
            return _productListItem(
                searchResponse.products.products[position], context, position);
          },
        ),
      ],
    );
  }

  _productListItem(Products product, BuildContext context, int position) {
    return InkWell(
      onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ProductDetail(products: product))),
      child: Card(
        child: Row(
          children: <Widget>[
            Card(
              child: Image.network(
                product.imgs[0].largeUrl,
                height: MediaQuery.of(context).size.width / 2.5,
              ),
            ),
            Expanded(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      product.name,
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: Theme.of(context)
                          .textTheme
                          .body1
                          .copyWith(fontSize: 20.0),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Text(prices(product.productPrice),
                        style: Theme.of(context).textTheme.body2.copyWith(
                            decoration: TextDecoration.lineThrough,
                            color: Colors.grey)),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(left: 8.0, right: 8.0),
                    child: Text(prices(product.salesPrice),
                        style: Theme.of(context).textTheme.display1),
                  ),
                  Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Container(
                            decoration: new BoxDecoration(
                                borderRadius: BorderRadius.circular(5.0),
                                border: new Border.all(
                                    color: Colors.grey, width: 1.0)),
                            child: Padding(
                              padding: const EdgeInsets.all(2.0),
                              child: Row(
                                children: <Widget>[
                                  Text(product.ratings.getAverage()),
                                  Icon(
                                    Icons.star,
                                    color: Colors.orangeAccent,
                                    size: 20.0,
                                  )
                                ],
                              ),
                            )),
                      ),
                      Text("Out of "
                          "${product.ratings.total.toString()}"
                          " Ratings"),
                    ],
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      recentlyViewed(product.view),
                      style: Theme.of(context).textTheme.display2,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DynamicDialog extends StatefulWidget {
  final SearchBloc categoriesBloc;

  DynamicDialog(this.categoriesBloc);

  @override
  State<StatefulWidget> createState() {
    return _DialogState(categoriesBloc);
  }
}

class _DialogState extends State<DynamicDialog> {
  final SearchBloc searchBloc;

  _DialogState(this.searchBloc);

  double minValue = 1.0, maxValue = 99.0;
  List<Brands> brandList = [];
  List<String> sizeList = [];
  List<String> colorList = [];
  List<bool> brandSelected = [];
  List<bool> sizeSelected = [];
  List<bool> colorSelected = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: searchBloc,
        builder: (context, _searchBlocState) {
          if (_searchBlocState is FilterDialogDisplayState) {
            minValue = searchBloc.minRangePrice.toDouble();
            maxValue = searchBloc.maxRangePrice.toDouble();
            brandList = searchBloc.brandList;
            sizeList = searchBloc.sizeList;
            colorList = searchBloc.colorList;
            brandSelected = searchBloc.brandSelected;
            sizeSelected = searchBloc.sizeSelected;
            colorSelected = searchBloc.colorSelected;
          }
          if (_searchBlocState is PriceUpdated) {
            print("Price Updated");
            minValue = searchBloc.minRangePrice.toDouble();
            maxValue = searchBloc.maxRangePrice.toDouble();
          }
          if (_searchBlocState is SizeUpdated) {
            sizeSelected = _searchBlocState.sizeSelected;
          }
          if (_searchBlocState is BrandsUpdated) {
            brandSelected = _searchBlocState.brandSelected;
          }
          if (_searchBlocState is ColorUpdated) {
            colorSelected = _searchBlocState.colorSelected;
          }
          return SingleChildScrollView(
              child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Center(
                    child: Text(
                  "Apply Filter",
                  style: Theme.of(context)
                      .textTheme
                      .display1
                      .copyWith(fontSize: 24.0),
//                style: redText,
                )),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(children: <Widget>[
                        Center(
                            child: Text(
                          "Price",
                          style: Theme.of(context)
                              .textTheme
                              .display2
                              .copyWith(fontSize: 20.0),

//                        style: greyText,
                        )),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                          color: AppColors.PRIMARY_BLUE,
                        ),
                      ]),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "MinPrice:",
                            style: Theme.of(context)
                                .textTheme
                                .subhead
                                .copyWith(fontSize: 18.0),
                          ),
                          Text(
                            "MaxPrice: ",
                            style: Theme.of(context)
                                .textTheme
                                .subhead
                                .copyWith(fontSize: 18.0),
                          ),
                        ],
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            "Rs.${minValue * 1000}",
                            style: Theme.of(context)
                                .textTheme
                                .display2
                                .copyWith(
                                    fontSize: 15.0, color: AppColors.BLACk),
                          ),
                          Text(
                            "Rs.${maxValue * 1000}",
                            style: Theme.of(context)
                                .textTheme
                                .display2
                                .copyWith(
                                    fontSize: 15.0, color: AppColors.BLACk),
                          ),
                        ],
                      ),
                    ),
                    RangeSlider(
                        values: RangeValues(minValue, maxValue),
                        min: 10,
                        max: 100,
                        onChanged: (RangeValues rangeValues) {
                          searchBloc.add(PriceChanged(rangeValues));
                        }),
                  ],
                ),
              ), //Price
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(children: <Widget>[
                        Center(
                            child: Text(
                          "Brands",
                          style: Theme.of(context)
                              .textTheme
                              .display2
                              .copyWith(fontSize: 20.0),

//                        style: greyText,
                        )),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                          color: AppColors.PRIMARY_BLUE,
                        ),
                      ]),
                    ),
                  ],
                ),
              ), //Price

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: brandList == null
                    ? Container()
                    : ListView.builder(
                        shrinkWrap: true,
                        physics: NeverScrollableScrollPhysics(),
                        itemCount: brandList.length,
                        itemBuilder: (context, position) {
                          return Row(
                            children: <Widget>[
                              Checkbox(
                                activeColor: AppColors.LIGHT_GREY,
                                checkColor: AppColors.PRIMARY_BLUE,
                                value: brandSelected[position],
                                onChanged: (bool value) {
                                  if (value) {
                                    searchBloc.add(BrandSelected(position));
                                  } else {
                                    searchBloc.add(BrandDeselected(position));
                                  }
                                },
                              ),
                              Text(brandList[position].name),
                            ],
                          );
                        },
                      ),
              ),
              //Brands
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(children: <Widget>[
                        Center(
                            child: Text(
                          "Sizes",
                          style: Theme.of(context)
                              .textTheme
                              .display2
                              .copyWith(fontSize: 20.0),

//                        style: greyText,
                        )),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                          color: AppColors.PRIMARY_BLUE,
                        ),
                      ]),
                    ),
                    sizeList == null
                        ? Container()
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: sizeList.length,
                            itemBuilder: (context, position) {
                              return Row(
                                children: <Widget>[
                                  Checkbox(
                                    activeColor: AppColors.LIGHT_GREY,
                                    checkColor: AppColors.PRIMARY_BLUE,
                                    value: sizeSelected[position],
                                    onChanged: (bool value) {
                                      if (value) {
                                        searchBloc.add(SizeSelected(position));
//                                    Navigator.pop(context);
                                      } else {
                                        searchBloc
                                            .add(SizeDeselected(position));
//                                    Navigator.pop(context);
                                      }
                                    },
                                  ),
                                  Text(sizeList[position]),
                                ],
                              );
                            },
                          ),
                  ],
                ),
              ), //Sizes
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(children: <Widget>[
                        Center(
                            child: Text(
                          "Colors",
                          style: Theme.of(context)
                              .textTheme
                              .display2
                              .copyWith(fontSize: 20.0),

//                        style: greyText,
                        )),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                          color: AppColors.PRIMARY_BLUE,
                        ),
                      ]),
                    ),
                    colorList == null
                        ? Container()
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: colorList.length,
                            itemBuilder: (context, position) {
                              return Row(
                                children: <Widget>[
                                  Checkbox(
                                      activeColor: AppColors.LIGHT_GREY,
                                      checkColor: AppColors.PRIMARY_BLUE,
                                      value: colorSelected[position],
                                      onChanged: (bool value) {
                                        if (value) {
                                          searchBloc
                                              .add(ColorSelected(position));
//                                        Navigator.pop(context);
                                        } else {
                                          searchBloc
                                              .add(ColorDeselected(position));
//                                        Navigator.pop(context);
                                        }
                                      }),
                                  Text(colorList[position]),
                                ],
                              );
                            },
                          ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: RaisedButton(
                  color: AppColors.PRIMARY_BLUE,
                  child: Text(
                    "Filter Products",
                    style: Theme.of(context)
                        .textTheme
                        .subhead
                        .copyWith(color: AppColors.WHITE),
                  ),
                  onPressed: () {
                    searchBloc.add(FilterApply());
                    Navigator.pop(context);
                  },
                ),
              )
            ],
          ));
        });
  }
}

class SearchBar extends StatelessWidget {
  final SearchBloc _searchBlocBloc;

  SearchBar(this._searchBlocBloc);

  @override
  Widget build(BuildContext context) {
    return TextField(
      onSubmitted: (searchTerm) {
        _searchBlocBloc.add(LoadSearchEvent(searchTerm));
      },
      decoration: InputDecoration(
        border: InputBorder.none,
        contentPadding: EdgeInsets.symmetric(horizontal: 8.0, vertical: 12.0),
//        hintStyle: greyText,
        hintText: 'Search here...',
      ),
    );
  }
}
