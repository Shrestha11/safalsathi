import 'package:dio/dio.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/error_handler.dart';
import 'package:safalsathi/common/generic_response.dart';
import 'package:safalsathi/common/string.dart';

class SearchProductRepository {
  SearchProductApiProvider _searchProductApiProvider =
      SearchProductApiProvider();

  Future<ApiResponse> getSearch(String query) {
    return _searchProductApiProvider.getDetail(query);
  }
}

class SearchProductApiProvider {
  Future<ApiResponse> getDetail(String query) async {
    try {
      Response response = await dioBASE().get(searchProductUrl + query);
      return ApiResponse(
          responseData: response.data,
          apiCall: ApiResultStatus.SUCCESS,
          message: "No Error");
    } on DioError catch (e) {
      print("DIO ERROR $e");
      if (e.type == DioErrorType.DEFAULT)
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.NO_NETWORK,
            message:
                "Make sure wifi or cellular data is turned on, Then try again");
//      else
      return ApiResponse(
          responseData: e,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(e));
    } catch (error) {
      print("DIO Api Error");

      return ApiResponse(
          responseData: error,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(error));
    }
  }
}
