class Policies {
  String privacyPolicy;
  String returnPolicy;
  String termsConditions;
  String cancellation;
  String payments;
  String shipping;
  String error;

  Policies(
      {this.privacyPolicy,
      this.returnPolicy,
      this.termsConditions,
      this.cancellation,
      this.payments,
      this.shipping});

  Policies.fromJson(Map<String, dynamic> json) {
    privacyPolicy = json['privacy_policy'];
    returnPolicy = json['return_policy'];
    termsConditions = json['terms_conditions'];
    cancellation = json['cancellation'];
    payments = json['payments'];
    shipping = json['shipping'];
  }

  Policies.withError(this.error);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['privacy_policy'] = this.privacyPolicy;
    data['return_policy'] = this.returnPolicy;
    data['terms_conditions'] = this.termsConditions;
    data['cancellation'] = this.cancellation;
    data['payments'] = this.payments;
    data['shipping'] = this.shipping;
    return data;
  }
}
