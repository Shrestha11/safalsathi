import 'package:dio/dio.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/error_handler.dart';
import 'package:safalsathi/legals/models/legals.dart';
//import 'package:safalsathi/network/network_endpoint.dart';

class LegalsApiProvider {
  Future<Policies> getPolicies() async {
    try {
      Response response = await dioBASE().get("policies");
      return Policies.fromJson(response.data);
    } catch (error) {
      return Policies.withError(handleError(error));
    }
  }

//  String _handleError(DioError error) {
//    String errorDescription = "";
//    if (error is DioError) {
//      switch (error.error) {
//        case DioErrorType.CANCEL:
//          errorDescription = "Request to API server was cancelled";
//          break;
//        case DioErrorType.CONNECT_TIMEOUT:
//          errorDescription = "Connection timeout with API server";
//          break;
//        case DioErrorType.DEFAULT:
//          errorDescription =
//              "Connection to API server failed due to internet connection";
//          break;
//        case DioErrorType.RECEIVE_TIMEOUT:
//          errorDescription = "Receive timeout in connection with API server";
//          break;
//        case DioErrorType.RESPONSE:
//          errorDescription =
//              "Received invalid status code: ${error.response.statusCode}";
//          break;
//      }
//    } else {
//      errorDescription = "Unexpected error occured";
//    }
//    return errorDescription;
//  }

  void _setupLoggingInterceptor() {
    int maxCharactersPerLine = 200;

    dioBASE().interceptor.request.onSend = (Options options) {
      print("--> ${options.method}"); // ${options.path}");
      print("Content type: ${options.contentType}");
      print("<-- END HTTP");
      return options;
    };

    dioBASE().interceptor.response.onSuccess = (Response response) {
      print(
          "<-- ${response.statusCode} ${response.request.method} ${response.request.path}");
      String responseAsString = response.data.toString();
      if (responseAsString.length > maxCharactersPerLine) {
        int iterations =
            (responseAsString.length / maxCharactersPerLine).floor();
        for (int i = 0; i <= iterations; i++) {
          int endingIndex = i * maxCharactersPerLine + maxCharactersPerLine;
          if (endingIndex > responseAsString.length) {
            endingIndex = responseAsString.length;
          }
          print(responseAsString.substring(
              i * maxCharactersPerLine, endingIndex));
        }
      } else {
        print(response.data);
      }
      print("<-- END HTTP");
    };
  }
}
