
import 'package:safalsathi/legals/models/legals.dart';

import 'legals_api_provider.dart';

class LegalsRepository {
  LegalsApiProvider legalsApiProvider = new LegalsApiProvider();

  Future<Policies> getLegals() {
    return legalsApiProvider.getPolicies();
  }
}
