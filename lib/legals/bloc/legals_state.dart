import 'package:meta/meta.dart';
import 'package:safalsathi/legals/models/legals.dart';

@immutable
abstract class LegalsState {}

class InitialLegalsState extends LegalsState {}

class LoadingLegalsState extends LegalsState {}

class LoadedLegalsState extends LegalsState {
  final Policies legalsModel;

  LoadedLegalsState(this.legalsModel);
}

class LoadingErrorState extends LegalsState {
  final String msg;

  LoadingErrorState(this.msg);
}
