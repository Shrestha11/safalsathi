import 'dart:async';
import 'package:bloc/bloc.dart';
import 'package:safalsathi/legals/models/legals.dart';
import 'package:safalsathi/legals/repository/legals_repository.dart';
import './bloc.dart';

class LegalsBloc extends Bloc<LegalsEvent, LegalsState> {
  @override
  LegalsState get initialState => InitialLegalsState();

  @override
  Stream<LegalsState> mapEventToState(
    LegalsEvent event,
  ) async* {
    if (event is LoadLegalsEvent) {
      yield LoadingLegalsState();
      final LegalsRepository _legalsRepository = new LegalsRepository();
      Policies _legalsModel;
      try {
        _legalsModel = await _legalsRepository.getLegals();
        yield LoadedLegalsState(_legalsModel);
      } catch (e) {
        yield LoadingErrorState(e.toString());
      }
    }
  }
}
