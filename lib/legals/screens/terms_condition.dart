import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:html/parser.dart';
import 'package:safalsathi/legals/bloc/legals_bloc.dart';
import 'package:safalsathi/legals/bloc/legals_event.dart';
import 'package:safalsathi/legals/bloc/legals_state.dart';
import 'package:safalsathi/legals/models/legals.dart';

class LegalsTerm extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LegalsTermState();
  }
}

class LegalsTermState extends State<LegalsTerm> {
  LegalsBloc _legalsBloc = new LegalsBloc();

  @override
  void initState() {
    super.initState();
    _legalsBloc.add(LoadLegalsEvent());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Padding(
        padding: const EdgeInsets.all(15),
        child: BlocBuilder(
          bloc: _legalsBloc,
          builder: (context, state) {
            if (state is LoadingLegalsState) {
              return _buildLoadingWidget();
            }
            if (state is LoadingErrorState) {
              return _buildErrorWidget(state.msg);
            }
            if (state is LoadedLegalsState) {
              return _buildUserWidget(state.legalsModel);
            }
            return Container();
          },
        ),
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Error occured: $error",
              style: Theme.of(context).textTheme.subtitle),
        ],
      ),
    );
  }

  Widget _buildLoadingWidget() {
    return Center(
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text("Loading data from API...",
              style: Theme.of(context).textTheme.subtitle),
          Padding(
            padding: EdgeInsets.only(top: 5),
          ),
          CircularProgressIndicator(),
        ],
      ),
    );
  }

  Widget parseData(String policies) {
    var document = parse(policies);
    return Text(
      document.body.text,
      style: TextStyle(fontFamily: 'Lato-Thin', fontWeight: FontWeight.normal),
    );
  }

  Widget _buildUserWidget(Policies policies) {
    return SingleChildScrollView(
      child: Padding(
        padding: const EdgeInsets.all(3.0),
        child: Column(
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Privacy Policy",
                  style: TextStyle(
                      fontSize: 25,
                      fontFamily: 'Lato-Black',
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            parseData(policies.privacyPolicy),
            Padding(
              padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
              child: Align(
                alignment: Alignment.topLeft,
                child: Text(
                  "Payments",
                  style: TextStyle(
                      fontSize: 25,
                      fontFamily: 'Lato-Black',
                      fontWeight: FontWeight.bold),
                ),
              ),
            ),
            parseData(policies.payments),
          ],
        ),
      ),
    );
  }
}
