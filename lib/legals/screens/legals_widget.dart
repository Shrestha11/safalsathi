import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/legals/bloc/legals_bloc.dart';
import 'package:safalsathi/legals/bloc/legals_event.dart';
import 'package:safalsathi/legals/bloc/legals_state.dart';
import 'package:safalsathi/legals/models/legals.dart';

class LegalsWidget extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LegalsWidgetState();
  }
}

class LegalsWidgetState extends State<LegalsWidget> {
  LegalsBloc _legalsBloc = new LegalsBloc();

  @override
  void initState() {
    super.initState();
    _legalsBloc.add(LoadLegalsEvent());
  }

  @override
  Widget build(BuildContext context) {

    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: BlocBuilder(
        bloc: _legalsBloc,
        builder: (context, state) {
          if (state is LoadingLegalsState) {
            return Center(child: CircularProgressIndicator());
          }
          if (state is LoadingErrorState) {
            print("dd");
            buildSnackError(context, state.msg);
            return Center(child: Text("ERROR"));
          }
          if (state is LoadedLegalsState) {
            print("cc");
            return _buildUserWidget(state.legalsModel);
          }
          return Center(child: Text("Ops! Something went wrong"));
        },
      ),
    );
  }

  Widget _buildErrorWidget(String error) {
    return Center(
        child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text("Error occured: $error",
            style: Theme.of(context).textTheme.subtitle),
      ],
    ));
  }

  Widget _buildUserWidget(Policies policies) {
    print("ssssss ${policies.termsConditions}");
    return SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Align(
            alignment: Alignment.topLeft,
            child: Text(
              "Terms & Conditions",
              style: TextStyle(
                  fontSize: 25,
                  fontFamily: 'Lato-Black',
                  fontWeight: FontWeight.bold),
            ),
          ),
         //todo
         // parseHtml(policies.termsConditions),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Shipping",
                style: TextStyle(
                    fontSize: 25,
                    fontFamily: 'Lato-Black',
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
         //todo
         // parseHtml(policies.shipping),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 20, 0, 20),
            child: Align(
              alignment: Alignment.topLeft,
              child: Text(
                "Cancellation",
                style: TextStyle(
                    fontSize: 25,
                    fontFamily: 'Lato-Black',
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
         // parseHtml(policies.cancellation),
        ],
      ),
    );
  }
}
