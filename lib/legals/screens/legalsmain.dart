import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:safalsathi/legals/screens/terms_condition.dart';

import 'legals.dart';

class LegalsMain extends StatefulWidget {
  _LegalsState createState() => _LegalsState();
}

class _LegalsState extends State<LegalsMain>
    with SingleTickerProviderStateMixin {
  TabController _tabController;

  @override
  void initState() {
    _tabController = new TabController(length: 2, vsync: this);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: new Text("Policies", style: TextStyle(color: Colors.white)),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.white,
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: Column(
        children: <Widget>[
          Container(
            color: Colors.lightGreen,
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: TabBar(
                controller: _tabController,
                unselectedLabelColor: Colors.white60,
                indicatorColor: Colors.lightGreen,
                labelColor: Colors.white,
                tabs: <Widget>[
                  new Text("Terms & Condition"),
                  new Text("Privacy Policy")
                ],
              ),
            ),
          ),
          Expanded(
            child: TabBarView(
              children: [
                LegalsPage(),
                LegalsTerm(),
              ],
              controller: _tabController,
            ),
          ),
        ],
      ),
    );
  }
}
