import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/changePassword/bloc/bloc.dart';
import 'package:safalsathi/common/color.dart';
import 'package:safalsathi/common/common_ui.dart';

import 'bloc/edit_account_bloc.dart';
import 'bloc/edit_account_event.dart';

class EditAccountSetting extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return EditAccountSettingState();
  }
}

class EditAccountSettingState extends State<EditAccountSetting> {
  GlobalKey<FormState> _validationKeyRegister = new GlobalKey();
  TextEditingController oldPasswordController = new TextEditingController();
  TextEditingController newPasswordController = new TextEditingController();
  TextEditingController reNewPasswordController = new TextEditingController();
  EditAccountBloc _editAccountBloc = EditAccountBloc();

  @override
  void dispose() {
    super.dispose();
    _editAccountBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Change Password"),
        ),
        body: BlocListener(
          bloc: _editAccountBloc,
          listener: (context, state) {
            if (state is PasswordUpdated) {
              buildSnackSuccess(context, state.msg);
              //show Snack SUCCESS
            }
            if (state is PasswordNetworkFailure) {
              buildSnackError(context, state.msg);
              //show snakc Failure Due to network
            }
            if (state is PasswordUpdateFailed) {
              buildSnackError(context, state.msg);
              //show snack Update Failure Message
            }
          },
          child: Form(
            key: _validationKeyRegister,
            child: Column(
              children: <Widget>[
                TextField(
                  controller: oldPasswordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'Old Password',
                    hintStyle: TextStyle(color: Colors.grey),
                  ),
                ),
                TextField(
                  controller: newPasswordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'New Password',
                    hintStyle: TextStyle(color: Colors.grey),
                  ),
                ),
                TextField(
                  controller: reNewPasswordController,
                  obscureText: true,
                  decoration: InputDecoration(
                    border: InputBorder.none,
                    hintText: 'PLEASE ENTER YOUR EMAIL',
                    hintStyle: TextStyle(color: Colors.grey),
                  ),
                ),
                RaisedButton(
                  child: Text(
                    "Reset Password",
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(color: AppColors.WHITE),
                  ),
                  onPressed: () {
                    if (_validationKeyRegister.currentState.validate()) {
                      _editAccountBloc.add(SubmitPassword(
                          oldPasswordController.text,
                          newPasswordController.text,
                          reNewPasswordController.text));
                    }
                  },
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
