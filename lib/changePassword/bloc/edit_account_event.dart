import 'package:meta/meta.dart';

@immutable
abstract class EditAccountEvent {}

class SubmitPassword extends EditAccountEvent {
  final String oldPass, newPass, reNewPass;

  SubmitPassword(this.oldPass, this.newPass, this.reNewPass);

  @override
  String toString() => 'onChangeButtonPress';
}
