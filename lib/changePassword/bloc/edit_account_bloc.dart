import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/AccountPage/model/changePasswordResponse.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/error_handler.dart';
import 'package:safalsathi/common/generic_response.dart';
import 'package:safalsathi/common/string.dart';

import 'bloc.dart';

class EditAccountBloc extends Bloc<EditAccountEvent, EditAccountState> {
  @override
  EditAccountState get initialState => InitialEditAccountState();

  @override
  Stream<EditAccountState> mapEventToState(
    EditAccountEvent event,
  ) async* {
    if (event is SubmitPassword) {
      ChangePasswordRepository _changePasswordRepository =
          ChangePasswordRepository();
      ChangePasswordResponse changePasswordResponse;
      ApiResponse _apiResponse;
      try {
        _apiResponse = await _changePasswordRepository.updatePassword(
            event.oldPass, event.newPass, event.reNewPass);
        if (_apiResponse.apiCall == ApiResultStatus.SUCCESS) {
          changePasswordResponse =
              ChangePasswordResponse.fromJsonMap(_apiResponse.responseData);
          yield PasswordUpdated(changePasswordResponse.msg);
        } else if (_apiResponse.apiCall == ApiResultStatus.ERROR) {
          changePasswordResponse =
              ChangePasswordResponse.fromJsonMap(_apiResponse.responseData);
          yield PasswordUpdateFailed(changePasswordResponse.msg);
        } else if (_apiResponse.apiCall == ApiResultStatus.NO_NETWORK) {
          changePasswordResponse =
              ChangePasswordResponse.fromJsonMap(_apiResponse.responseData);
          yield PasswordNetworkFailure(changePasswordResponse.msg);
        }
      } catch (e) {
        yield PasswordUpdateFailed("Ops! Something Went Wrong \n $e");
      }
    }
  }
}

class ChangePasswordRepository {
  ChangePasswordApiProvider _changePasswordApiProvider =
      ChangePasswordApiProvider();

  Future<ApiResponse> updatePassword(
      String oldPass, String newPass, String reNewPass) {
    return _changePasswordApiProvider.postPassword(oldPass, newPass, reNewPass);
  }
}

class ChangePasswordApiProvider {
  Future<ApiResponse> postPassword(
      String oldPass, String newPass, String reNewPass) async {
    FormData formData2 = FormData.fromMap(
      {
        "current_password": "$oldPass",
        "password": "$newPass",
        "password_confirmation": "$reNewPass",
      },
    );
    Response response;
    try {
      response = await dioBASE(await getAuth())
          .post(urlPasswordChange, data: formData2);
      print(response);
      print("TEST OUTPUT 5${response.data}");
      return ApiResponse(
          responseData: response.data,
          apiCall: ApiResultStatus.SUCCESS,
          message: "No Error");
    } on DioError catch (e) {
      if (e.type == DioErrorType.DEFAULT)
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.NO_NETWORK,
            message:
                "Make sure wifi or cellular data is turned on, Then try again");
      else if (e.type == DioErrorType.RESPONSE &&
          e.response.statusCode == 406) {
        return ApiResponse(
            responseData: e.response.data,
            apiCall: ApiResultStatus.SUCCESS,
            message: handleError(e));
      } else
        return ApiResponse(
          responseData: e,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(e),
        );
    } catch (error) {
      print("DIO Api Error");

      return ApiResponse(
          responseData: error,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(error));
    }
  }
}

Future<ChangePasswordResponse> _changePassword(
    String oldPass, String newPass, String reNewPass) async {
  FormData formData2 = FormData.fromMap(
    {
      "current_password": "$oldPass",
      "password": "$newPass",
      "password_confirmation": "$reNewPass",
    },
  );

  try {
    Response response =
        await dioBASE(await getAuth()).post(urlPasswordChange, data: formData2);
    ChangePasswordResponse.fromJsonMap(response.data);
  } catch (error) {
    return null;
  }
  return null;
}
