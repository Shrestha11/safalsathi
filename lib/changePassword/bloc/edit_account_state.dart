import 'package:meta/meta.dart';

@immutable
abstract class EditAccountState {}

class InitialEditAccountState extends EditAccountState {}

class PasswordUpdated extends EditAccountState {
  final String msg;

  PasswordUpdated(this.msg);
}

class PasswordUpdateFailed extends EditAccountState {
  final String msg;

  PasswordUpdateFailed(this.msg);
}

class PasswordNetworkFailure extends EditAccountState {
  final String msg;

  PasswordNetworkFailure(this.msg);
}
