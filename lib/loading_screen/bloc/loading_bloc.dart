import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/home_main/repository/home_repository.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/error_handler.dart';
import 'package:safalsathi/common/generic_response.dart';
import 'package:safalsathi/common/string.dart';
import 'package:safalsathi/loading_screen/model/searchHint.dart';

import './bloc.dart';

class LoadingBloc extends Bloc<LoadingEvent, LoadingState> {
  HomeRepository _homeRepository = HomeRepository();
  @override
  LoadingState get initialState => InitialLoadingState();

  @override
  Stream<LoadingState> mapEventToState(
    LoadingEvent event,
  ) async* {
    //LoadingDataRepository _loadingDataRepository = LoadingDataRepository();
    //SharedPreferences prefs = await SharedPreferences.getInstance();
    if (event is LoadingEvent) {
      yield LoadingDataState('Search Query');
      //try {
      //  ApiResponse _apiResponseSearch =
        //    await _loadingDataRepository.loadSearchResponse();
        //SearchHint _searchHint =
          //  SearchHint.fromJsonMap(_apiResponseSearch.responseData);
        //await prefs.setStringList('mylist', _searchHint.list);
      try {
        ApiResponse _apiResponse = await _homeRepository.getHome();
        if (_apiResponse.apiCall == ApiResultStatus.ERROR) {
          yield LoadingError(_apiResponse.message);
        } else if (_apiResponse.apiCall == ApiResultStatus.SUCCESS) {
          try {
            yield LoadComplete();
          } catch (e) {
            print("ERROR PARSING $e");
          }
        } else if (_apiResponse.apiCall == ApiResultStatus.NO_NETWORK) {
          yield LoadingError(_apiResponse.message);
        }
      } catch (e) {
        yield LoadingError(e.toString());
      }

     //   yield LoadComplete();
      //} catch (e) {
        //yield LoadingError(e.toString());
       // print(e);
     // }
    }
  }
}

/*class LoadingDataRepository {
  LoadingApiProvider _loadingApiProvider = LoadingApiProvider();

  Future<ApiResponse> loadSearchResponse() {
    return _loadingApiProvider.loadSearchHint();
  }
}

class LoadingApiProvider {
  Future<ApiResponse> loadSearchHint() async {
    try {
      Response response = await dioBASE().get(urlSearchHint);
      print(response);
      return ApiResponse(
          responseData: response.data,
          apiCall: ApiResultStatus.SUCCESS,
          message: "No Error");
    } on DioError catch (e) {
      print("DIO ERROR $e");
      if (e.type == DioErrorType.DEFAULT)
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.NO_NETWORK,
            message:
                "Make sure wifi or cellular data is turned on, Then try again");
//      else
      return ApiResponse(
          responseData: e,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(e));
    } catch (error) {
      print("DIO Api Error");

      return ApiResponse(
          responseData: error,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(error));
    }
  }
}*/
