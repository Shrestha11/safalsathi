import 'package:meta/meta.dart';

@immutable
abstract class LoadingEvent {}

class LoadingStart extends LoadingEvent {}
