import 'package:meta/meta.dart';

@immutable
abstract class LoadingState {}

class InitialLoadingState extends LoadingState {}

class LoadingDataState extends LoadingState {
  final String string;

  LoadingDataState(this.string);
}

class LoadComplete extends LoadingState {}

class LoadingError extends LoadingState {
  final String string;

  LoadingError(this.string);
}
