import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/home_screen/home.dart';
import 'package:safalsathi/loading_screen/bloc/bloc.dart';

class LoadingScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Colors.white70,
        body: Stack(
          children: <Widget>[

      Container(
        width: MediaQuery.of(context).size.width,
        height: MediaQuery.of(context).size.height,

        decoration: new BoxDecoration(
          gradient: new LinearGradient(
            begin: FractionalOffset.topLeft,
            end: FractionalOffset.bottomRight,
            // colors: [AppColors.GRADIENT_START, AppColors.GRADIENT_END],
            colors:[Colors.deepOrange,Colors.orange],
          ),
        ),
      ),
            Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Image.asset(
                  'images/safal.jpeg',
                ),
                LoadingStates(),
                CircularProgressIndicator(),
              ],
            ),
          ],
        ));
  }
}

class LoadingStates extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return LoadingState();
  }
}

class LoadingState extends State<LoadingStates> {
  LoadingBloc _loadingBloc = LoadingBloc();
  String a = "loading";

  @override
  void initState() {
    super.initState();
    _loadingBloc.add(LoadingStart());
  }

  @override
  void dispose() {
    super.dispose();
    _loadingBloc.close();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _loadingBloc,
      listener: (context, state) {
        if (state is LoadingDataState) {
          a = state.string;
        }
        if (state is LoadComplete) {
          Navigator.pushReplacement(context,
              MaterialPageRoute(builder: (BuildContext context) => Home()));

        }
      },
      child: BlocBuilder(
          bloc: _loadingBloc,
          builder: (context, state) {
            if (state is LoadingDataState) {}
            return Container();
          }),
    );
  }
}
