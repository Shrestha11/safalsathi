import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/checkOut/CheckOut.dart';

import 'package:safalsathi/common/Database/databaseModel.dart';
import 'package:safalsathi/common/model/products.dart';
import 'package:safalsathi/common/color.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/common_ui.dart';

import '../app_styles.dart';

import 'Cart/cart_bloc.dart';
import 'Cart/cart_event.dart';
import 'Cart/cart_state.dart';


class MyCart extends StatefulWidget {
  @override
  // State<StatefulWidget> createState() {
  // return _MyCartState();
  //_LegalsState createState() => _LegalsState();
  _MyCartState createState() => _MyCartState();
}

class _MyCartState extends State<MyCart> {
  CartBloc _cartBloc = new CartBloc();
  List<Cart> cartList;
  Products products;
  bool error = false;
  String message = "";
  String calculatedPrice = "", taxAmount = "", finalPrice = "";

  @override
  void initState() {
    super.initState();
    _cartBloc.add(LoadDatabase());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("My Cart",style: TextStyle(color:Colors.white),),
          backgroundColor: Colors.deepOrange,
        iconTheme: IconThemeData(color: Colors.white),
      ),
      body: BlocListener(
        bloc: _cartBloc,
        listener: (context, _cartState) {
          if (_cartState is ErrorState) {
            buildSnackError(context, _cartState.message);
          }
          if (_cartState is SuccessState) {
            buildSnackSuccess(context, _cartState.msg);
          }
          if (_cartState is UserNotLoggedIn) {
            print("UserNotLoggedIn");
            buildUserNotLoggedIn(context, _cartState.message);
          }
          if (_cartState is CartEmpty) {
            buildSnackError(context, "Empty Cart");
          }
          if (_cartState is IntentState) {
            /*Navigator.push(
                context,
                MaterialPageRoute(
                  maintainState: false,
                  builder: (context) => CheckOut(),
                ));*/
          }
          if (_cartState is LoadedDatabse) {
//                        if (_cartState.cartList == null) print("NULL"); else
//                          print("NOT NULL ${cartList.length}");
            cartList = _cartState.cartList;
            print("ddddddd ${cartList[0].detail}");
            print("vvvvvvvvvvvv $cartList");
          }
        },
        child: Column(
          children: <Widget>[
            Expanded(
              child:  ListView(
                //shrinkWrap: true,
                children: <Widget>[
                  // Expanded(
                  // child:
                  BlocBuilder(
                      bloc: _cartBloc,
                      builder: (BuildContext context, CartState _cartState) {
                        if (_cartState is CartEmpty) {
                          return Center(child: Text("Empty Cart"));
                        }

                        print("jjjjjjjjjjjjjjjjjjjjjjjj");

                        return Center(
                          child: cartList == null
                              ? Text("EMPTY ")
                              : ListView.builder(
                            shrinkWrap: true,
                            physics: ClampingScrollPhysics(),
                            itemCount: cartList.length,
                            itemBuilder: (context, index) {
                              products = decode(cartList[index].detail);
                              return Card(
                                child: Row(
                                  children: <Widget>[
                                    Image.network(
                                      products.imgs[0].mediumUrl,
                                      width:
                                      MediaQuery.of(context).size.width / 3,
                                    ),
                                    Container(
                                      width: ((2 * MediaQuery.of(context).size.width) / 3) - 8.0,
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                        MainAxisAlignment.spaceEvenly,
                                        children: <Widget>[
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                8.0, 4.0, 8.0, 4.0),
                                            child: Text(
                                              products.name,
                                              style: blackH1Text,
                                              maxLines: 2,
                                              overflow: TextOverflow.ellipsis,
                                            ),
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Card(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      2.0),
                                                ),
                                                child: Padding(
                                                  padding:
                                                  const EdgeInsets.fromLTRB(
                                                      4.0, 2.0, 4.0, 4.0),
                                                  child: Text(
                                                    cartList[index].size == null
                                                        ? ""
                                                        : cartList[index].size,
                                                    style: greyH6Text,
                                                  ),
                                                ),
                                              ),
                                              Card(
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(
                                                      2.0),
                                                ),
                                                child: Padding(
                                                  padding:
                                                  const EdgeInsets.fromLTRB(
                                                      4.0, 2.0, 4.0, 2.0),
                                                  child: Text(
                                                    cartList[index].color ==
                                                        null
                                                        ? ""
                                                        : cartList[index].color,
                                                    style: greyH6Text,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            children: <Widget>[
                                              Padding(
                                                padding:
                                                const EdgeInsets.fromLTRB(
                                                    8.0, 4.0, 8.0, 4.0),
                                                child: Text(
                                                    products.getsalePrice(),
                                                    style: greyH4Text),
                                              ),
                                              Padding(
                                                padding:
                                                const EdgeInsets.fromLTRB(
                                                    8.0, 4.0, 8.0, 4.0),
                                                child: Text(
                                                  cartList[index].getqty(),
                                                  style: blackH4Text,
                                                ),
                                              )
                                            ],
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                8.0, 4.0, 8.0, 4.0),
                                            child: Text(
                                              products.getsalePriceQty(
                                                  cartList[index].qty),
                                              style: blackH3Text,
                                            ),
                                          ),
                                          Padding(
                                            padding: const EdgeInsets.fromLTRB(
                                                8.0, 4.0, 8.0, 4.0),
                                            child: Row(
                                              children: <Widget>[
                                                Icon(
                                                  Icons.edit,
                                                  color: Colors.red,
                                                  size: 20.0,
                                                ),
                                                InkWell(
                                                  onTap: () => _cartBloc.add(
                                                      DeleteItem(cartList[index].cartId,

                                                          cartList[index].detail)),
                                                  child: Icon(
                                                    Icons.delete,
                                                    color: Colors.red,
                                                    size: 20.0,
                                                  ),
                                                ),
                                              ],
                                            ),
                                          )
                                        ],
                                      ),
                                    ),
                                  ],
                                ),
                              );
                            },
                          ),
                        );
                      }),
                  //),
                  BlocBuilder(
                    bloc: _cartBloc,
                    builder: (BuildContext context, CartState _cartState) {

                      if (_cartState is LoadedDatabse) {
                        calculatedPrice = _cartState.calculatedPrice;
                        taxAmount = _cartState.taxAmount;
                        finalPrice = _cartState.finalPrice;
                        return cartList == null
                            ? Container()
                            :

                        Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Text(
                                    "Total Price",
                                    style: blackH1Text,
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text("Calculated Price",
                                          style: blackH5Text),
                                      Text(
                                        calculatedPrice,
                                        style: greyH5Text,
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Tax Amount",
                                        style: blackH5Text,
                                      ),
                                      Text(
                                        taxAmount,
                                        style: greyH5Text,
                                      ),
                                    ],
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Row(
                                    mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      Text(
                                        "Final Price",
                                        style: blackH5TextBold,
                                      ),
                                      Text(
                                        finalPrice,
                                        style: greyH5TextBold,
                                      ),
                                    ],
                                  ),
                                ),
                              ],
                            ),
                          ),

                        );
                      }
                      return Text("");
                     /* return cartList == null
                          ? Container()
                          : Card(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child:

                          Column(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[

                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(
                                  "Total Price",
                                  style: blackH1Text,
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text("Calculated Price",
                                        style: blackH5Text),
                                    Text(
                                      calculatedPrice,
                                      style: greyH5Text,
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "Tax Amount",
                                      style: blackH5Text,
                                    ),
                                    Text(
                                      taxAmount,
                                      style: greyH5Text,
                                    ),
                                  ],
                                ),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  mainAxisAlignment:
                                  MainAxisAlignment.spaceBetween,
                                  children: <Widget>[
                                    Text(
                                      "Final Price",
                                      style: blackH5TextBold,
                                    ),
                                    Text(
                                      finalPrice,
                                      style: greyH5TextBold,
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                        ),
                      );*/
                    },
                  ),
                ],
              ),),
            Align(
              //alignment: FractionalOffset.bottomCenter,
              child: ButtonTheme(
                minWidth: MediaQuery.of(context).size.width,
                height: 50,
                buttonColor: Colors.red,
                child: RaisedButton(
                    elevation: 0.0,
                    child: Text(
                      "CheckOut",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      //_cartBloc.add(IntentCheckout());
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                            maintainState: false,
                            builder: (context) => CheckOut(),
                          ));
                    }),
              ),
            ),
          ],
        ),
      ),
    );
  }
}