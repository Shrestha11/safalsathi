import 'package:meta/meta.dart';
import 'package:safalsathi/common/Database/databaseModel.dart';

@immutable
abstract class CartState {}

class InitialCartState extends CartState {}

class LoadedDatabse extends CartState {
  final List<Cart> cartList;
  final String calculatedPrice, taxAmount, finalPrice;

  LoadedDatabse(
      this.cartList, this.calculatedPrice, this.taxAmount, this.finalPrice);

  @override
  String toString() => 'LoadedDatabse State';
}

class CartEmpty extends CartState {
  @override
  String toString() => 'CartEmpty';
}

class ErrorState extends CartState {
  final String message;

  ErrorState(this.message);

  @override
  String toString() => 'Error State';
}

class SuccessState extends CartState {
  final String msg;

  SuccessState(this.msg);

  @override
  String toString() => 'Item Deleted';
}

class UserNotLoggedIn extends CartState {
  final String message;

  UserNotLoggedIn(this.message);

  @override
  String toString() => 'User Not Logged In';
}

class IntentState extends CartState {
  @override
  String toString() => 'IntentState';
}
