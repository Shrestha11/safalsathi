import 'package:meta/meta.dart';

@immutable
abstract class CartEvent {}

class LoadDatabase extends CartEvent {
  @override
  String toString() => 'Load Database Event';
}

class DeleteItem extends CartEvent {
  final int id;
  final String name;

  DeleteItem(this.id, this.name);

  @override
  String toString() => 'Delete Item';
}
class IntentCheckout extends CartEvent{
  @override
  String toString() => 'Intent checkout';
}
