import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/Database/DatabaseHelper.dart';
import 'package:safalsathi/common/Database/databaseModel.dart';
import 'package:safalsathi/common/model/products.dart';

import 'bloc.dart';

class CartBloc extends Bloc<CartEvent, CartState> {
  DatabaseHelper _databaseHelper = DatabaseHelper();
  List<Cart> cartList;
  double calculatedPrice, taxTotalAmount, finalPrice, taxAmount;

  @override
  CartState get initialState => InitialCartState();

  @override
  Stream<CartState> mapEventToState(
    CartEvent event,
  ) async* {
    if (event is LoadDatabase) {
      print("aaaa");
      try {
        cartList = await _databaseHelper.fetchAll();
        //print("sssss ${cartList.first.detail}");
       // if (cartList.length != 0) {
          if(cartList !=null){
          calculatedPrice = 0;
          taxAmount = 0;
          finalPrice = 0;
          for (int i = 0; i < cartList.length; i++) {
            Products _products = decode(cartList[i].detail);
            int taxPercent = _products.tax == null ? 0 : _products.tax;
            taxAmount = taxAmount +
                ((taxPercent * (_products.salesPrice * cartList[i].qty)) / 100);
            calculatedPrice =
                calculatedPrice + (_products.salesPrice * cartList[i].qty);
          }
          finalPrice = calculatedPrice + taxAmount;
          yield (LoadedDatabse(cartList, "Rs. $calculatedPrice",
              "Rs. $taxAmount", "Rs. $finalPrice"));
        } else
          yield CartEmpty();
      } on NoSuchMethodError catch (e) {
        yield (ErrorState("No item in cart $e"));
      } catch (e) {
        yield (ErrorState(e.toString()));
      }
    }
    if (event is DeleteItem) {
      var response = await _databaseHelper.removeItem(event.id);
      if (response == 1) {
        //yield (SuccessState("Deleted ${event.name}"));
        yield (SuccessState("Deleted"));
        add(LoadDatabase());
      } else
       // yield (ErrorState("Unable to Delete ${event.name}"));
        yield (ErrorState("Unable to Delete"));
    }
    if (event is IntentCheckout) {
      if ((await getAuth()) == null) {
        yield (UserNotLoggedIn("User Not Logged In"));
      } else
        yield IntentState();
    }
  }
}
