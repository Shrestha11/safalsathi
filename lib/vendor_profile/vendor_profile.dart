import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/common/common_ui.dart';

import 'bloc/vendor_profile_bloc.dart';
import 'bloc/vendor_profile_event.dart';
import 'bloc/vendor_profile_state.dart';
import 'model/VendorProfileResponse.dart';

class VendorProfile extends StatelessWidget {
  final String shopName;

  VendorProfile(this.shopName);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Services(shopName),
    );
  }
}

class Services extends StatefulWidget {
  final String shopName;

  Services(this.shopName);

  @override
  _ServiceState createState() {
    return _ServiceState();
  }
}

class _ServiceState extends State<Services> {
  VendorProfileBloc _vendorProfileBloc = VendorProfileBloc();

  @override
  void initState() {
    super.initState();
    _vendorProfileBloc.add(LoadVendorInformationEvent(widget.shopName));
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: _vendorProfileBloc,
        builder: (context, state) {
          if (state is LoadingVendorProfileState) {
            return Center(child: Text("Loading"));
          }
          if (state is LoadingErrorVendorProfileState) {
            return Center(child: Text("Loading Error"));
          }
          if (state is LoadedVendorProfileState) {
            return buildUserWidget(context, state.vendorProfileResponse);
          }
          return Container();
        });
  }

  Widget buildUserWidget(
      BuildContext context, VendorProfileResponse vendorProfileResponse) {
    return ListView(children: <Widget>[
      Container(
        height: MediaQuery.of(context).size.width / 1.2,
        width: MediaQuery.of(context).size.width,
        child: Stack(children: <Widget>[
          ClipPath(
            child: Container(
              height: MediaQuery.of(context).size.width / 1.3,
              width: MediaQuery.of(context).size.width,
              decoration: BoxDecoration(
                image: DecorationImage(
                    image: NetworkImage(
                      vendorProfileResponse.image,
                    ),
                    fit: BoxFit.cover),
              ),
            ),
            clipper: CustomClipPath(),
          ),
          Positioned(
            left: 10,
            bottom: 25,
            child: Column(
              children: <Widget>[
                FloatingActionButton(
                  onPressed: () =>
                      launchPhone(vendorProfileResponse.vendor.primary_phone),
                  backgroundColor: Colors.white,
                  child: ClipOval(
                    child: Container(
                        height: 52,
                        width: 52,
                        color: Colors.white,
                        child: Icon(
                          Icons.phone,
                          color: Colors.red,
                        )),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Call",
                    style: TextStyle(
                        fontSize: 16,
                        fontWeight: FontWeight.w400,
                        color: Colors.black),
                  ),
                ),
              ],
            ),
          ),
          Positioned(
            right: 10,
            bottom: 25,
            child: Column(
              children: <Widget>[
                FloatingActionButton(
                  onPressed: () =>
                      launchEmail(vendorProfileResponse.vendor.primary_email),
                  backgroundColor: Colors.white,
                  child: ClipOval(
                    child: Container(
                        color: Colors.white,
                        height: 52,
                        width: 52,
                        child: Icon(
                          Icons.email,
                          color: Colors.red,
                        )),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    "Email",
                    style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: Colors.black),
                  ),
                )
              ],
            ),
          ),
          Positioned(
            left: 50,
            bottom: 20,
            right: 50,
            child: Column(
              children: <Widget>[
                Container(
                  height: 140,
                  width: 140,
                  foregroundDecoration: new BoxDecoration(
                      color: Colors.white,
                      borderRadius:
                          new BorderRadius.all(new Radius.circular(100.0)),
                      image: DecorationImage(
                          image: NetworkImage(vendorProfileResponse.image))),
                ),
              ],
            ),
          ),
        ]),
      ),
      Container(
        child: Column(
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.edit_location,
                    color: Colors.black,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    vendorProfileResponse.vendor.address,
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.email,
                    color: Colors.black,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    vendorProfileResponse.vendor.primary_email,
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Icon(
                    Icons.phone,
                    color: Colors.black,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    vendorProfileResponse.vendor.primary_phone,
                    style: TextStyle(
                      fontSize: 15,
                      fontWeight: FontWeight.w400,
                      color: Colors.black,
                    ),
                  ),
                ),
              ],
            ),
            vendorProfileResponse.vendor.description == null
                ? Container()
                : Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      vendorProfileResponse.vendor.description,
                      style: TextStyle(
                        fontSize: 15,
                        fontWeight: FontWeight.w400,
                        color: Colors.black,
                      ),
                      maxLines: 5,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
          ],
        ),
      )
    ]);
  }
}

class CustomClipPath extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    Path path = Path();
    path.lineTo(0, size.height - 60);
    path.quadraticBezierTo(
        size.width / 2, size.height, size.width, size.height - 60);
    path.lineTo(size.width, 0);
    path.close();

    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) => false;
}
