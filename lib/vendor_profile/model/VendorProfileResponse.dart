import 'package:safalsathi/vendor_profile/model/vendor.dart';

class VendorProfileResponse {

  final Object ad;
  final Vendor vendor;
  final String image;

	VendorProfileResponse.fromJsonMap(Map<String, dynamic> map): 
		ad = map["ad"],
		vendor = Vendor.fromJsonMap(map["vendor"]),
		image = map["image"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['ad'] = ad;
		data['vendor'] = vendor == null ? null : vendor.toJson();
		data['image'] = image;
		return data;
	}
}
