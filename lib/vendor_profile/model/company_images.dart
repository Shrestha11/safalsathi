
class Company_images {

  final int id;
  final int vendor_details_id;
  final String image;
  final String created_at;
  final String updated_at;

	Company_images.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		vendor_details_id = map["vendor_details_id"],
		image = map["image"],
		created_at = map["created_at"],
		updated_at = map["updated_at"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['vendor_details_id'] = vendor_details_id;
		data['image'] = image;
		data['created_at'] = created_at;
		data['updated_at'] = updated_at;
		return data;
	}
}
