
import 'company_images.dart';

class Vendor {

  final int id;
  final int user_id;
  final String name;
  final String vendor_type;
  final Object description;
  final String type;
  final String pan_number;
  final Object registration_no;
  final String primary_email;
  final Object secondary_email;
  final String primary_phone;
  final Object secondary_phone;
  final String address;
  final Object seo_keywords;
  final Object seo_description;
  final String created_at;
  final String updated_at;
  final int verified;
  final String tax_clearance;
  final List<Company_images> company_images;

	Vendor.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		user_id = map["user_id"],
		name = map["name"],
		vendor_type = map["vendor_type"],
		description = map["description"],
		type = map["type"],
		pan_number = map["pan_number"],
		registration_no = map["registration_no"],
		primary_email = map["primary_email"],
		secondary_email = map["secondary_email"],
		primary_phone = map["primary_phone"],
		secondary_phone = map["secondary_phone"],
		address = map["address"],
		seo_keywords = map["seo_keywords"],
		seo_description = map["seo_description"],
		created_at = map["created_at"],
		updated_at = map["updated_at"],
		verified = map["verified"],
		tax_clearance = map["tax_clearance"],
		company_images = List<Company_images>.from(map["company_images"].map((it) => Company_images.fromJsonMap(it)));

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['user_id'] = user_id;
		data['name'] = name;
		data['vendor_type'] = vendor_type;
		data['description'] = description;
		data['type'] = type;
		data['pan_number'] = pan_number;
		data['registration_no'] = registration_no;
		data['primary_email'] = primary_email;
		data['secondary_email'] = secondary_email;
		data['primary_phone'] = primary_phone;
		data['secondary_phone'] = secondary_phone;
		data['address'] = address;
		data['seo_keywords'] = seo_keywords;
		data['seo_description'] = seo_description;
		data['created_at'] = created_at;
		data['updated_at'] = updated_at;
		data['verified'] = verified;
		data['tax_clearance'] = tax_clearance;
		data['company_images'] = company_images != null ? 
			this.company_images.map((v) => v.toJson()).toList()
			: null;
		return data;
	}
}
