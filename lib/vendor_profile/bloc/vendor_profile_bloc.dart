import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/error_handler.dart';
import 'package:safalsathi/common/generic_response.dart';
import 'package:safalsathi/vendor_profile/model/VendorProfileResponse.dart';

import './bloc.dart';

class VendorProfileBloc extends Bloc<VendorProfileEvent, VendorProfileState> {
  @override
  VendorProfileState get initialState => InitialVendorProfileState();

  @override
  Stream<VendorProfileState> mapEventToState(
    VendorProfileEvent event,
  ) async* {
    ApiResponse apiResponse = ApiResponse();
    VendorProfileRepository _vendorProfileRepository =
        VendorProfileRepository();
    if (event is LoadVendorInformationEvent) {
      yield LoadingVendorProfileState();
      try {
        apiResponse =
            await _vendorProfileRepository.getVendorProfileInfo(event.shopName);
        print("Response ${apiResponse.apiCall}");
        if (apiResponse.apiCall == ApiResultStatus.ERROR) {
          yield LoadingErrorVendorProfileState(apiResponse.message);
        } else if (apiResponse.apiCall == ApiResultStatus.SUCCESS) {
          yield LoadedVendorProfileState(
              VendorProfileResponse.fromJsonMap(apiResponse.responseData));
        } else if (apiResponse.apiCall == ApiResultStatus.NO_NETWORK) {
          yield LoadingErrorVendorProfileState(apiResponse.message);
        }
      } catch (e) {
        print("CATCH $e");
        yield LoadingErrorVendorProfileState(e.toString());
      }
    }
  }
}

class VendorProfileRepository {
  VendorProfileApiProvider _vendorProfileApiProvider =
      VendorProfileApiProvider();

  Future<ApiResponse> getVendorProfileInfo(String shopName) {
    return _vendorProfileApiProvider.loadVendorProfileInfo(shopName);
  }
}

class VendorProfileApiProvider {
  Future<ApiResponse> loadVendorProfileInfo(String shopName) async {
    print(shopName);
    try {
      Response response = await dioBASE().get("/mall-profile?name=$shopName");
      print(response);
      return ApiResponse(
          responseData: response.data,
          apiCall: ApiResultStatus.SUCCESS,
          message: "No Error");
    } on DioError catch (e) {
      print("DIO ERROR $e");
      if (e.type == DioErrorType.DEFAULT)
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.NO_NETWORK,
            message:
                "Make sure wifi or cellular data is turned on, Then try again");
//      else
      return ApiResponse(
          responseData: e,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(e));
    } catch (error) {
      print("DIO Api Error");

      return ApiResponse(
          responseData: error,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(error));
    }
  }
}
