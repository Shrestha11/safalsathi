import 'package:equatable/equatable.dart';

abstract class VendorProfileEvent extends Equatable {}

class LoadVendorInformationEvent extends VendorProfileEvent {
  final String shopName;

  LoadVendorInformationEvent(this.shopName);

  @override
  // TODO: implement props
  List<Object> get props => null;
}
