import 'package:equatable/equatable.dart';
import 'package:safalsathi/vendor_profile/model/VendorProfileResponse.dart';

abstract class VendorProfileState extends Equatable {}

class InitialVendorProfileState extends VendorProfileState {
  @override
  List<Object> get props => [];
}

class LoadingVendorProfileState extends VendorProfileState {
  @override
  // TODO: implement props
  List<Object> get props => null;
}

class LoadingErrorVendorProfileState extends VendorProfileState {
  final String message;

  LoadingErrorVendorProfileState(this.message);

  @override
  // TODO: implement props
  List<Object> get props => null;
}

class LoadedVendorProfileState extends VendorProfileState {
  final VendorProfileResponse vendorProfileResponse;
  LoadedVendorProfileState(this.vendorProfileResponse);

  @override
  // TODO: implement props
  List<Object> get props => null;
}
