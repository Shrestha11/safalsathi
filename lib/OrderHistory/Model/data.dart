import 'order_products.dart';

class Data {
  final int id;
 final String order_status;
  final String order_date;
  final List<OrderProducts> orderProducts;
  final int subtotal;
  final dynamic tax;
  final int discount;
  final String shippingAmount;
  final dynamic grandtotal;

  Data.fromJsonMap(Map<String, dynamic> map)
      : id = map["id"],
        order_status = map["order_status"],
        order_date = map["order_date"],
        orderProducts = List<OrderProducts>.from(
            map["orderProducts"].map((it) => OrderProducts.fromJsonMap(it))),
        subtotal = map["subtotal"],
        tax = map["tax"],
        discount = map["discount"],
        shippingAmount = map["shipping_amount"],
        grandtotal = map["grandtotal"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['order_status'] = order_status;
    data['order_date'] = order_date;
    data['orderProducts'] = orderProducts != null
        ? this.orderProducts.map((v) => v.toJson()).toList()
        : null;
    data['subtotal'] = subtotal;
    data['tax'] = tax;
    data['discount'] = discount;
    data['shipping_amount'] = shippingAmount;
    data['grandtotal'] = grandtotal;
    return data;
  }

  String getgrandTotal() {
    print("ToTAL  ${grandtotal.toString()}");
    return "Rs. " + grandtotal.toString();
  }
}
