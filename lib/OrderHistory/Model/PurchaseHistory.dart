import 'data.dart';
import 'links.dart';
import 'meta.dart';

class PurchaseHistory {
  final List<Data> data;
  final Links links;
  final Meta meta;

  PurchaseHistory.fromJsonMap(Map<String, dynamic> map)
      : data = List<Data>.from(map["data"].map((it) => Data.fromJsonMap(it))),
        links = Links.fromJsonMap(map["links"]),
        meta = Meta.fromJsonMap(map["meta"]);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> datas = new Map<String, dynamic>();
    datas['data'] =
        data != null ? this.data.map((v) => v.toJson()).toList() : null;
    datas['links'] = links == null ? null : links.toJson();
    datas['meta'] = meta == null ? null : meta.toJson();
    return datas;
  }
}
