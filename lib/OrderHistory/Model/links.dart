
class Links {

  final String first;
  final String last;
  final Object prev;
  final Object next;

	Links.fromJsonMap(Map<String, dynamic> map): 
		first = map["first"],
		last = map["last"],
		prev = map["prev"],
		next = map["next"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['first'] = first;
		data['last'] = last;
		data['prev'] = prev;
		data['next'] = next;
		return data;
	}
}
