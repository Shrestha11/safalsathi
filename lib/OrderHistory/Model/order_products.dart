class OrderProducts {
  final int orderId;
  final int productId;
  final int qty;
  final String price;
  final int tax;
  final Object discount;
  final int preBooking;
  final Object colour;
  final String size;
  final String createdAt;
  final String updatedAt;
  final String name;
  final int total;
  final String img;

  OrderProducts.fromJsonMap(Map<String, dynamic> map)
      : orderId = map["order_id"],
        productId = map["product_id"],
        qty = map["qty"],
        price = map["price"],
        tax = map["tax"],
        discount = map["discount"],
        preBooking = map["prebooking"],
        colour = map["colour"],
        size = map["size"],
        createdAt = map["created_at"],
        updatedAt = map["updated_at"],
        name = map["name"],
        total = map["total"],
        img = map["img"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['order_id'] = orderId;
    data['product_id'] = productId;
    data['qty'] = qty;
    data['price'] = price;
    data['tax'] = tax;
    data['discount'] = discount;
    data['prebooking'] = preBooking;
    data['colour'] = colour;
    data['size'] = size;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['name'] = name;
    data['total'] = total;
    data['img'] = img;
    return data;
  }

  String getprice() {
    return "Rs. " + price;
  }
}
