
class Meta {

  final int currentPage;
  final int from;
  final int lastPage;
  final String path;
  final int perPage;
  final int to;
  final int total;

	Meta.fromJsonMap(Map<String, dynamic> map): 
		currentPage = map["current_page"],
		from = map["from"],
		lastPage = map["last_page"],
		path = map["path"],
		perPage = map["per_page"],
		to = map["to"],
		total = map["total"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['current_page'] = currentPage;
		data['from'] = from;
		data['last_page'] = lastPage;
		data['path'] = path;
		data['per_page'] = perPage;
		data['to'] = to;
		data['total'] = total;
		return data;
	}
}
