import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';
import 'package:safalsathi/OrderHistory/Bloc/bloc.dart';

@immutable
abstract class PurchaseBlocEvent  {}

class Fetch extends PurchaseBlocEvent {
  @override
  String toString() => 'Fetch';
}

class IntentEvent extends PurchaseBlocEvent {
  final int id;
  final PurchaseblocState purchaseblocstate;

  IntentEvent(this.id,this.purchaseblocstate) {
    print("Intent Event with id=$id ");
  }

  @override
  String toString() => 'Change View';
}
