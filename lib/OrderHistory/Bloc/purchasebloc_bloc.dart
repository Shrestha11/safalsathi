import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/OrderHistory/Model/PurchaseHistory.dart';
import 'package:safalsathi/common/common_ui.dart';

import 'bloc.dart';
import 'package:safalsathi/common//DioBase.dart';


class PurchaseBlocBloc extends Bloc<PurchaseBlocEvent, PurchaseblocState> {
  @override
  PurchaseblocState get initialState => PostUninitialized();

  @override
  Stream<PurchaseblocState> mapEventToState(
    PurchaseBlocEvent event,
  ) async* {
   // if (await getAuth()!=null) {
      if (event is Fetch) {
        try {
          if (initialState is PostUninitialized) {
            final posts = await _fetchPosts(0);
            print(posts);
            //initial starting page number is passed into _fetchPost(pageNumber)
            yield PostLoaded(purchaseHistory: posts, hasReachedMax: false);
            return;
          }

          if (initialState is PostLoaded) {
            final posts = await _fetchPosts(0);
            print(posts);
            yield posts.data.isEmpty
                ? (initialState as PostLoaded).copyWith(hasReachedMax: true)
                : PostLoaded(purchaseHistory: posts, hasReachedMax: false);
            return;
          }
        } catch (e) {
          yield PostError(e.toString());
          return;
        }
      }
      if (event is IntentEvent) {
        if (event.purchaseblocstate is PostLoaded) {
          print("bhbh");
          yield IntentState(event.id);
          return;
        }
      }
    //}
    //yield NotLoggedInState();
  }

  // ignore: unused_element
  bool _hasReachedMax(PurchaseblocState state) =>
      state is PostLoaded && state.hasReachedMax;
}

Future<PurchaseHistory> _fetchPosts(int a) async {
  final String _endpoint = "/my-account/orders";
  try {
    Response response = await dioBASE(await getAuth()).get(_endpoint);
    return PurchaseHistory.fromJsonMap(response.data);
  } catch (error) {
    return null;
  }
}
