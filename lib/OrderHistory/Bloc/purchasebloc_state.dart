import 'package:equatable/equatable.dart';
import 'package:safalsathi/OrderHistory/Model/PurchaseHistory.dart';
import 'package:meta/meta.dart';

@immutable
abstract class PurchaseblocState  {
 // PurchaseblocState([List props = const []]) : super(props);
}

class PostUninitialized extends PurchaseblocState {
  @override
  String toString() => 'PostUninitialized';
}

class PostError extends PurchaseblocState {
  final String error;

  PostError(this.error);

  @override
  String toString() => 'PostError';
}

class IntentState extends PurchaseblocState {
  final int id;

  IntentState(this.id);

  @override
  String toString() => 'Loading';
}

class PostLoaded extends PurchaseblocState {
  final PurchaseHistory purchaseHistory;
  final bool hasReachedMax;

  PostLoaded({this.purchaseHistory, this.hasReachedMax});
     // : super([purchaseHistory, hasReachedMax]);

  PostLoaded copyWith({
    PurchaseHistory purchaseHistory,
    bool hasReachedMax,
  }) {
    return PostLoaded(
      purchaseHistory: purchaseHistory ?? this.purchaseHistory,
      hasReachedMax: hasReachedMax ?? this.hasReachedMax,
    );
  }

  @override
  String toString() => 'PostLoaded { posts: ${purchaseHistory.toString()} }';
}

class NotLoggedInState extends PurchaseblocState {
  @override
  String toString() => 'NotLoggedInState';
}
