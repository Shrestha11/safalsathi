//import 'dart:js';

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/Invoice/Invoice.dart';
import 'package:safalsathi/OrderHistory/Bloc/purchasebloc_bloc.dart';
import 'package:safalsathi/OrderHistory/Model/data.dart';
import 'package:safalsathi/OrderHistory/Model/order_products.dart';
import 'package:safalsathi/common/common_ui.dart';

import '../app_styles.dart';
import 'Bloc/purchasebloc_event.dart';
import 'Bloc/purchasebloc_state.dart';

class OrderHistory extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return OrderHistoryState();
  }
}

class OrderHistoryState extends State<OrderHistory> {
  // ignore: unused_field
  final _scrollController = ScrollController();
  final PurchaseBlocBloc _purchaseblocBloc = PurchaseBlocBloc();

  // ignore: unused_field
  final _scrollThreshold = 200.0;

  OrderHistoryState() {
//    _scrollController.addListener(_onScroll);
    _purchaseblocBloc.add(Fetch());
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Scaffold(
        appBar: AppBar(
          title: Text("Order History",style:TextStyle(color: Colors.white)),
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Colors.red[600],
        ),
        body: BlocListener(
          bloc: _purchaseblocBloc,
          listener: (context, _purchaseBlocState) {
            if (_purchaseBlocState is NotLoggedInState) {
             // notLoggedInDialog(context);
            }
            if (_purchaseBlocState is PostError) {
              buildSnackError(context, _purchaseBlocState.error);
            }
          },
          child: BlocBuilder(
              bloc: _purchaseblocBloc,
              builder:
                  (BuildContext context, PurchaseblocState _purchaseBlocState) {
                print("CURRENT STATE= $_purchaseBlocState");
                if (_purchaseBlocState is PostUninitialized) {
                  return Center(
                    child: Text(" LOADING DATA!!"),
                  );
                }
                if (_purchaseBlocState is NotLoggedInState) {
                  return Container();
                }
                if (_purchaseBlocState is PostError) {
                  return GestureDetector(
                    onTap: () => reload(),
                    child: Center(
                      child: Text("ERROR LOADING DATA!!"),
                    ),
                  );
                }
                if (_purchaseBlocState is IntentState) {
                  print("asghghgh");
                 WidgetsBinding.instance.addPostFrameCallback((_) {
                    print("ghghgh");
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) =>
                            Invoice(id: _purchaseBlocState.id),
                      ),
                    );
                    return new Container(width: 0.0, height: 0.0);
                 });
                }
                if (_purchaseBlocState is PostLoaded) {
                  return Column(
                    children: <Widget>[
                      _purchaseBlocState.purchaseHistory.data != null
                          ? Expanded(
                              child: ListView(
                                children: List.generate(
                                    _purchaseBlocState
                                        .purchaseHistory.data.length, (index) {
                                  return _buildCard(
                                      _purchaseBlocState
                                          .purchaseHistory.data[index],
                                      context,_purchaseBlocState);
                                }),
                              ),
                            )
                          : Container(),
                    ],
                  );
                }
                return new Container(width: 0.0, height: 0.0);
              }),
        ),
      ),
    );
  }

  @override
  void dispose() {
    //_purchaseblocBloc.dispose();
    super.dispose();
  }

  reload() {
    print("reloading...");
    _purchaseblocBloc.add(Fetch());
  }

  Widget _buildCard(Data data, BuildContext context,PurchaseblocState _purchaseblocstate) {
    return InkWell(
      onTap: () =>
         // onTaps(data.id, _purchaseblocBloc,_purchaseblocstate),
      Navigator.push(
        context,
        MaterialPageRoute(
          builder: (context) =>
              Invoice(id: data.id),
        ),
      ),
      child: Padding(
        padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
        child: Card(
          elevation: 0.5,
          margin: EdgeInsets.all(0.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(1.0),
          ),
          child: Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              children: <Widget>[
                ListView(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  children: List.generate(data.orderProducts.length, (indexz) {
                    return _buildCardSecon(data.orderProducts[indexz]);
                  }),
                ),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        children: <Widget>[
                          Row(
                            children: <Widget>[
                              Icon(
                                Icons.calendar_today,
                                size: 10.0,
                              ),
                              Expanded(
                                child: Text(
                                  data.order_date,
                                  style: smallTextStyle,
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                          Container(
                            child: Text(
                              data.order_status,
                              textAlign: TextAlign.left,
                              style: smallTextStyle,
                            ),
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                        child: Center(
                            child: Text(
                      data.getgrandTotal(),
                      style: highligttedTextStyle,
                    ))),
                    Expanded(
                        child: Stack(
                      children: <Widget>[
                        FlatButton(
                          child: Text(
                            "Refund!",
                            style: smallTextStyle,
                          ),
                          shape: Border.fromBorderSide(
                              BorderSide(color: Colors.red.shade100)),
                          onPressed: doSomethng(),
                        ),
                      ],
                    )),
                  ],
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

onTaps(id, PurchaseBlocBloc _purchaseblocBloc,PurchaseblocState _purchaseblocstate) {
  print("vvcv");
  _purchaseblocBloc.add(IntentEvent(id,_purchaseblocstate));
}

historyOnTap(int id, BuildContext context) {}

doNoting() {
  print("Do Nothign");
}

doSomethng() {
  print("DoingSomeThing");
}

Widget _buildCardSecon(OrderProducts orderProduct) {
  return Card(
      elevation: 2.0,
      margin: EdgeInsets.all(0.0),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(1.0),
      ),
      child: Row(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Card(
              elevation: 0.5,
              margin: EdgeInsets.all(1.0),
              shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(1.0),
              ),
              child: Image.network(
                orderProduct.img,
                height: 120,
              )),
          Expanded(
            child: Container(
              height: 120,
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.start,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      orderProduct.name,
                      overflow: TextOverflow.ellipsis,
                      style: seconhighligttedTextStyle,
                      maxLines: 2,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      orderProduct.getprice(),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                      style: TextStyle(color: Colors.green),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      orderProduct.qty.toString(),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ));
}
