import 'package:flutter/material.dart';

class AppColors {
  static const Color GREY = Colors.grey;
  static const Color RED = Colors.red;
  static const Color LIGHT_GREY = Color(0xFFDDDDDD);
  static const Color LIGHT_GREY2 = Color(0xFFEEEEEE);
  static const Color BUTTON_BLUE = Color(0xFF2c25b3);
  static const Color BUTTON_BLUE_SPLASH = Color(0xFFF2DA04);
  static const Color PRIMARY_COLOR_LIGHT = Color(0xFFA5CFF1);
  static const Color PRIMARY_COLOR_DARK = Color(0xFF0D3656);
  static const Color ACCENT_COLOR = Color(0xFFF2DA04);
  static const Color GRADIENT_START = Color(0xFF1a38b0);
  static const Color GRADIENT_END = Color(0xFF963b5c);
  static const Color WHITE = Color(0xFFFFFFFF);
  static const Color BANNER_COLOR = Color(0xFF2c25b3);
  static const Color BLACk = Color(0xFF000000);
  static const Color PRIMARY_BLUE = Color(0xFF0038c3);
  static const Color PRIMARY_RED = Color(0xFFcd3331);
  static const Color ORANGE = Colors.orange;
  static const Color DEEPORANGE = Colors.deepOrange;
  static const Color GREEN = Colors.green;
  static const Color LIGHTGREEN = Colors.lightGreen;

}
