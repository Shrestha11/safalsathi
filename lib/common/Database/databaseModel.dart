import 'package:flutter/foundation.dart';

class Cart {
  @required
  final int cartId;
  @required
  final String detail;
  @required
  final int qty;
  @required
  final String color;
  @required
  final String size;

  Map<String, dynamic> toMapForDb() {
    var map = Map<String, dynamic>();
    map['cartId'] = cartId;
    map['detail'] = detail;
    map['qty'] = qty;
    map['color'] = color;
    map['size'] = size;
    return map;
  }

  Cart(this.cartId, this.detail, this.qty, this.color, this.size);

  Cart.fromDb(Map<String, dynamic> map)
      : cartId = map['cartId'],
        detail = map['detail'],
        qty = map['qty'],
        color = map['color'],
        size = map['size'];

  String getqty() {
    return "x $qty";
  }
}
