import 'dart:io';

import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import 'databaseModel.dart';

class DatabaseHelper {
  static final DatabaseHelper _instance = DatabaseHelper._();
  static Database _database;

  DatabaseHelper._();

  factory DatabaseHelper() {
    return _instance;
  }

  Future<Database> get db async {
    if (_database != null) {
      return _database;
    }

    _database = await init();

    return _database;
  }

  Future<Database> init() async {
    Directory directory = await getApplicationDocumentsDirectory();
    String dbPath = join(directory.path, 'cartdatabase.db');
    var database = openDatabase(dbPath,
        version: 1, onCreate: _onCreate, onUpgrade: _onUpgrade);
    return database;
  }

  void _onCreate(Database db, int version) {
    db.execute('''
    CREATE TABLE $cartTable(
      cartId INTEGER PRIMARY KEY,
      detail TEXT,
      qty INTEGER,
      color STRING,
      size STRING)
  ''');
    print("Database was created!");
  }

  String cartTable = 'cart_table';

  void _onUpgrade(Database db, int oldVersion, int newVersion) {
    // Run migration according database versions
  }

  Future<int> addItem(Cart _cart) async {
    var client = await db;
    return client.insert(cartTable, _cart.toMapForDb(),
        conflictAlgorithm: ConflictAlgorithm.replace
    );
  }

  Future<List<Cart>> fetchAll() async {
    List<Cart> cartList;
    try {
      var client = await db;

      final Future<List<Map<String, dynamic>>> futureMaps =
          client.query(cartTable);
      var maps = await futureMaps;
      if (maps.length == 0) {
        return null;
      }
      cartList = maps.map((item) {
        return Cart.fromDb(item);
      }).toList();
    } catch (error, stacktrace) {
      print("Error:$error -StackTrace:$stacktrace");
    }
    return cartList;
  }

  Future<Cart> fetchItem(int cartId) async {
    var client = await db;
    final Future<List<Map<String, dynamic>>> futureMaps =
        client.query(cartTable, where: 'cartId = ?', whereArgs: [cartId]);
    var maps = await futureMaps;
    if (maps.length != 0) {
      return Cart.fromDb(maps.first);
    }
    return null;
  }

  Future<int> updateItem(Cart newItem) async {
    var client = await db;
    return client.update(cartTable, newItem.toMapForDb(),
        where: 'id = ?',
        whereArgs: [newItem.cartId],
        conflictAlgorithm: ConflictAlgorithm.replace);
  }

  Future<int> removeItem(int id) async {
    var client = await db;
    return client.delete(cartTable, where: 'cartId = ?', whereArgs: [id]);
  }
    Future<int> removeAllItem() async {
    var client = await db;
    return client.delete(cartTable);//, where: 'cartId = ?', whereArgs: [id]);
  }

  String join(String path, String s) {
    return "$path$s";
  }
}
