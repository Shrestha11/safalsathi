class ApiResponse<T> {
  T responseData;
  ApiResultStatus apiCall;
  String message;

  ApiResponse({this.responseData, this.apiCall, this.message});
}

enum ApiResultStatus { SUCCESS, ERROR, NO_NETWORK, STAT_ERR }
