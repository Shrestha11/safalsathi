import 'package:html/parser.dart';
import 'package:safalsathi/common/model/ratings.dart';

import 'imgs.dart';

class Products {
  int id;
  int userId;
  String name;
  String slug;
  int view;
  int main;
  String color;
  int productPrice;
  int salesPrice;
  int super_store_status;
  String quality;
  int negotiable;
  int tax;
  int approved;
  String sku;
  int stockQuantity;
  int stock;
  int prebooking;
  int commission;
  String longDescription;
  String shortDescription;
  int brandId;
  int from;
  int to;
  String status;
  String createdAt;
  String updatedAt;
  int superStoreStatus;
  Ratings ratings;
  List<Imgs> imgs;

  Map<String, dynamic> toMap() {
    var map = Map<String, dynamic>();
    if (id != null) {}
    map['product_id'] = id;
    map['user_id'] = userId;
    map['name'] = name;
    map['slug'] = slug;
    map['view'] = view;
    map['main'] = main;
    map['color'] = color;
    map['product_price'] = productPrice;
    map['sale_price'] = salesPrice;
    map['super_store_status'] = super_store_status;
    map['quality'] = quality;
    map['negotiable'] = negotiable;
    map['tax'] = tax;
    map['approved'] = approved;
    map['sku'] = sku;
    map['stock_quantity'] = stockQuantity;
    map['stock'] = stock;
    map['prebooking'] = prebooking;
    map['commision'] = commission;
    map['long_description'] = longDescription;
    map['short_description'] = shortDescription;
    map['brand_id'] = brandId;
    map['fromm'] = from;
    map['too'] = to;
    map['status'] = status;
    map['created_at'] = createdAt;
    map['update_at'] = updatedAt;
    map['superStoreStatus'] = superStoreStatus;
//    map['ratings_string_value'] = ratingStringValue;
//    map['image_string_value'] = imageStringValue;

    return map;
  }

  Products.withStringValue(
    this.id,
    this.userId,
    this.name,
    this.slug,
    this.view,
    this.main,
    this.color,
    this.productPrice,
    this.salesPrice,
    this.super_store_status,
    this.quality,
    this.negotiable,
    this.tax,
    this.approved,
    this.sku,
    this.stockQuantity,
    this.stock,
    this.prebooking,
    this.commission,
    this.longDescription,
    this.shortDescription,
    this.brandId,
    this.from,
    this.superStoreStatus,
    this.to,
    this.status,
    this.createdAt,
    this.updatedAt,
//      this.ratingStringValue,
//      this.imageStringValue
  );

  Products.fromMap(dynamic obj) {
    this.id = obj["id"];
    this.userId = obj["user_id"];
    this.name = obj["name"];
    this.slug = obj["slug"];
    this.view = obj["view"];
    this.main = obj["main"];
    this.color = obj["color"];
    this.productPrice = obj["product_price"];
    this.salesPrice = obj["sale_price"];
    this.super_store_status = obj["super_store_status"];
    this.quality = obj["quality"];
    this.negotiable = obj["negotiable"];
    this.tax = obj["tax"];
    this.approved = obj["approved"];
    this.sku = obj["sku"];
    this.stockQuantity = obj["stock_quantity"];
    this.stock = obj["stock"];
    this.prebooking = obj["prebooking"];
    this.commission = obj["commission"];
    this.longDescription = obj["long_description"];
    this.shortDescription = obj["short_description"];
    this.brandId = obj["brand_id"];
    this.from = obj["fromm"];
    this.to = obj["too"];
    this.superStoreStatus = obj["superStoreStatus"];
    this.status = obj["status"];
    this.createdAt = obj["created_at"];
    this.updatedAt = obj["update_at"];
//    this.ratingStringValue = obj["ratings_string_value"];
//    this.imageStringValue = obj["image_string_value"];
  }

  Products.fromJsonMap(Map<String, dynamic> map)
      : id = map["id"],
        userId = map["user_id"],
        name = map["name"],
        slug = map["slug"],
        view = map["view"],
        main = map["main"],
        color = map["color"],
        productPrice = map["product_price"],
        salesPrice = map["sale_price"],
        super_store_status = map["super_store_status"],
        quality = map["quality"],
        negotiable = map["negotiable"],
        tax = map["tax"],
        approved = map["approved"],
        sku = map["sku"],
        stockQuantity = map["stock_quantity"],
        stock = map["stock"],
        superStoreStatus = map["superStoreStatus"],
        prebooking = map["prebooking"],
        commission = map["commission"],
        longDescription = map["long_description"],
        shortDescription = map["short_description"],
        brandId = map["brand_id"],
        from = map["from"],
        to = map["to"],
        status = map["status"],
        createdAt = map["created_at"],
        updatedAt = map["updated_at"],
        ratings = Ratings.fromJsonMap(map["ratings"]),
        imgs = List<Imgs>.from(map["imgs"].map((it) => Imgs.fromJsonMap(it)));

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['user_id'] = userId;
    data['name'] = name;
    data['slug'] = slug;
    data['view'] = view;
    data['main'] = main;
    data['color'] = color;
    data['product_price'] = productPrice;
    data['sale_price'] = salesPrice;
    data['super_store_status'] = super_store_status;
    data['quality'] = quality;
    data['negotiable'] = negotiable;
    data['tax'] = tax;
    data['approved'] = approved;
    data['sku'] = sku;
    data['stock_quantity'] = stockQuantity;
    data['stock'] = stock;
    data['superStoreStatus'] = superStoreStatus;
    data['prebooking'] = prebooking;
    data['commission'] = commission;
    data['long_description'] = longDescription;
    data['short_description'] = shortDescription;
    data['brand_id'] = brandId;
    data['from'] = from;
    data['to'] = to;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['ratings'] = ratings == null ? null : ratings.toJson();
    data['imgs'] =
        imgs != null ? this.imgs.map((v) => v.toJson()).toList() : null;
    return data;
  }

  String getsalePrice() {
    return "Rs. $salesPrice";
  }

  String getPrice() {
    return "Rs. $productPrice";
  }

  String getDiscount() {
    return (productPrice - salesPrice).toString();
  }

  String getDiscountPercentage() {
    return (((productPrice - salesPrice) / productPrice) * 100)
        .toStringAsFixed(3);
  }

  String getDescription() {
    if (longDescription == null) return "";
    var document = parse(longDescription);

    String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }

  String getsalePriceQty(int qty2) {
    int qtyPrice = salesPrice * qty2;
    print("QTY2 : $qty2");
    print("Sales Prc :$salesPrice");
    print("Total  $qtyPrice");
    return "Rs. $qtyPrice";
  }
}
