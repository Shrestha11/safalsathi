class Imgs {
  final String relativePath;
  final String url;
  final String smallUrl;
  final String mediumUrl;
  final String largeUrl;
  final String largeSlideshowUrl;

  Imgs.fromJsonMap(Map<String, dynamic> map)
      : relativePath = map["relativePath"],
        url = map["url"],
        smallUrl = map["smallUrl"],
        mediumUrl = map["mediumUrl"],
        largeUrl = map["largeUrl"],
        largeSlideshowUrl = map["largeSlideshowUrl"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['relativePath'] = relativePath;
    data['url'] = url;
    data['smallUrl'] = smallUrl;
    data['mediumUrl'] = mediumUrl;
    data['largeUrl'] = largeUrl;
    data['largeSlideshowUrl'] = largeSlideshowUrl;
    return data;
  }
}
//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> data = new Map<String, dynamic>();
//    data['relativePath'] = relativePath;
//    data['url'] = url;
//    data['smallUrl'] = smallUrl;
//    data['mediumUrl'] = mediumUrl;
//    data['largeUrl'] = largeUrl;
//    data['largeSlideshowUrl'] = largeSlideshowUrl;
//    return data;
//  }

//  @override
//  String toString() {
//    return 'Imgs{relativePath: $relativePath, url: $url, smallUrl: $smallUrl, mediumUrl: $mediumUrl, largeUrl: $largeUrl, largeSlideshowUrl: $largeSlideshowUrl}';
//  }
