import 'package:dio/dio.dart';

dioBASE([String token = ""]) {
  BaseOptions option = new BaseOptions(
    headers: token == null ? null : {"Authorization": token},
    baseUrl: "http://safalsathi.com/api/",
  //  baseUrl: "http://smartbazaar.com.np/api/",
    connectTimeout: 30000,
    receiveTimeout: 30000,
  );
  Dio dio = Dio(option);
  return dio;
}
