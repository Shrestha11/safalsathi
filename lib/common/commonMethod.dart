import 'dart:convert';

//import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:safalsathi/common/model/products.dart';
import 'package:url_launcher/url_launcher.dart';

String validateEmail(String value) {
  String pattern =
      r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
  RegExp regExp = new RegExp(pattern);
  if (value.length == 0) {
    return "Email is Required";
  } else if (!regExp.hasMatch(value)) {
    return "Invalid Email";
  } else {
    return null;
  }
}

String validatePassword(String value) {
  if (value.length < 6) {
    return "Min Length of Password is 8";
  } else {
    return null;
  }
}

String validateRePassword(String value, String password) {
  if (value.length < 6) {
    return "Min Length of Password is 8";
  } else if (value != password) {
    return "Password Doesnot match with Password.";
  } else
    return null;
}

String validateComment(String value) {
  if (value.length < 1) {
    return "";
  } else
    return null;
}

String validatePhone(String value) {
  if (value.length < 10 || value.length > 10) {
    return "Invalid Phone Number!";
  } else
    return null;
}

String validateFirstName(String firstName) {
  if (firstName.length == 0) {
    return "First Name is Required";
  } else
    return null;
}

String validateLastName(String lastName) {
  if (lastName.length == 0) {
    return "Last Name is Required";
  } else
    return null;
}

String validateZone(String zone) {
  if (zone.length == 0) {
    return "Zone is Required";
  } else
    return null;
}

String validateDistrict(String district) {
  if (district.length == 0) {
    return "District is Required";
  } else
    return null;
}

String validateArea(String area) {
  if (area.length == 0) {
    return "Area is Required";
  } else
    return null;
}

String prices(int price) {
  return "NRS. $price";
}

String recentlyViewed(int views) {
  return "$views people just viewed this.";
}

String discount(int price, int salesPrice) {


  return "${(((price - salesPrice) / salesPrice) * 100).truncate()}% off";
}

String stockRemaining(int stock) {
  if (stock == null) {
    return "OUT OF STOCK";
  }
  return "$stock only remaining";
}

Future clearSharedPrefences() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.clear();
  return;
}

int discountPrice(int price, int salesPrice) {
  return ((price - salesPrice) / salesPrice).round();
}

Products decode(String cartDetail) {
  return Products.fromJsonMap(json.decode(cartDetail));
}

launchURL(String url) async {
  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

/*setNotificationOnOff() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  bool isNotificationOn = prefs.get("notification");
  if (isNotificationOn == null) prefs.setBool("notification", false);
  try {
    OneSignal.shared.setSubscription(isNotificationOn);
  } catch (e) {
    print(e);
    throw 'ERROR $e';
  }
}*/

setSharedPrefOnOff(bool value) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.setBool("notification", false);
}

void printWrapped(String text) {
  final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
  pattern.allMatches(text).forEach((match) => print(match.group(0)));
}
