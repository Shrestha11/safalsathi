import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:html/parser.dart';
//import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:safalsathi/Login/LoginPage.dart';
//import 'package:smart_bazaar/category/category_listing.dart';
import 'package:safalsathi/common/color.dart';
import 'package:safalsathi/common/string.dart';
import 'package:safalsathi/home_main/model/categories.dart';
import 'package:safalsathi/search/SearchPage.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter_svg/flutter_svg.dart';

Widget buildLoadingWidget(BuildContext buildContext) {
  return Center(
      child: CircularProgressIndicator(
    valueColor:
        new AlwaysStoppedAnimation<Color>(Theme.of(buildContext).primaryColor),
  ));
}
Widget buildEmptyWidget(BuildContext buildContext) {
  return Container(
    child: Column(
      children: <Widget>[
        Expanded(child: SvgPicture.asset("images/empty.svg")),
        Text("EMPTY"),
        SizedBox(
          height: 50.0,
        )
      ],
    ),
    width: MediaQuery.of(buildContext).size.width,
    height: MediaQuery.of(buildContext).size.height,
  );
}
bool logged = false;

addStringToSF(String accessToken) async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  prefs.setString('token', "Bearer $accessToken");
  logged = true;
}

Future<String> getAuth() async {
  SharedPreferences prefs = await SharedPreferences.getInstance();
  String a = prefs.getString('token') ?? null;
  print("fff $a");
  return (a);
}

Future<void> logOut() async {
  print("lll");
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.clear();
  //OneSignal.shared.removeExternalUserId();
  logged = false;
}

Widget buildErrorWidget(String error) {
  return Center(
    child: Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "Error occured: $error",
        ),
      ],
    ),
  );
}

void buildSnackSuccess(BuildContext context, String msg) {
  Scaffold.of(context).showSnackBar(
    SnackBar(
      backgroundColor: Colors.deepOrange,
      content: Text(msg),
      duration: Duration(seconds: 2),
    ),
  );
}

void loading(BuildContext context, String msg) {
  Scaffold.of(context).showSnackBar(
    SnackBar(
      backgroundColor: Colors.deepOrange,
      content: Text(msg),
      duration: Duration(seconds: 2),
    ),
  );
}

void buildSnackError(BuildContext context, String error) {
  Scaffold.of(context).showSnackBar(
    SnackBar(
      backgroundColor: Colors.red,
      content: Text(error),
    ),
  );
}

openFacebook(String facebookUrl) async {
  if (await canLaunch(facebookUrl)) {
    await launch(facebookUrl);
  } else {
    print("ERROR");
    throw 'Could not launch $facebookUrl';
  }
}

openInstagram(String instagramUrl) async {
  if (await canLaunch(instagramUrl)) {
    await launch(instagramUrl);
  } else {
    print("ERROR");
    throw 'Could not launch $instagramUrl';
  }
}

openTwitter(String twitterLink) async {
  if (await canLaunch(twitterLink)) {
    await launch(twitterLink);
  } else {
    print("ERROR");
    throw 'Could not launch $twitterLink';
  }
}

launchEmail(String email) async {
  var url = "mailto:$email";

  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

launchPhone(String phone) async {
  var url = "tel:$phone";

  if (await canLaunch(url)) {
    await launch(url);
  } else {
    throw 'Could not launch $url';
  }
}

void buildUserNotLoggedIn(BuildContext context, String error) {
  Scaffold.of(context).showSnackBar(
    SnackBar(
      backgroundColor: Colors.red,
      content: Text(error),
      action: SnackBarAction(
          label: "Login",
          textColor: AppColors.WHITE,
          onPressed: () => Navigator.push(
              context,
              MaterialPageRoute(
                maintainState: false,
                builder: (context) => LoginPage(),
              ))),
    ),
  );
}

class WaveClipper1 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0, size.height - 50);

    var firstEndPoint = Offset(size.width * 0.6, size.height - 29 - 50);
    var firstControlPoint = Offset(size.width * .25, size.height - 60 - 50);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondEndPoint = Offset(size.width, size.height - 60);
    var secondControlPoint = Offset(size.width * 0.84, size.height - 50);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class WaveClipper3 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0, size.height);

    var firstEndPoint = Offset(size.width * 0.6, size.height - 15 - 50);
    var firstControlPoint = Offset(size.width * .25, size.height - 60 - 50);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondEndPoint = Offset(size.width, size.height - 40);
    var secondControlPoint = Offset(size.width * 0.84, size.height - 30);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

class WaveClipper2 extends CustomClipper<Path> {
  @override
  Path getClip(Size size) {
    final path = Path();
    path.lineTo(0.0, size.height - 50);

    var firstEndPoint = Offset(size.width * .7, size.height - 40);
    var firstControlPoint = Offset(size.width * .25, size.height);
    path.quadraticBezierTo(firstControlPoint.dx, firstControlPoint.dy,
        firstEndPoint.dx, firstEndPoint.dy);

    var secondEndPoint = Offset(size.width, size.height - 45);
    var secondControlPoint = Offset(size.width * 0.84, size.height - 50);
    path.quadraticBezierTo(secondControlPoint.dx, secondControlPoint.dy,
        secondEndPoint.dx, secondEndPoint.dy);
    path.lineTo(size.width, size.height);
    path.lineTo(size.width, 0);
    path.close();
    return path;
  }

  @override
  bool shouldReclip(CustomClipper<Path> oldClipper) {
    return false;
  }
}

Widget parseHtml(String policies) {
  print(policies);
  var document = parse(policies);
  return Text(
    document.body.text,
    style: TextStyle(fontFamily: 'Lato-Thin', fontWeight: FontWeight.normal),
  );
}

Widget parseHtml2(String policies, BuildContext context) {
  print(policies);
  var document = parse(policies);
  return Text(
    document.body.text,
    style: Theme.of(context)
        .textTheme
        .body1
        .copyWith(fontWeight: FontWeight.normal, color: AppColors.GREY),
  );
}

Widget backGround(BuildContext context) {
  return Container(
   // color: AppColors.LIGHT_GREY2,
    height: MediaQuery.of(context).size.height,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          child: ClipPath(
            clipper: WaveClipper2(),
            child: Container(
              decoration: new BoxDecoration(
                gradient: new LinearGradient(
                  begin: FractionalOffset.topLeft,
                  end: FractionalOffset.bottomRight,
                 // colors: [AppColors.GRADIENT_START, AppColors.GRADIENT_END],
                    colors:[Colors.deepOrange,Colors.orange],
                ),
              ),
            ),
          ),
          height: MediaQuery.of(context).size.width / 2,
        ),

//      Expanded(
//          flex: 8,
//          child: Padding(
//            padding: const EdgeInsets.only(top: 8.0),
//            child: Image.asset(
//              "images/background_paint.png",
//              fit: BoxFit.fitWidth,
//              height: double.infinity,
//              width: double.infinity,
//              alignment: Alignment.center,
//            ),
//          )),
      ],
    ),
  );
}

class CustomSearchBar extends StatelessWidget {
  final List<Categories> categories;

  CustomSearchBar(this.categories);

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(14.0),
      ),
      child: Container(
        child: InkWell(
          onTap: () => Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => SearchPageStart()),
          ),
          child: Row(
            children: <Widget>[
              InkWell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    decoration: BoxDecoration(
                      color: AppColors.GRADIENT_START,
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(8.0),
                        bottomLeft: Radius.circular(8.0),
                      ),
                    ),
                    child: Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(left: 8.0),
                          child: InkWell(
                           /* onTap: () => Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) =>
                                    CategoryListing(categories),
                              ),
                            ),*/
                            child: Icon(
                              Icons.playlist_play,
                              color: AppColors.WHITE,
                            ),
                          ),
                        ),
                        Padding(
                          padding:
                              const EdgeInsets.fromLTRB(4.0, 2.0, 4.0, 2.0),
                          child: RotatedBox(
                              quarterTurns: 3,
                              child: Text(
                                categoriesText,
                                style: Theme.of(context)
                                    .textTheme
                                    .title
                                    .copyWith(
                                        fontSize: 8.0, color: AppColors.WHITE),
                              )),
                        ),
                      ],
                    ),
                  ),
                ),
                /*onTap: () => Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => CategoryListing(categories),
                  ),
                ),*/
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Icon(
                  Icons.search,
                  size: 14.0,
                ),
              ),
              Expanded(
                child: Text(
                  "Search product name,service name or shop name",
                  style: Theme.of(context).textTheme.overline,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class CustomAppBar extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      height: 40.0,
      child: Row(
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(16.0, 8.0, 8.0, 8.0),
            child: Image.asset('images/safal.jpeg'),
          ),
          Expanded(
            child: Container(),
          ),
        ],
      ),
    );
  }
}

class LoadImageAsync extends StatelessWidget {
  final String url;

  LoadImageAsync(this.url);

  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      imageUrl: url,
      imageBuilder: (context, imageProvider) => Container(
        decoration: BoxDecoration(
          image: DecorationImage(
            image: imageProvider,
            fit: BoxFit.cover,
          ),
        ),
      ),
      placeholder: (context, url) => Center(child: CircularProgressIndicator()),
      errorWidget: (context, url, error) => Padding(
        padding: const EdgeInsets.all(8.0),
        child: Icon(Icons.error),
      ),
    );
  }
}
