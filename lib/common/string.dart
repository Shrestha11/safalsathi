const String appName = 'Smart Bazaar';
const String signInWithEmail = 'Sign In With Email';
const String register = "Register";
const String categoriesText = "Categories";

const String labelUserName = "User Name";
const String labelPassword = "Password";
const String labelEmail = "Email";
const String labelConfirmPassword = "Re-Enter Password";
const String labelPhone = "Phone";
const String bargain = "Bargain";
const String detail = "Detail";
const String shortDescription = "Short Description";

const String tempImage =
    'https://images.unsplash.com/photo-1535713875002-d1d0cf377fde?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=800&q=80';
const String tempImage2 =
    'https://upload.wikimedia.org/wikipedia/commons/e/e0/Long_March_2D_launching_VRSS-1.jpg';

const String tempImage3 =
    'https://change-itn.eu/wp-content/uploads/2018/07/long.jpg';
const String facebookImg =
    'https://change-itn.eu/wp-content/uploads/2018/07/long.jpg';

const String urlHome = 'home';
const String urlBargain = 'negotiable/all-negotiable';
const String urlSearchHint = 'search-autocomplete';
const String urlHomeServices = 'services-home';
const String productSectionUrl = 'product-section?page=';
const String searchProductUrl = 'search?query=';
const String wishListUrl = 'my-account/wishlists';
const String userImageUrl = 'user-image';
const String registerAsSeller = 'http://smartbazaar.com.np/become-a-seller';
const String forgotPassword = 'https://www.smartbazaar.com.np/password/reset';
const String endpoint = "my-account/orders";
const String urlPasswordChange = "my-account/change-password";
const String urlAboutUs = "about";
const String urlDefaultAddress = "my-account/default-address";
const String serviceRequest = "service-request";
const String registerUserApi = "register";
const String postReview = "review";
const String urlSocialLogin = "social-register";
const String addBargainString = "negotiable/add-negotiable";
const String getBargainId = "negotiable/view-negotiable?negotiable_id=";

String urlService(int id) {
  return "service/$id";
}

String urlInvoice(int id) {
  return "my-account/order/$id";
}

String urlBargainDelete(int id) {
  return 'negotiable/delete-negotiable?negotiable_id=$id';
}

String urlPushMessage(int negotiableId, String message) {
  return "negotiable/send-message?negotiable_id=$negotiableId&message=$message";
}

String urlProductDetail(int productId) {
  return 'product/$productId';
}

String urlUserReview(int productId) {
  return '/review/$productId';
}

String urlUserFav(int id) {
  return "/product-wishlist/$id";
}

String urlUserFavRemove(int id) {
  return "my-account/wishlist/$id";
}

String urlUserFavAdd() {
  return "my-account/wishlist";
}

double width;
