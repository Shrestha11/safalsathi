import 'package:meta/meta.dart';

@immutable
abstract class ProductDetailEvent {}

class LoadDetailEvent extends ProductDetailEvent {
  final int id;

  LoadDetailEvent(this.id);
}

class AddToFavourite extends ProductDetailEvent {}

class AddToCartClicked extends ProductDetailEvent {}

class ColorUpdate extends ProductDetailEvent {}

class SizeUpdate extends ProductDetailEvent {}

class BuyNowClicked extends ProductDetailEvent {}

class AddtoCartDialogShow extends ProductDetailEvent {}

class BuyProductDialogShow extends ProductDetailEvent {}

class QtySelected extends ProductDetailEvent {
  final int qty;

  QtySelected(this.qty);
}

class AddtoCart extends ProductDetailEvent {}

class BuyProduct extends ProductDetailEvent {}

class FavClicked extends ProductDetailEvent {}

class ReviewDialogShow extends ProductDetailEvent {}

class UserPostReview extends ProductDetailEvent {
  final double ratings;
  final String text;
  final int productId;

  UserPostReview(this.ratings, this.text, this.productId);
}

class BargainAdd extends ProductDetailEvent {}

class RadioButtonSizeUpdate extends ProductDetailEvent {
  final int id;

  RadioButtonSizeUpdate(this.id);
}
