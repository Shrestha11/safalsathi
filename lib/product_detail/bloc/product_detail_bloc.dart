import 'dart:async';
import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/common/Database/DatabaseHelper.dart';
import 'package:safalsathi/common/Database/databaseModel.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/error_handler.dart';
import 'package:safalsathi/common/generic_response.dart';
import 'package:safalsathi/common/model/products.dart';
import 'package:safalsathi/common/string.dart';
import 'package:safalsathi/home_main/model/favResponse/FavResponse.dart';
import 'package:safalsathi/product_detail/Model/ProductDetailResponse.dart';
import 'package:safalsathi/product_detail/Model/Review/getReviewResponse.dart';
import 'package:safalsathi/product_detail/Model/bargain/bargainAddResponse.dart';

import './bloc.dart';

class ProductDetailBloc extends Bloc<ProductDetailEvent, ProductDetailState> {
  static int productId;
  ProductDetailResponse productDetailResponse;
  ProductDetailRepository _productDetailRepository = ProductDetailRepository();
  final DatabaseHelper _databaseHelper = new DatabaseHelper();
  bool isNetworkError = false;
  bool isLoadingError = false;
  int id;
  String selectedColor;
  String selectedSize;
  int qty = 1;
  String encodedDetail;
  bool isFav = false;
  int bargainId;
  GetReviewResponse getReviewResponse;

  void saveEncode(Products products) {
    encodedDetail = json.encode(products);
  }

  @override
  ProductDetailState get initialState => InitialProductDetailState();

  @override
  Stream<ProductDetailState> mapEventToState(
    ProductDetailEvent event,
  ) async* {
    if (event is RadioButtonSizeUpdate) {
      selectedSize = productDetailResponse.data.sizes[event.id];
      yield RadioButtonSizeUpdated(event.id);
    }
    if (event is LoadDetailEvent) {
      productId = event.id;
      yield LoadingDetailState();
      try {
        ApiResponse apiResponseProductDetail =
            await _productDetailRepository.getProductDetails(productId);
        ApiResponse apiResponseUserReview =
            await _productDetailRepository.getUserReviews(productId);
        switch (apiResponseProductDetail.apiCall) {
          case ApiResultStatus.SUCCESS:
            productDetailResponse = ProductDetailResponse.fromJsonMap(
                apiResponseProductDetail.responseData);
            break;
          case ApiResultStatus.ERROR:
            isLoadingError = true;
            break;
          case ApiResultStatus.NO_NETWORK:
            isNetworkError = true;
            break;
          case ApiResultStatus.STAT_ERR:
            isLoadingError = true;
            break;
        }
        switch (apiResponseUserReview.apiCall) {
          case ApiResultStatus.SUCCESS:
            getReviewResponse = GetReviewResponse.fromJsonMap(
                apiResponseUserReview.responseData);
            break;
          case ApiResultStatus.ERROR:
            isLoadingError = true;
            break;
          case ApiResultStatus.NO_NETWORK:
            isNetworkError = true;
            break;
          case ApiResultStatus.STAT_ERR:
            isLoadingError = true;
            break;
        }
      } catch (e) {
        isLoadingError = true;
      }
      if (isLoadingError ||
          productDetailResponse == null ||
          getReviewResponse == null) {
        yield LoadingErrorState("ERROR");
      } else if (isNetworkError) {
        yield NetworkErrorState("NETWORK ERROR");
      } else {
        yield ProductDetailLoadedState(
            productDetailResponse, getReviewResponse);
      }
      try {
        if (await getAuth() != null) {
          ApiResponse favResponse =
              await _productDetailRepository.loadFav(productId);
          if (favResponse.apiCall == ApiResultStatus.NO_NETWORK) {
            yield NetworkErrorState(favResponse.message);
          } else if (favResponse.apiCall == ApiResultStatus.ERROR) {
            yield LoadingErrorState(favResponse.message);
          } else if (favResponse.apiCall == ApiResultStatus.SUCCESS) {
            isFav =
                FavResponse.fromJsonMap(favResponse.responseData).status != 0
                    ? true
                    : false;
            yield FavUpdated(isFav);
          }
        }
      } catch (e) {
        yield LoadingErrorState(e.toString());
      }
    }
    if (event is AddtoCartDialogShow) {
      if ((await getAuth()) == null) {
        yield ShowFailureSnack("User Not Logged IN");
      } else
        yield AddtoCartDialogShowing();
    }
    if (event is BuyProductDialogShow) {
      if ((await getAuth()) == null) {
        yield (UserNotLoggedIn("User Not Logged In"));
      } else {
        yield BuyProductDialogShowing();
      }
      //yield BuyProductDialogShowing();
    }
    if (event is QtySelected) {
      this.qty = event.qty;
      yield QtyUpdated(qty);
    }
    if (event is AddtoCart) {
      Cart _cart =
          new Cart(productId, encodedDetail, qty, selectedColor, selectedSize);
      int id2;

      try {
        id2 = await _databaseHelper.addItem(_cart);
      } catch (e) {
        yield Failure(e.toString());
      }
      if (productId == id2) {
        yield Success("Product is Added");
      }
    }
    if (event is BuyProduct) {
      _databaseHelper.removeAllItem();
      Cart _cart =
          new Cart(productId, encodedDetail, qty, selectedColor, selectedSize);
      int id2;
      try {
        id2 = await _databaseHelper.addItem(_cart);
        print("id2 $id2");
        print("eee $encodedDetail");
      } catch (e) {
        yield Failure(e.toString());
      }
      if (productId == id2) {
        yield Success("Product is Added");
      }
    }
    if (event is ReviewDialogShow) {
      if (await getAuth() == null)
        yield ShowFailureSnack("User Not Logged In");
      else
        yield ReviewDialogShowingState();
    }
    if (event is UserPostReview) {
      try {
        ApiResponse apiResponse = await _productDetailRepository.addReview(
            event.ratings, event.text, event.productId);
        ApiResponse apiResponseUserReview =
            await _productDetailRepository.getUserReviews(productId);
        switch (apiResponseUserReview.apiCall) {
          case ApiResultStatus.SUCCESS:
            getReviewResponse = GetReviewResponse.fromJsonMap(
                apiResponseUserReview.responseData);
            yield ProductDetailLoadedState(
                productDetailResponse, getReviewResponse);
            // TODO: Handle this case.
            break;
          case ApiResultStatus.ERROR:
            // TODO: Handle this case.
            break;
          case ApiResultStatus.NO_NETWORK:
            // TODO: Handle this case.
            break;
          case ApiResultStatus.STAT_ERR:
            // TODO: Handle this case.
            break;
        }
        if (apiResponse.apiCall == ApiResultStatus.NO_NETWORK) {
          yield ShowFailureSnack("NO NETWORK");
        } else if (apiResponse.apiCall == ApiResultStatus.ERROR) {
          yield ShowFailureSnack("ERROR");
        } else if (apiResponse.apiCall == ApiResultStatus.SUCCESS) {
          yield ShowSuccessSnack("Posted");
        }
      } catch (e) {
        yield ShowFailureSnack("ERROR");
      }
    }

    if (event is BargainAdd) {
      if (await getAuth() == null) {
        yield ShowFailureSnack("You Need To Login");
      } else {
        yield ProcessingState();
        print("Negotiable Id $productId");
        try {
          ApiResponse apiResponse =
              await _productDetailRepository.addBargain(productId);
          if (apiResponse.apiCall == ApiResultStatus.SUCCESS) {
            BargainAddResponse _bargainAddResponse =
                BargainAddResponse.fromJsonMap(apiResponse.responseData);
            bargainId = _bargainAddResponse.negotiable_id;
            yield BargainAddedState(bargainId);
          } else if (apiResponse.apiCall == ApiResultStatus.ERROR) {
            yield ShowFailureSnack("ERROR");
          } else if (apiResponse.apiCall == ApiResultStatus.SUCCESS) {
            yield ShowSuccessSnack("Posted");
          }
        } catch (e) {
          yield ShowFailureSnack("ERROR");
        }
      }
    }
    if (event is FavClicked) {
      if (await getAuth() == null)
        yield ShowFailureSnack("User Not Logged In");
      else {
        isFav = !isFav;
        yield FavUpdated(isFav);
        try {
          ApiResponse apiResponse =
              await _productDetailRepository.updateFav(productId, !isFav);
          if (apiResponse.apiCall == ApiResultStatus.NO_NETWORK) {
            isFav = !isFav;
            yield FavUpdated(isFav);
            yield ShowFailureSnack("NO NETWORK");
          } else if (apiResponse.apiCall == ApiResultStatus.ERROR) {
            isFav = !isFav;
            yield FavUpdated(isFav);
            print("API FAILIURE SNACK ${apiResponse.message}");
            yield ShowFailureSnack("ERROR");
          } else if (apiResponse.apiCall == ApiResultStatus.SUCCESS) {
            print("ApiResponse ${apiResponse.responseData.toString()}");
            yield FavUpdated(isFav);
            yield ShowSuccessSnack("Posted");
          }
        } catch (e) {
          isFav = !isFav;
          yield FavUpdated(isFav);
        }
      }
    }
  }
}

class ProductDetailRepository {
  ProductDetailApiProvider _apiProvider = ProductDetailApiProvider();

  Future<ApiResponse> getProductDetails(int productId) {
    return _apiProvider.getDetail(productId);
  }

  Future<ApiResponse> getUserReviews(int productId) {
    return _apiProvider.getUserReview(productId);
  }

  Future<ApiResponse> loadFav(int productId) {
    return _apiProvider.getFavInfo(productId);
  }

  Future<ApiResponse> updateFav(int productId, bool currentValue) {
    return _apiProvider.updateFav(productId, currentValue);
  }

  Future<ApiResponse> addReview(double ratings, String text, int productId) {
    return _apiProvider.addReview(ratings, text, productId);
  }

  Future<ApiResponse> addBargain(int productId) {
    return _apiProvider.addBargain(productId);
  }
}

class ProductDetailApiProvider {
  Future<ApiResponse> getDetail(int productId) async {
    try {
      Response response = await dioBASE().get(urlProductDetail(productId));
      return ApiResponse(
          responseData: response.data,
          apiCall: ApiResultStatus.SUCCESS,
          message: "No Error");
    } on DioError catch (e) {
      print("DIO ERROR $e");
      if (e.type == DioErrorType.DEFAULT)
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.NO_NETWORK,
            message:
                "Make sure wifi or cellular data is turned on, Then try again");
//      else
      return ApiResponse(
          responseData: e,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(e));
    } catch (error) {
      print("DIO Api Error");

      return ApiResponse(
          responseData: error,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(error));
    }
  }

  Future<ApiResponse> getUserReview(int productId) async {
    try {
      Response response = await dioBASE().get(urlUserReview(productId));
      print("Success $response");
      return ApiResponse(
          responseData: response.data,
          apiCall: ApiResultStatus.SUCCESS,
          message: "No Error");
    } on DioError catch (e) {
      print("DIO ERROR $e");
      if (e.type == DioErrorType.DEFAULT)
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.NO_NETWORK,
            message:
                "Make sure wifi or cellular data is turned on, Then try again");
//      else
      return ApiResponse(
          responseData: e,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(e));
    } catch (error) {
      print("DIO Api Error");

      return ApiResponse(
          responseData: error,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(error));
    }
  }

  Future<ApiResponse> getFavInfo(int productId) async {
    String auth = await getAuth();
    if (auth != null) {
      try {
        Response response = await dioBASE(auth).get(urlUserFav(productId));
        print("FAV RESPONSE RAW $response");
        return ApiResponse(
            responseData: response.data,
            apiCall: ApiResultStatus.SUCCESS,
            message: "No Error");
      } on DioError catch (e) {
        if (e.type == DioErrorType.DEFAULT)
          return ApiResponse(
              responseData: e,
              apiCall: ApiResultStatus.NO_NETWORK,
              message:
                  "Make sure wifi or cellular data is turned on, Then try again");
        else
          return ApiResponse(
              responseData: e,
              apiCall: ApiResultStatus.ERROR,
              message: handleError(e));
      } catch (error) {
        print("DIO Api Error");

        return ApiResponse(
            responseData: error,
            apiCall: ApiResultStatus.ERROR,
            message: handleError(error));
      }
    } else
      return ApiResponse(
          responseData: "ERROR",
          apiCall: ApiResultStatus.ERROR,
          message: "User Not Logged In");
  }

  Future<ApiResponse> updateFav(int productId, bool currentValue) async {
    String auth = await getAuth();
    if (auth != null) {
      try {
        if (currentValue == true) {
          Response response =
              await dioBASE(auth).get(urlUserFavRemove(productId));
          return ApiResponse(
              responseData: response.data,
              apiCall: ApiResultStatus.SUCCESS,
              message: "No Error");
        } else {
          Response response = await dioBASE(auth).post(urlUserFavAdd(), data: {
            "product": productId,
          });
          print("RESPONSE $response");
          return ApiResponse(
              responseData: response.data,
              apiCall: ApiResultStatus.SUCCESS,
              message: "No Error");
        }
      } on DioError catch (e) {
        if (e.type == DioErrorType.DEFAULT)
          return ApiResponse(
              responseData: e,
              apiCall: ApiResultStatus.NO_NETWORK,
              message:
                  "Make sure wifi or cellular data is turned on, Then try again");
        else
          return ApiResponse(
              responseData: e,
              apiCall: ApiResultStatus.ERROR,
              message: handleError(e));
      } catch (error) {
        print("DIO Api Error");

        return ApiResponse(
            responseData: error,
            apiCall: ApiResultStatus.ERROR,
            message: handleError(error));
      }
    } else {
      print("NOT LOGGED IN");
      return ApiResponse(
          responseData: "ERROR",
          apiCall: ApiResultStatus.ERROR,
          message: "User Not Logged In");
    }
  }

  Future<ApiResponse> addReview(
      double rating, String text, int productId) async {
    String auth = await getAuth();
    if (auth != null) {
      try {
        Response response = await dioBASE(auth).post(postReview,
            data: {"product_id": productId, "stars": rating, "review": text});
        print(response);
        return ApiResponse(
            responseData: response.data,
            apiCall: ApiResultStatus.SUCCESS,
            message: "No Error");
      } on DioError catch (e) {
        if (e.type == DioErrorType.DEFAULT)
          return ApiResponse(
              responseData: e,
              apiCall: ApiResultStatus.NO_NETWORK,
              message:
                  "Make sure wifi or cellular data is turned on, Then try again");
        else
          return ApiResponse(
              responseData: e,
              apiCall: ApiResultStatus.ERROR,
              message: handleError(e));
      } catch (error) {
        print("DIO Api Error");

        return ApiResponse(
            responseData: error,
            apiCall: ApiResultStatus.ERROR,
            message: handleError(error));
      }
    } else
      return ApiResponse(
          responseData: "ERROR",
          apiCall: ApiResultStatus.ERROR,
          message: "User Not Logged In");
  }

  Future<ApiResponse> addBargain(int productId) async {
    String auth = await getAuth();
    print("PRODUCT ID");
    if (auth != null) {
      print("EVENT IS BARGAIN ADD $auth");
      try {
        Response response = await dioBASE(auth)
            .post(addBargainString, data: {"product_id": productId});
        print("RESPONSE $response");
        return ApiResponse(
            responseData: response.data,
            apiCall: ApiResultStatus.SUCCESS,
            message: "No Error");
      } on DioError catch (e) {
        print("DIO Api Error ${e.response}");

        if (e.type == DioErrorType.DEFAULT)
          return ApiResponse(
              responseData: e,
              apiCall: ApiResultStatus.NO_NETWORK,
              message:
                  "Make sure wifi or cellular data is turned on, Then try again");
        else
          return ApiResponse(
              responseData: e,
              apiCall: ApiResultStatus.ERROR,
              message: handleError(e));
      } catch (error) {
        print("DIO Api Error $error");

        return ApiResponse(
            responseData: error,
            apiCall: ApiResultStatus.ERROR,
            message: handleError(error));
      }
    } else
      return ApiResponse(
          responseData: "ERROR",
          apiCall: ApiResultStatus.ERROR,
          message: "User Not Logged In");
  }
}
