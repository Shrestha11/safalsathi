import 'package:meta/meta.dart';
import 'package:safalsathi/product_detail/Model/ProductDetailResponse.dart';
import 'package:safalsathi/product_detail/Model/Review/getReviewResponse.dart';

@immutable
abstract class ProductDetailState {}

class InitialProductDetailState extends ProductDetailState {}

class LoadingDetailState extends ProductDetailState {}

class LoadingErrorState extends ProductDetailState {
  final String message;

  LoadingErrorState(this.message);
}

class NetworkErrorState extends ProductDetailState {
  final String message;

  NetworkErrorState(this.message);
}

class ProductDetailLoadedState extends ProductDetailState {
  final ProductDetailResponse productDetailResponse;
  final GetReviewResponse getReviewResponse;

  ProductDetailLoadedState(this.productDetailResponse, this.getReviewResponse);
}

class FavouriteSelectedState extends ProductDetailState {}

class FavouriteDeselectedState extends ProductDetailState {}

class AddtoCartDialogShowing extends ProductDetailState {}

class BuyProductDialogShowing extends ProductDetailState {}

class QtyUpdated extends ProductDetailState {
  final int qty;

  QtyUpdated(this.qty);
}

class Success extends ProductDetailState {
  final String msg;

  Success(this.msg);

  @override
  String toString() => 'Success';
}

class Failure extends ProductDetailState {
  final String msg;

  Failure(this.msg);

  @override
  String toString() => 'Failure';
}

class UserNotLoggedIn extends ProductDetailState {
  final String msg;

  UserNotLoggedIn(this.msg);
}

class FavUpdated extends ProductDetailState {
  final bool isFav;

  FavUpdated(this.isFav);
}

class ReviewDialogShowingState extends ProductDetailState {}

class ShowSuccessSnack extends ProductDetailState {
  final String msg;

  ShowSuccessSnack(this.msg);
}

class ShowFailureSnack extends ProductDetailState {
  final String msg;

  ShowFailureSnack(this.msg);
}

class ProcessingState extends ProductDetailState {}

class BargainAddedState extends ProductDetailState {
  final int negotiableId;

  BargainAddedState(this.negotiableId);
}

class RadioButtonSizeUpdated extends ProductDetailState {
  final int id;

  RadioButtonSizeUpdated(this.id);
}
