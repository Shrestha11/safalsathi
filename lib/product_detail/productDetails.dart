import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:safalsathi/bargain/bargainDetail.dart';
import 'package:safalsathi/common/color.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/model/products.dart';
import 'package:safalsathi/home_main/home_main.dart';
import 'package:safalsathi/product_detail/Model/ProductDetailResponse.dart';
import 'package:safalsathi/product_detail/bloc/bloc.dart';
import 'package:safalsathi/product_detail/bloc/product_detail_bloc.dart';
import 'package:safalsathi/product_detail/productDetail.dart';
import 'package:safalsathi/product_detail/productDetailViews.dart';

import 'Model/Review/getReviewResponse.dart';
import 'bloc/product_detail_event.dart';

class LoadRemainingViews extends StatefulWidget {
  final ProductDetailBloc productDetailBloc;
  final Products products;
  final int id;

  LoadRemainingViews(this.id, this.productDetailBloc, this.products);

  @override
  State<StatefulWidget> createState() {
    return LoadRemainingViewsState(id, productDetailBloc, products);
  }
}

class LoadRemainingViewsState extends State<LoadRemainingViews> {
  final ProductDetailBloc productDetailBloc;
  final Products products;
  final int id;
  ProductDetailResponse _productDetailResponse;
  GetReviewResponse _getReviewResponse;
  int negotiableId;

  @override
  void initState() {
    super.initState();
    widget.productDetailBloc.add(LoadDetailEvent(id));
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.light),
    );
  }

  @override
  void dispose() {
    super.dispose();
    SystemChrome.setSystemUIOverlayStyle(
      SystemUiOverlayStyle(statusBarIconBrightness: Brightness.dark),
    );
    // send data to the first page
  }

  LoadRemainingViewsState(this.id, this.productDetailBloc, this.products) {
    // _productId = products.id;
    productDetailBloc.saveEncode(products);
    //for (int i = 0; i < products.imgs.length; i++) {
    //imgList.add(products.imgs[i].mediumUrl);
    //}
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: widget.productDetailBloc,
      listener: (context, state) {
        if (state is ProductDetailLoadedState) {
          _productDetailResponse = state.productDetailResponse;
          _getReviewResponse = state.getReviewResponse;
        }
        if (state is BargainAddedState) {
         /* negotiableId = state.negotiableId;
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => BargainDetail(negotiableId),
            ),
          );*/
        }
        if (state is ProcessingState) {
          buildSnackSuccess(context, "Processing");
        }
        if (state is ShowFailureSnack) {
          buildSnackError(context, "User Not Logged In");
        }
      },
      child: BlocBuilder(
        bloc: widget.productDetailBloc,
        builder: (context, state) {
          if (state is LoadingDetailState) {
            return Center(child: CircularProgressIndicator());
          }
          if (state is NetworkErrorState) {
            return Center(child: CircularProgressIndicator());
          }
          if (state is LoadingErrorState) {
            return Center(child: CircularProgressIndicator());
          }

          return AnnotatedRegion<SystemUiOverlayStyle>(
            value: SystemUiOverlayStyle(
              statusBarIconBrightness: Brightness.light,
              statusBarBrightness: Brightness.light,
            ),
            child: Column(
              children: <Widget>[
                _productDetailResponse == null
                    ? Container()
                    : _buildHomeDataWidget(_productDetailResponse, context,
                        widget.productDetailBloc),
                _getReviewResponse == null
                    ? Container()
                    : loadReviews(
                        _getReviewResponse,
                        context,
                        productDetailBloc,
                      ),
                SimilarProduct(_productDetailResponse.data.similarProducts),
              ],
            ),
          );
        },
      ),
    );
  }
}

Widget _colorVariant(List<Products> relatedProducts, int id,
    ProductDetailBloc _productDetailBloc) {
  return Card(
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            "Color",
          ),
          Container(
            height: 75,
            child: ListView.builder(
              shrinkWrap: true,
              scrollDirection: Axis.horizontal,
              itemCount: relatedProducts.length,
              itemBuilder: (context, position) {
                return InkWell(
                  onTap: () => relatedProducts[position].id == id
                      ? null
                      : Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => ProductDetail(
                                products: relatedProducts[position]),
                          ),
                        ),
                  child: Card(
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Image.network(
                        relatedProducts[position].imgs[0].smallUrl,
                        width: 50,
                        height: 50,
                      ),
                    ),
//                    shape: id == relatedProducts[position].id
//                        ? Border.fromBorderSide(
//                            BorderSide(color: Colors.black, width: 5.0))
//                        : Border.fromBorderSide(BorderSide(color: Colors.grey)),
                  ),
                );
              },
            ),
          ),
        ],
      ),
    ),
  );
}

Widget _buildHomeDataWidget(ProductDetailResponse productDetailResponse,
    BuildContext context, ProductDetailBloc productDetailBloc) {
  return ListView(
    shrinkWrap: true,
    physics: const NeverScrollableScrollPhysics(),
    children: <Widget>[
      ProductDetailSpecification(productDetailResponse.data.specifications),
      ProductDetailFeatures(productDetailResponse.data.features),

//      ProductDetailSize(productDetailResponse.data.sizes),
      productDetailResponse.data.sizes.length >= 1
          ? _sizeVariant(productDetailResponse.data.sizes, productDetailBloc)
          : Container(),
      //color variant

      ProductDetailColorVariant(productDetailResponse.data.relatedProducts,
          productDetailResponse.data.id),
//
      productDetailResponse.data.relatedProducts.length <= 1
          ? Container()
          : _colorVariant(productDetailResponse.data.relatedProducts,
              productDetailResponse.data.id, productDetailBloc),
    ],
  );
}

class SimilarProduct extends StatelessWidget {
  final List<Products> similarProducts;

  SimilarProduct(this.similarProducts);

  @override
  Widget build(BuildContext context) {
    if (similarProducts.length == 0) {
      return Container();
    } else if (similarProducts == null) {
      return Container();
    } else
      return Container(
        child: ListView(
          physics: NeverScrollableScrollPhysics(),
          shrinkWrap: true,
          children: <Widget>[
            Text(
              "Similar Products",
              style:
                  Theme.of(context).textTheme.display1.copyWith(fontSize: 25.0),
            ),
            GridView.builder(
              physics: const NeverScrollableScrollPhysics(),
              gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                crossAxisCount: 2,
                childAspectRatio: 0.72,
              ),
              semanticChildCount: 2,
              scrollDirection: Axis.vertical,
              shrinkWrap: true,
              itemCount: similarProducts.length,
              itemBuilder: (context, position) {
                return ProductItemDisplay(similarProducts[position]);
              },
            ),
          ],
        ),
      );
  }
}

class ProductDetailColorVariant extends StatelessWidget {
  final List<Products> relatedProducts;
  final int id;

  ProductDetailColorVariant(this.relatedProducts, this.id);

  @override
  Widget build(BuildContext context) {
    return relatedProducts.length == 0
        ? Container()
        : Padding(
            padding: const EdgeInsets.all(8.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  "Color",
//            style: detailPagetextStyle,
                ),
                Container(
                  height: 75,
                  child: ListView.builder(
                    shrinkWrap: true,
                    scrollDirection: Axis.horizontal,
                    itemCount: relatedProducts.length,
                    itemBuilder: (context, position) {
                      return InkWell(
                        onTap: () => relatedProducts[position].id == id
                            ? null
                            : Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ProductDetail(
                                      products: relatedProducts[position]),
                                ),
                              ),
                        child: Card(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Image.network(
                              relatedProducts[position].imgs[0].smallUrl,
                              width: 50,
                            ),
                          ),
                          shape: relatedProducts[position].id == id
                              ? Border.fromBorderSide(
                                  BorderSide(color: Colors.grey))
                              : null,
                        ),
                      );
                    },
                  ),
                ),
              ],
            ),
          );
  }
}

class ProductDetailSize extends StatelessWidget {
  final List<String> sizes;

  ProductDetailSize(this.sizes);

  @override
  Widget build(BuildContext context) {
    return sizes == null
        ? Container()
        : Padding(
            padding: const EdgeInsets.all(8.0),
            child: Text("Selected Size ${sizes[0]}"),
          );
  }
}

Widget _sizeVariant(List<String> sizes, ProductDetailBloc _productDetailBloc) {
  int _selectedSizeVariant = 0;

  return Card(
    child: Padding(
      padding: const EdgeInsets.all(8.0),
      child: BlocBuilder(
        bloc: _productDetailBloc,
        builder: (context, state) {
          if (state is RadioButtonSizeUpdated) {
            _selectedSizeVariant = state.id;
          }
          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                "Size",
//            style: detailPagetextStyle,
              ),
              Container(
                height: 25.0,
                child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: sizes.length,
                    scrollDirection: Axis.horizontal,
                    itemBuilder: (context, position) {
                      return Row(children: <Widget>[
                        Radio(
                          value: position,
                          groupValue: _selectedSizeVariant,
                          onChanged: (e) {
                            _productDetailBloc.add(RadioButtonSizeUpdate(e));
                          },
                          activeColor: Colors.deepPurple,
                        ),
                        Text(sizes[position]),
                      ]);
                    }),
              ),
            ],
          );
        },
      ),
    ),
  );
}

Widget loadReviews(GetReviewResponse response, BuildContext context,
    ProductDetailBloc productDetailBloc) {
  return Column(
    crossAxisAlignment: CrossAxisAlignment.start,
    mainAxisAlignment: MainAxisAlignment.start,
    children: <Widget>[
      Row(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: <Widget>[
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text(
                "Rating and Reviews",
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Text("${response.ratings.average} star ratings"),
            ),
          ],
        ),
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: RaisedButton(
            padding: const EdgeInsets.all(8.0),
            textColor: Colors.white,
            color: AppColors.PRIMARY_RED,
            child: new Text(
              "Rate Product",
            ),
            onPressed: () => productDetailBloc.add(ReviewDialogShow()),
          ),
        )
      ]),
      Container(
        color: Colors.black45,
        height: 1,
      ),
      ListView.builder(
        scrollDirection: Axis.vertical,
        shrinkWrap: true,
        physics: const NeverScrollableScrollPhysics(),
        itemCount: response.reviews.length > 5 ? 5 : response.reviews.length,
        itemBuilder: (context, position) {
          return Column(
            children: <Widget>[
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Image.network(
                      response.reviews[position].image,
                      height: 50,
                      width: 50,
                    ),
                  ),
                  Expanded(
                    child: Text(
                      response.reviews[position].fullName,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.grade,
                          color: Colors.yellow.shade800,
                          size: 20.0,
                        ),
                        Text(response.reviews[position].stars.toString()),
                      ],
                    ),
                  )
                ],
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      response.reviews[position].review,
                      maxLines: 5,
//                            style: AppColors.GREY,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      response.reviews[position].date,
//                            style: smallTextStyle,
                    ),
                  ),
                ],
              ),
              position == (response.reviews.length - 1)
                  ? Container()
                  : Container(
                      color: Colors.black45,
                      height: 1,
                    ),
            ],
          );
        },
      ),
    ],
  );
}
