
import 'package:safalsathi/common/model/products.dart';
import 'package:safalsathi/product_detail/Model/specifications.dart';

import 'features.dart';
import 'images.dart';

class Data {
  final int id;
  final String name;
  final String shortDescription;
  final String longDescription;
  final int productPrice;
  final int salesPrice;
  final int stock;
  final int stockQuantity;
  final int from;
  final int to;
  final int tax;
  final int preBooking;
  final List<Images> images;
  final List<String> sizes;
  final String shopName;
  final List<Specifications> specifications;
  final List<Features> features;
  final List<Products> similarProducts;
  final int wishList;
  final List<Products> relatedProducts;

  Data.fromJsonMap(Map<String, dynamic> map)
      : id = map["id"],
        name = map["name"],
        shortDescription = map["short_description"],
        longDescription = map["long_description"],
        productPrice = map["product_price"],
        salesPrice = map["sale_price"],
        stock = map["stock"],
        stockQuantity = map["stock_quantity"],
        from = map["from"],
        to = map["to"],
        tax = map["tax"],
        preBooking = map["prebooking"],
        images = List<Images>.from(
            map["images"].map((it) => Images.fromJsonMap(it))),
        sizes = List<String>.from(map["sizes"]),
        shopName = map["shop_name"],
        specifications = List<Specifications>.from(
            map["specifications"].map((it) => Specifications.fromJsonMap(it))),
        features = List<Features>.from(
            map["features"].map((it) => Features.fromJsonMap(it))),
        similarProducts = List<Products>.from(
            map["similarProducts"].map((it) => Products.fromJsonMap(it))),
        wishList = map["wishlist"],
        relatedProducts = List<Products>.from(
            map["related_products"].map((it) => Products.fromJsonMap(it)));

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['name'] = name;
    data['short_description'] = shortDescription;
    data['long_description'] = longDescription;
    data['product_price'] = productPrice;
    data['sale_price'] = salesPrice;
    data['stock'] = stock;
    data['stock_quantity'] = stockQuantity;
    data['from'] = from;
    data['to'] = to;
    data['tax'] = tax;
    data['prebooking'] = preBooking;
    data['images'] =
        images != null ? this.images.map((v) => v.toJson()).toList() : null;
    data['sizes'] = sizes;
    data['shop_name'] = shopName;
    data['specifications'] = specifications != null
        ? this.specifications.map((v) => v.toJson()).toList()
        : null;
    data['features'] =
        features != null ? this.features.map((v) => v.toJson()).toList() : null;
    data['similarProducts'] = similarProducts != null
        ? this.similarProducts.map((v) => v.toJson()).toList()
        : null;
    data['wishlist'] = wishList;
    data['related_products'] = relatedProducts != null
        ? this.relatedProducts.map((v) => v.toJson()).toList()
        : null;
    return data;
  }
}
