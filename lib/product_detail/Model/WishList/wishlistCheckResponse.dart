
class WishListCheckResponse {

  final int status;

	WishListCheckResponse.fromJsonMap(Map<String, dynamic> map): 
		status = map["status"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		return data;
	}
}
