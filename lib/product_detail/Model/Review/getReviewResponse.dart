import 'package:safalsathi/common/model/ratings.dart';
import 'package:safalsathi/product_detail/Model/Review/reviews.dart';

class GetReviewResponse {
  final List<Reviews> reviews;
  final Ratings ratings;

  GetReviewResponse.fromJsonMap(Map<String, dynamic> map)
      : reviews = List<Reviews>.from(
            map["reviews"].map((it) => Reviews.fromJsonMap(it))),
        ratings = Ratings.fromJsonMap(map["ratings"]);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['reviews'] =
        reviews != null ? this.reviews.map((v) => v.toJson()).toList() : null;
    data['ratings'] = ratings == null ? null : ratings.toJson();
    return data;
  }
}
