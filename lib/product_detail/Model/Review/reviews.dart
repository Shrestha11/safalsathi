
class Reviews {

  final int id;
  final int userId;
  final int productId;
  final int stars;
  final int status;
  final String review;
  final String createdAt;
  final String updatedAt;
  final int ownerId;
  final String image;
  final String fullName;
  final String date;

	Reviews.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		userId = map["user_id"],
		productId = map["product_id"],
		stars = map["stars"],
		status = map["status"],
		review = map["review"],
		createdAt = map["created_at"],
		updatedAt = map["updated_at"],
		ownerId = map["owner_id"],
		image = map["image"],
		fullName = map["fullname"],
		date = map["date"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['user_id'] = userId;
		data['product_id'] = productId;
		data['stars'] = stars;
		data['status'] = status;
		data['review'] = review;
		data['created_at'] = createdAt;
		data['updated_at'] = updatedAt;
		data['owner_id'] = ownerId;
		data['image'] = image;
		data['fullname'] = fullName;
		data['date'] = date;
		return data;
	}
}
