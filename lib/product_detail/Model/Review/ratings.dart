class Ratings {
  var average;
  final int one;
  final int two;
  final int three;
  final int four;
  final int five;
  final int total;

  Ratings.fromJsonMap(Map<String, dynamic> map)
      : average = map["average"],
        one = map["one"],
        two = map["two"],
        three = map["three"],
        four = map["four"],
        five = map["five"],
        total = map["total"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['average'] = average;
    data['one'] = one;
    data['two'] = two;
    data['three'] = three;
    data['four'] = four;
    data['five'] = five;
    data['total'] = total;
    return data;
  }
}
