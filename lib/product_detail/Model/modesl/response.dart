
class Response {

  final List<String> list;

	Response.fromJsonMap(Map<String, dynamic> map): 
		list = List<String>.from(map["list"]);


	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['list'] = list;
		return data;
	}
}
