
class BargainAddResponse {

  final String status;
  final String message;
  final int negotiable_id;

	BargainAddResponse.fromJsonMap(Map<String, dynamic> map): 
		status = map["status"],
		message = map["message"],
		negotiable_id = map["negotiable_id"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['status'] = status;
		data['message'] = message;
		data['negotiable_id'] = negotiable_id;
		return data;
	}
}
