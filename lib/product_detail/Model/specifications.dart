
class Specifications {

  final int id;
  final int productId;
  final String title;
  final String description;
  final String createdAt;
  final String updatedAt;

	Specifications.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		productId = map["product_id"],
		title = map["title"],
		description = map["description"],
		createdAt = map["created_at"],
		updatedAt = map["updated_at"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['product_id'] = productId;
		data['title'] = title;
		data['description'] = description;
		data['created_at'] = createdAt;
		data['updated_at'] = updatedAt;
		return data;
	}
}
