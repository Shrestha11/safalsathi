
class RelatedProducts {

  final int id;
  final String color;
  final String image;

	RelatedProducts.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		color = map["color"],
		image = map["image"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['color'] = color;
		data['image'] = image;
		return data;
	}
}
