import 'package:html/parser.dart';

class SimilarProducts {
  final int id;
  final int userId;
  final String name;
  final String slug;
  final int view;
  final int main;
  final String color;
  final int productPrice;
  final int salesPrice;
  final String quality;
  final int negotiable;
  final Object tax;
  final int approved;
  final String sku;
  final int stockQuantity;
  final int stock;
  final int preBooking;
  final Object commission;
  final String longDescription;
  final Object shortDescription;
  final int brandId;
  final int from;
  final int to;
  final String status;
  final String createdAt;
  final String updatedAt;
  final String img;

  SimilarProducts.fromJsonMap(Map<String, dynamic> map)
      : id = map["id"],
        userId = map["user_id"],
        name = map["name"],
        slug = map["slug"],
        view = map["view"],
        main = map["main"],
        color = map["color"],
        productPrice = map["product_price"],
        salesPrice = map["sale_price"],
        quality = map["quality"],
        negotiable = map["negotiable"],
        tax = map["tax"],
        approved = map["approved"],
        sku = map["sku"],
        stockQuantity = map["stock_quantity"],
        stock = map["stock"],
        preBooking = map["prebooking"],
        commission = map["commission"],
        longDescription = map["long_description"],
        shortDescription = map["short_description"],
        brandId = map["brand_id"],
        from = map["from"],
        to = map["to"],
        status = map["status"],
        createdAt = map["created_at"],
        updatedAt = map["updated_at"],
        img = map["img"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = id;
    data['user_id'] = userId;
    data['name'] = name;
    data['slug'] = slug;
    data['view'] = view;
    data['main'] = main;
    data['color'] = color;
    data['product_price'] = productPrice;
    data['sale_price'] = salesPrice;
    data['quality'] = quality;
    data['negotiable'] = negotiable;
    data['tax'] = tax;
    data['approved'] = approved;
    data['sku'] = sku;
    data['stock_quantity'] = stockQuantity;
    data['stock'] = stock;
    data['prebooking'] = preBooking;
    data['commission'] = commission;
    data['long_description'] = longDescription;
    data['short_description'] = shortDescription;
    data['brand_id'] = brandId;
    data['from'] = from;
    data['to'] = to;
    data['status'] = status;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['img'] = img;
    return data;
  }

  String getsalePrice() {
    return "Rs. $salesPrice";
  }

  String getPrice() {
    return "Rs. $productPrice";
  }

  String getDiscount() {
    return (productPrice - salesPrice).toString();
  }

  String getDescription() {
    var document = parse(longDescription);

    String parsedString = parse(document.body.text).documentElement.text;

    return parsedString;
  }
}
