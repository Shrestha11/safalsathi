
class Features {

  final int id;
  final int productId;
  final String feature;
  final String createdAt;
  final String updatedAt;

	Features.fromJsonMap(Map<String, dynamic> map): 
		id = map["id"],
		productId = map["product_id"],
		feature = map["feature"],
		createdAt = map["created_at"],
		updatedAt = map["updated_at"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['id'] = id;
		data['product_id'] = productId;
		data['feature'] = feature;
		data['created_at'] = createdAt;
		data['updated_at'] = updatedAt;
		return data;
	}
}
