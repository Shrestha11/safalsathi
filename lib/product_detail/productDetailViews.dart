import 'package:flutter/material.dart';
import 'package:safalsathi/product_detail/Model/features.dart';
import 'package:safalsathi/product_detail/Model/specifications.dart';

class ProductDetailFeatures extends StatelessWidget {
  final List<Features> featureList;

  ProductDetailFeatures(this.featureList);

  @override
  Widget build(BuildContext context) {
    return featureList.length == 0
        ? Container()
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Features",
//            style: detailPagetextStyle,
                ),
              ),
              ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: featureList.length,
                itemBuilder: (context, position) {
                  return Row(
                    children: <Widget>[
                      Expanded(
                        child: Padding(
                          padding: const EdgeInsets.fromLTRB(8.0, 2.0, 2.0, 2.0),
                          child: Text(
                            featureList[position].feature,
                            style: TextStyle(
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                            ),
                          ),
                        ),
                      ),
                    ],
                  );
                },
              )
            ],
          );
  }
}

class ProductDetailSpecification extends StatelessWidget {
  final List<Specifications> specifications;

  ProductDetailSpecification(this.specifications);

  @override
  Widget build(BuildContext context) {
    return specifications.length == 0
        ? Container()
        : Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text(
                  "Specification",
                ),
              ),
              ListView.builder(
                physics: const NeverScrollableScrollPhysics(),
                shrinkWrap: true,
                itemCount: specifications.length,
                itemBuilder: (context, position) {
                  return Row(
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8.0, 2.0, 2.0, 2.0),
                        child: Text(
                          specifications[position].title + ":",
                          style: TextStyle(
                            color: Colors.black,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.fromLTRB(8.0, 2.0, 2.0, 2.0),
                        child: Text(specifications[position].description),
                      ),
                    ],
                  );
                },
              )
            ],
          );
  }
}
