import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:numberpicker/numberpicker.dart';
import 'package:rating_bar/rating_bar.dart';
import 'package:safalsathi/checkOut/CheckOut.dart';
import 'package:safalsathi/common/color.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/model/products.dart';
import 'package:safalsathi/common/string.dart';
import 'package:safalsathi/home_main/home_main_widget.dart';
import 'package:safalsathi/product_detail/bloc/bloc.dart';
import 'package:safalsathi/product_detail/productDetails.dart';
import 'package:safalsathi/vendor_profile/vendor_profile.dart';

class ProductDetail extends StatelessWidget {
  final Products products;
  final ProductDetailBloc productDetailBloc = ProductDetailBloc();

  void dispose() {
    productDetailBloc.close();
  }

  ProductDetail({Key key, @required this.products}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Column(
        children: <Widget>[
          Expanded(
            child: ListView(
              padding: const EdgeInsets.all(0.0),
              children: <Widget>[
                detailCarousel(products.imgs, context),
                Row(
                  children: <Widget>[
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          prices(products.salesPrice),
                          style: Theme.of(context).textTheme.display1,
                        ),
                      ),
                    ),
                    Fav(
                      productDetailBloc,
                    ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        prices(products.productPrice),
                        style: Theme.of(context).textTheme.body2.copyWith(
                            decoration: TextDecoration.lineThrough,
                            color: Colors.grey),
                      ),
                    ),
                    products.productPrice == products.salesPrice
                        ? Container()
                        : Container(
                            decoration: BoxDecoration(
                              color: Colors.green,
                              borderRadius: BorderRadius.circular(10),
                              boxShadow: [
                                BoxShadow(color: Colors.black12, blurRadius: 5)
                              ],
                            ),
//                color: AppColors.GREY,
                            child: Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text(
                                discount(
                                    products.productPrice, products.salesPrice),
                                style: Theme.of(context)
                                    .textTheme
                                    .body2
                                    .copyWith(
                                        decoration: TextDecoration.lineThrough,
                                        color: Colors.white),
                              ),
                            ),
                          ),
                  ],
                ),
                Row(
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text(
                        stockRemaining(products.stockQuantity),
                        style: Theme.of(context)
                            .textTheme
                            .body2
                            .copyWith(color: AppColors.GREY),
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Text(
                    products.name,
                    style: Theme.of(context).textTheme.display1.copyWith(
                        color: AppColors.BLACk, fontWeight: FontWeight.w800),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Container(
                    height: 1.0,
                    color: AppColors.GREY,
                  ),
                ),
                products.shortDescription == null
                    ? Container()
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Text(
                          shortDescription,
                          style: Theme.of(context).textTheme.body1,
                        ),
                      ),
                products.shortDescription == null
                    ? Container()
                    : Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: parseHtml2(products.shortDescription, context),
                      ),
                LoadRemainingViews(products.id, productDetailBloc, products),
              ],
            ),
          ),
          AddToCartButton(products.id, productDetailBloc, products),
        ],
      ),
    );
  }
}

class Fav extends StatefulWidget {
  final ProductDetailBloc productDetailBloc;

  Fav(this.productDetailBloc);

  @override
  State<StatefulWidget> createState() {
    return FavState();
  }
}

class FavState extends State<Fav> {
  bool isFav = false;

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: widget.productDetailBloc,
      listener: (context, state) {
        if (state is FavUpdated) {
          isFav = state.isFav;
          print("IS FAV UPDATED $isFav");
        }
      },
      child: BlocBuilder(
        bloc: widget.productDetailBloc,
        builder: (context, state) {
          return InkWell(
            onTap: () => widget.productDetailBloc.add(FavClicked()),
            child: Padding(
              padding: const EdgeInsets.all(8.0),
              child: Icon(
                isFav ? Icons.favorite : Icons.favorite_border,
                color: isFav ? AppColors.PRIMARY_RED : AppColors.GREY,
              ),
            ),
          );
        },
      ),
    );
  }
}

class AddToCartButton extends StatefulWidget {
  final int id;
  final ProductDetailBloc productDetailBloc;
  final Products products;

  AddToCartButton(this.id, this.productDetailBloc, this.products);

  @override
  State<StatefulWidget> createState() {
    return AddtoCartButtonView();
  }
}

class AddtoCartButtonView extends State<AddToCartButton> {
  String shopName;

  void _showRatingDialog(BuildContext context) {
    final _passwordController = TextEditingController();
    double ratings;
    // flutter defined function
    showDialog(
      context: context,
      builder: (BuildContext context) {
        // return object of type Dialog
        return SimpleDialog(
          title: Center(
            child: new Text(
              "Review this product...",
            ),
          ),
          children: <Widget>[
            Container(
              child: Column(
                children: <Widget>[
                  RatingBar(
                    onRatingChanged: (rating) {
                      ratings = rating;
                    },
                    filledIcon: Icons.star,
                    emptyIcon: Icons.star_border,
                    filledColor: AppColors.PRIMARY_RED,
                    emptyColor: Colors.black,
                    size: 32,
                  ),
                  new TextFormField(
                      keyboardType: TextInputType.multiline,
                      controller: _passwordController,
                      maxLines: 10), //Number_of_lines(int),)),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new SimpleDialogOption(
                      child: Text(
                        "Submit",
//                        style: seconhighligttedTextStyle,
                      ),
                      onPressed: () {
                        widget.productDetailBloc.add(UserPostReview(ratings,
                            _passwordController.text, widget.products.id));
                        return Navigator.of(context).pop();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ],
        );
      },
    );
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: widget.productDetailBloc,
      listener: (context, state) {
        if (state is AddtoCartDialogShowing) {
          widget.products.stockQuantity == null
              ? buildSnackSuccess(context, "Out of Stock")
              : _showAddtoCartDialog(context, widget.productDetailBloc, state,
                  widget.products.stockQuantity);
        }
        if (state is UserNotLoggedIn) {
          print("UserNotLoggedIn");
          buildSnackError(context, state.msg);
        }
        if (state is BuyProductDialogShowing) {
          _showAddtoCartDialog(context, widget.productDetailBloc, state,
              widget.products.stockQuantity);
        }
        if (state is Success) {
          buildSnackSuccess(context, state.msg);
        }
        if (state is ReviewDialogShowingState) {
          _showRatingDialog(context);
        }
        if (state is ProductDetailLoadedState) {
          shopName = state.productDetailResponse.data.shopName;
        }
      },
      child: Center(
        child: ButtonTheme(
          minWidth: MediaQuery.of(context).size.width,
          buttonColor: AppColors.PRIMARY_RED,
          height: 50,
          child: Row(
            children: <Widget>[
              widget.products.negotiable == 0
                  ? Container()
                  : /*InkWell(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          children: <Widget>[
                            Icon(
                              Icons.chat,
                              color: AppColors.GREY,
                            ),
                            Text(
                              "Bargain",
                              style: Theme.of(context)
                                  .textTheme
                                  .display1
                                  .copyWith(
                                      fontSize: 10.0, color: AppColors.BLACk),
                            )
                          ],
                        ),
                      ),
                      onTap: () => widget.productDetailBloc.add(BargainAdd()),
                    ),*/
              Container(
                width: 1,
                height: 50,
                color: AppColors.LIGHT_GREY,
              ),
             /* InkWell(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    children: <Widget>[
                      Icon(
                        Icons.local_convenience_store,
                        color: AppColors.GREY,
                      ),
                      Text(
                        "Visit Store",
                        style: Theme.of(context)
                            .textTheme
                            .display1
                            .copyWith(fontSize: 10.0, color: AppColors.BLACk),
                      )
                    ],
                  ),
                ),
                onTap: () {
                  if (shopName != null && shopName.length != 0) {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => VendorProfile(shopName),
                        ));
                  }
                  return;
                },
              ),*/
              Expanded(
                child: FlatButton(
                    color: AppColors.DEEPORANGE,
                    child: Text(
                      "Buy Now",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      if (widget.products.stockQuantity == null)
                        buildSnackError(context, "Out of Stock ");
                      else
                        widget.productDetailBloc.add(BuyProductDialogShow());
                    }),
              ),
              Expanded(
                child: FlatButton(
                    color: AppColors.GREEN,
                    child: Text(
                      "Add to Cart",
                      style: TextStyle(color: Colors.white),
                    ),
                    onPressed: () {
                      if (widget.products.stockQuantity == null)
                        buildSnackError(context, "Out of Stock ");
                      else
                        widget.productDetailBloc.add(AddtoCartDialogShow());
                    }),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

_showAddtoCartDialog(BuildContext context, ProductDetailBloc productDetailBloc,
    _productDetailState, int stockQuantity) {
  int qty = 1;
  showDialog(
    context: context,
    builder: (BuildContext context) {
      return SimpleDialog(
        title: Center(
          child: new Text(
            "Select Quantity",
          ),
        ),
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                BlocBuilder(
                  bloc: productDetailBloc,
                  builder: (BuildContext context,
                      ProductDetailState productDetailState) {
                    if (productDetailState is QtyUpdated) {
                      qty = productDetailState.qty;
                      return NumberPicker.integer(
                          initialValue: qty,
                          minValue: 1,
                          maxValue: stockQuantity,
                          onChanged: (newValue) {
                            return productDetailBloc.add(QtySelected(newValue));
                          });
                    }
                    return NumberPicker.integer(
                        initialValue: qty,
                        minValue: 1,
                        maxValue: stockQuantity,
                        onChanged: (newValue) {
                          return productDetailBloc.add(
                            QtySelected(newValue),
                          );
                        });
                  },
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: new SimpleDialogOption(
                    child: Text(
                      "Save",
                    ),
                    onPressed: () {
                      if (_productDetailState is AddtoCartDialogShowing) {
                        productDetailBloc.add(AddtoCart());
                        Navigator.pop(context);
                      } else {
                        productDetailBloc.add(BuyProduct());
                        //Navigator.pop(context);
                        Navigator.push(context,   MaterialPageRoute(
                            builder: (context) => CheckOut()
                      ),);
                      }
                    },
                  ),
                ),
              ],
            ),
          ),
        ],
      );
    },
  );
}

class LoadImage extends StatelessWidget {
  final Products products;

  LoadImage(this.products);

  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    return Image.network(
      products.imgs[0].largeUrl,
      fit: BoxFit.fitHeight,
      height: width,
      width: width,
    );
  }
}
