import 'package:flutter/cupertino.dart';

@immutable
abstract class LoginEvent {}

class PostLogin extends LoginEvent {
  final String email;
  final String password;

  PostLogin(
    this.email,
    this.password,
  );
}

class FacebookLoginStart extends LoginEvent {}

class GoogleLoginStart extends LoginEvent {}
