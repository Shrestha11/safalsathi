abstract class LoginState {}

class InitialLoginState extends LoginState {}

class LoginError extends LoginState {
  final String message;

  LoginError(this.message);
}

class LoginSuccess extends LoginState {}

class Loginloading extends LoginState {}

class LoggedIn extends LoginState {}
