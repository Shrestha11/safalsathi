import 'dart:convert';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
//import 'package:firebase_auth/firebase_auth.dart';
//import 'package:flutter_facebook_login/flutter_facebook_login.dart';
//import 'package:google_sign_in/google_sign_in.dart';
//import 'package:onesignal_flutter/onesignal_flutter.dart';
import 'package:rxdart/rxdart.dart';
import 'package:safalsathi/Login/Bloc/LoginEvent.dart';
import 'package:safalsathi/Login/Bloc/LoginState.dart';
import 'package:safalsathi/Login/Model/facebookLoginGraphApiResponse.dart';
import 'package:safalsathi/Login/repository/LoginRespository.dart';
import 'package:safalsathi/Register/Model/LoginResponse.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/generic_response.dart';

import 'bloc.dart';

class LoginBloc extends Bloc<LoginEvent, LoginState> {
 // final FirebaseAuth _auth = FirebaseAuth.instance;
  //final GoogleSignIn googleSignIn = GoogleSignIn();
  bool isLoggedIn = false;
  //var facebookLogin = FacebookLogin();
  final LoginRepository _repository = LoginRepository();
  final BehaviorSubject<LoginResponse> _subject =
      BehaviorSubject<LoginResponse>();
  final BehaviorSubject<String> _error = BehaviorSubject<String>();
  //SocialLoginRepository socialLoginRepository = SocialLoginRepository();

 /* Future<String> signInWithGoogle() async {
    final GoogleSignInAccount googleSignInAccount = await googleSignIn.signIn();
    final GoogleSignInAuthentication googleSignInAuthentication =
        await googleSignInAccount.authentication;

    final AuthCredential credential = GoogleAuthProvider.getCredential(
      accessToken: googleSignInAuthentication.accessToken,
      idToken: googleSignInAuthentication.idToken,
    );

    final AuthResult authResult = await _auth.signInWithCredential(credential);
    final FirebaseUser user = authResult.user;

    assert(!user.isAnonymous);
    assert(await user.getIdToken() != null);

    final FirebaseUser currentUser = await _auth.currentUser();
    assert(user.uid == currentUser.uid);

    return 'signInWithGoogle succeeded: $user';
  }*/

 /* Future<FacebookLoginGraphApiResponse> initiateFacebookLogin() async {
    FacebookLoginGraphApiResponse _facebookResponse;
    var facebookLoginResult = await facebookLogin.logIn(['email']);
    switch (facebookLoginResult.status) {
      case FacebookLoginStatus.error:
        print("Error");
        return null;
        break;
      case FacebookLoginStatus.cancelledByUser:
        return null;
        break;
      case FacebookLoginStatus.loggedIn:
        print("LoggedIn");
        var graphResponse = await Dio().get(
            'https://graph.facebook.com/v2.12/me?fields=name,first_name,last_name,email&access_token=${facebookLoginResult.accessToken.token}');
        var profile = json.decode(graphResponse.data);

        _facebookResponse = FacebookLoginGraphApiResponse.fromJsonMap(profile);
        break;
    }
    return _facebookResponse;
  }*/

  LoginState get initialState => InitialLoginState();

  Stream<LoginState> mapEventToState(LoginEvent event) async* {
 /*   if (event is FacebookLoginStart) {
//      if (isLoggedIn)
//      logOut();

      FacebookLoginGraphApiResponse facebookLoginGraphApiResponse =
          await initiateFacebookLogin();
      yield LoggedIn(); //Processing
      if (facebookLoginGraphApiResponse != null) {
        ApiResponse apiResponse = await socialLoginRepository.socialLogin(
            facebookLoginGraphApiResponse.name,
            facebookLoginGraphApiResponse.first_name,
            facebookLoginGraphApiResponse.last_name,
            facebookLoginGraphApiResponse.email,
            facebookLoginGraphApiResponse.id,
            "facebook");
        if (apiResponse.apiCall == ApiResultStatus.SUCCESS) {
          LoginResponse loginResponse =
              LoginResponse.fromJsonMap(apiResponse.responseData);
          addStringToSF(loginResponse.data.accessToken);
          OneSignal.shared.setExternalUserId(loginResponse.userId.toString());
          yield LoggedIn();
        } else if (apiResponse.apiCall == ApiResultStatus.ERROR) {
          yield LoginError(apiResponse.message);
        } else if (apiResponse.apiCall == ApiResultStatus.NO_NETWORK) {
          yield LoginError(apiResponse.message);
        }
      }
    }*/
   /* if (event is GoogleLoginStart) {
      signInWithGoogle();
    }*/
    if (event is PostLogin) {
      yield Loginloading();
      Response response;
      var ee;
      try {
        response = await _repository.postLogin(event.email, event.password);
      } catch (e) {
        yield LoginError(e.toString());
      }
      if (response == null) {
        yield LoginError("NULL");
      }

      if (response != null) {
        LoginResponse loginResponse;
        try {
          loginResponse = LoginResponse.fromJsonMap(response.data);
        } catch (e) {
          print("EXCEPTION LOGIN RESPONSE  $e");
        }
        final pattern = RegExp('.{1,800}'); // 800 is the size of each chunk
        pattern
            .allMatches(loginResponse.data.accessToken)
            .forEach((match) => print(" ${match.group(0)}"));

        addStringToSF(loginResponse.data.accessToken);
        //OneSignal.shared.setExternalUserId(loginResponse.userId.toString());
        _subject.sink.add(loginResponse);
        _error.sink.add(ee.toString());

        yield LoginSuccess();
      }
    }

    @override
    dispose() {
      _subject.close();
      _error.close();
    }
  }

  BehaviorSubject<LoginResponse> get subject => _subject;

  BehaviorSubject<String> get error => _error;
}

final loginBloc = LoginBloc();
