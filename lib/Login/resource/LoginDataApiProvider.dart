import 'package:dio/dio.dart';
import 'package:safalsathi/common/DioBase.dart';

class LoginDataApiProvider {
  Future<Response> postLogin(String email, String password) async {
    try {
      String _endpoint = "/login";
      Response response = await dioBASE()
          .post(_endpoint, data: {"email": email, "password": password});
      return response;
    } on DioError catch (dioError) {
      return Future.error(dioError.toString());
    } catch (error) {
      return Future.error(error);
    }
  }
}
