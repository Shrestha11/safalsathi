import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/Login/Bloc/LoginEvent.dart';
import 'package:safalsathi/Login/Bloc/LoginState.dart';
import 'package:safalsathi/Register/RegisterPage.dart';
import 'package:safalsathi/common/color.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/string.dart';
import 'package:safalsathi/legals/screens/legals.dart';

import 'Bloc/LoginBloc.dart';

class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: <Widget>[
          Container(
            child: ClipRRect(
              // make sure we apply clip it properly
              child: BackdropFilter(
                filter: ImageFilter.blur(sigmaX: 2, sigmaY: 2),
                child: Container(
                  alignment: Alignment.center,
                  color: Colors.grey.withOpacity(0.1),
                ),
              ),
            ),
            decoration: BoxDecoration(
              image: DecorationImage(
                image: ExactAssetImage("images/loginWall.jpeg"),
                fit: BoxFit.cover,
              ),
            ),
          ),
          LoginPage2(),
        ],
      ),
    );
  }
}

class LoginPage2 extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _Login();
  }
}

class _Login extends State<LoginPage2> {
  LoginBloc loginBloc;
  GlobalKey<FormState> _key = new GlobalKey();
  String email, password;
  bool isLoading = false;
  bool isTermsAgreed = false;

  @override
  void initState() {
    loginBloc = LoginBloc();

    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: loginBloc,
      listener: (context, LoginState state) {
        if (state is LoginError) {
          _onWidgetDidBuild(() {
            showDialog(
                context: context,
                builder: (BuildContext context) {
                  return SimpleDialog(
                    title: Center(
                      child: Text("User Not Verified", style: TextStyle()),
                    ),
                    children: <Widget>[
                      FlatButton(
                        child: Text('Ok'),
                        onPressed: () {
                          Navigator.pop(context);
                        },
                      ),
                    ],
                  );
                });
          });
        }
        if (state is LoginSuccess) {

          Navigator.of(context).popUntil((route) => route.isFirst);

//                Navigator.push(
//                    context,
//                    MaterialPageRoute(
//                      builder: (context) => Home(),
//                    ));
        }
        if (state is Loginloading) {
          Scaffold.of(context).showSnackBar(
            SnackBar(
                duration: new Duration(seconds: 1),
                content: Row(
                  children: <Widget>[
                    new CircularProgressIndicator(),
                    SizedBox(width: 40),
                    Text('Signing-In'),
                  ],
                )),
          );
        }
        if (state is LoggedIn) {
          return Navigator.of(context).popUntil((route) => route.isFirst);
        }
      },
      child: BlocBuilder<LoginBloc, LoginState>(
        bloc: loginBloc,
        builder: (BuildContext context, LoginState state) {
          return formUi(context);
        },
      ),
    );
  }

  Widget formUi(BuildContext context2) {
    return SingleChildScrollView(
      child: Form(
        key: _key,
        child: Padding(
          padding: const EdgeInsets.fromLTRB(28.0, 28.0, 28.0, 0.0),
          child: new Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 100.0,
              ),
              Padding(
                padding: const EdgeInsets.fromLTRB(80.0, 0.0, 0.0, 0.0),
                child: new Text(
                  "SafalSathi",
                  style: Theme.of(context)
                      .textTheme
                      .body1
                      .copyWith(fontSize: 30.0, color: AppColors.WHITE),
                ),
              ),
              SizedBox(
                height: 50.0,
              ),

              Padding(
                padding: const EdgeInsets.all(8.0),
                child: new TextFormField(
                  validator: validateEmail,
                  keyboardType: TextInputType.emailAddress,
                  autofocus: false,
                  style: TextStyle(color: AppColors.WHITE),
                  decoration: InputDecoration(
                    labelText: "Email",
                    fillColor: Colors.grey,
                    enabledBorder: OutlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.white, width: 2.0),
                      borderRadius: BorderRadius.circular(2.0),
                    ),
                    focusColor: AppColors.WHITE,
                    border: OutlineInputBorder(
                      borderSide:
                          const BorderSide(color: Colors.white, width: 2.0),
                      borderRadius: BorderRadius.circular(2.0),
                    ),
                    labelStyle: TextStyle(color: AppColors.WHITE),
                  ),
                  onSaved: (String val) {
                    email = val;
                  },
                ),
              ),
              new SizedBox(
                height: 15.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: new TextFormField(
                  autofocus: false,
                  obscureText: true,
                  validator: validatePassword,
                  style: TextStyle(color: AppColors.WHITE),
                  decoration: InputDecoration(
                      labelText: "Password",
                      fillColor: Colors.grey,
                      enabledBorder: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      focusColor: AppColors.WHITE,
                      border: OutlineInputBorder(
                        borderSide:
                            const BorderSide(color: Colors.white, width: 2.0),
                        borderRadius: BorderRadius.circular(2.0),
                      ),
                      labelStyle: TextStyle(color: AppColors.WHITE)),
                  onSaved: (String val) {
                    password = val;
                  },
                ),
              ),
              Row(
                children: <Widget>[
                  Checkbox(
                    onChanged: (bool value) {
                      setState(() {
                        isTermsAgreed = value;
                      });
                    },
                    focusColor: AppColors.WHITE,
                    checkColor: AppColors.WHITE,
                    value: isTermsAgreed,
                  ),
                  Text(
                    "I agree with ",
                    style: new TextStyle(
                        color: AppColors.WHITE, fontWeight: FontWeight.w300),
                  ),
                  InkWell(
                    onTap: () => Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => LegalsPage(),
                        )),
                    child: Text(
                      "Terms and Condition",
                      style: new TextStyle(
                          color: AppColors.WHITE, fontWeight: FontWeight.w900),
                    ),
                  )
                ],
              ),
              InkWell(
                onTap: () => isTermsAgreed
                    ? fetchPost()
                    : buildSnackError(
                        context2, "Agree with Terms and Condition"),
                child: new Container(
                    alignment: Alignment.center,
                    height: 60.0,
                    decoration: new BoxDecoration(
                        color: AppColors.WHITE,
                        borderRadius: new BorderRadius.circular(9.0)),
                    child: new Text("Login",
                        style: new TextStyle(
                            fontSize: 20.0, color: AppColors.PRIMARY_RED))),
              ),
              InkWell(
                onTap: () => launchURL(forgotPassword),
                child: Padding(
                  padding:
                      const EdgeInsets.only(left: 10.0, right: 20.0, top: 10.0),
                  child: new Container(
                      alignment: Alignment.center,
                      height: 60.0,
                      child: new Text("Forgot Password?",
                          style: new TextStyle(
                              fontSize: 17.0, color: AppColors.WHITE))),
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(child: Container()),
                    Text("Login With",
                        style: new TextStyle(
                            fontSize: 17.0,
                            color: AppColors.WHITE,
                            fontWeight: FontWeight.w300)),
                    Expanded(child: Container()),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(child: Container()),
                    InkWell(
                      onTap: () => loginBloc.add(GoogleLoginStart()),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          "images/google_logo.png",
                          width: 35.0,
                        ),
                      ),
                    ),
                    InkWell(
                      onTap: () => loginBloc.add(FacebookLoginStart()),
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Image.asset(
                          "images/fb_logo.png",
                          width: 50.0,
                        ),
                      ),
                    ),
                    Expanded(child: Container()),
                  ],
                ),
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
                  Text("Don't have an account?",
                      style: new TextStyle(
                          color: AppColors.WHITE, fontWeight: FontWeight.w300)),
                  InkWell(
                    onTap: () => Navigator.pushReplacement(
                        context,
                        MaterialPageRoute(
                          builder: (context) => RegisterPage(),
                        )),
                    child: new Text(" Sign Up",
                        style: new TextStyle(color: AppColors.WHITE)),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  void _onWidgetDidBuild(Function callback) {
    WidgetsBinding.instance.addPostFrameCallback((_) {
      callback();
    });
  }

  void fetchPost() async {
    if (_key.currentState.validate()) {
      _key.currentState.save();
      loginBloc.add(PostLogin(email, password));
    }
  }
}
