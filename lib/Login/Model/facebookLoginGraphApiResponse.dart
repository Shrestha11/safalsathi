
class FacebookLoginGraphApiResponse {

  final String name;
  final String first_name;
  final String last_name;
  final String email;
  final String id;

	FacebookLoginGraphApiResponse.fromJsonMap(Map<String, dynamic> map): 
		name = map["name"],
		first_name = map["first_name"],
		last_name = map["last_name"],
		email = map["email"],
		id = map["id"];

	Map<String, dynamic> toJson() {
		final Map<String, dynamic> data = new Map<String, dynamic>();
		data['name'] = name;
		data['first_name'] = first_name;
		data['last_name'] = last_name;
		data['email'] = email;
		data['id'] = id;
		return data;
	}
}
