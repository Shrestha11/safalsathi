class GoogleUserResponse {
  final String givenName;
  final String locale;
  final String familyName;
  final String picture;
  final String aud;
  final String azp;
  var exp;
  var iat;
  final String iss;
  var sub;
  final String name;
  final String email;
  final bool emailVerified;

  GoogleUserResponse.fromJsonMap(Map<String, dynamic> map)
      : givenName = map["given_name"],
        locale = map["locale"],
        familyName = map["family_name"],
        picture = map["picture"],
        aud = map["aud"],
        azp = map["azp"],
        exp = map["exp"],
        iat = map["iat"],
        iss = map["iss"],
        sub = map["sub"],
        name = map["name"],
        email = map["email"],
        emailVerified = map["email_verified"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['given_name'] = givenName;
    data['locale'] = locale;
    data['family_name'] = familyName;
    data['picture'] = picture;
    data['aud'] = aud;
    data['azp'] = azp;
    data['exp'] = exp;
    data['iat'] = iat;
    data['iss'] = iss;
    data['sub'] = sub;
    data['name'] = name;
    data['email'] = email;
    data['email_verified'] = emailVerified;
    return data;
  }
}
