import 'package:dio/dio.dart';
import 'package:safalsathi/Login/resource/LoginDataApiProvider.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/error_handler.dart';
import 'package:safalsathi/common/generic_response.dart';
import 'package:safalsathi/common/string.dart';

class LoginRepository {
  LoginDataApiProvider _apiProvider = LoginDataApiProvider();

  Future<Response> postLogin(String email, String password) {
    return _apiProvider.postLogin(email, password);
  }
}

class SocialLoginRepository {
  SocialLoginApiProvider _socialLoginApiProvider = SocialLoginApiProvider();

  Future<ApiResponse> socialLogin(String name, String firstName,
      String lastName, String email, String providerId, String providerName) {
    return _socialLoginApiProvider.socialLogin(
        name, firstName, lastName, email, providerId, providerName);
  }
}

class SocialLoginApiProvider {
  Future<ApiResponse> socialLogin(
      String name,
      String firstName,
      String lastName,
      String email,
      String providerId,
      String providerName) async {
    try {
      print(name + firstName + providerId + providerName);

      Response response = await dioBASE().post(urlSocialLogin, data: {
        "first_name": firstName,
        "last_name": lastName,
        "user_name": name,
        "email": email,
        "provider": providerName,
        "provider_id": providerId,
      });

      return ApiResponse(
          responseData: response.data,
          apiCall: ApiResultStatus.SUCCESS,
          message: "No Error");
    } on DioError catch (e) {
      print("EROR $e");
      if (e.type == DioErrorType.DEFAULT)
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.NO_NETWORK,
            message:
                "Make sure wifi or cellular data is turned on, Then try again");
      else
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.ERROR,
            message: handleError(e));
    } catch (error) {
      print("DIO Api Error $error");

      return ApiResponse(
          responseData: error,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(error));
    }
  }
}
