import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:dio/dio.dart';
import 'package:safalsathi/common/DioBase.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/error_handler.dart';
import 'package:safalsathi/common/generic_response.dart';
import 'package:safalsathi/common/string.dart';
import 'package:safalsathi/wishList/model/wishListResponse.dart';

import './bloc.dart';

class WishListBloc extends Bloc<WishListEvent, WishListState> {
  @override
  WishListState get initialState => InitialWishListState();

  @override
  Stream<WishListState> mapEventToState(
    WishListEvent event,
  ) async* {
    if (event is LoadWishListEvent) {
      yield WishListLoadingState();
      WishListRepository _wishListRepository = WishListRepository();
      try {
        ApiResponse _apiResponse = await _wishListRepository.loadWishList();
        if (_apiResponse.apiCall == ApiResultStatus.ERROR) {
          yield WishListLoadingErrorState(_apiResponse.message);
        } else if (_apiResponse.apiCall == ApiResultStatus.SUCCESS) {
          //remove all updated list
//        currentPage = 1;
          try {
            WishListResponse.fromJsonMap(_apiResponse.responseData);
          } catch (e) {
            print("ERROR $e");
          }
          yield WishListLoadedState(
              WishListResponse.fromJsonMap(_apiResponse.responseData));
        } else if (_apiResponse.apiCall == ApiResultStatus.NO_NETWORK) {
          yield WishListLoadingErrorState(_apiResponse.message);
        }
      } catch (e) {
        yield WishListLoadingErrorState(e.toString());
      }
    }
  }
}

class WishListRepository {
  WishListApiProvider _wishListApiProvider = WishListApiProvider();

  Future<ApiResponse> loadWishList() {
    return _wishListApiProvider.loadWishListResponse();
  }
}

class WishListApiProvider {
  Future<ApiResponse> loadWishListResponse() async {
    try {
      Response response = await dioBASE(await getAuth()).get(wishListUrl);
      print("RESPONSE $response");
      return ApiResponse(
          responseData: response.data,
          apiCall: ApiResultStatus.SUCCESS,
          message: "No Error");
    } on DioError catch (e) {
      print("DIO ERROR $e");
      if (e.type == DioErrorType.DEFAULT)
        return ApiResponse(
            responseData: e,
            apiCall: ApiResultStatus.NO_NETWORK,
            message:
                "Make sure wifi or cellular data is turned on, Then try again");
//      else
      return ApiResponse(
          responseData: e,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(e));
    } catch (error) {
      return ApiResponse(
          responseData: error,
          apiCall: ApiResultStatus.ERROR,
          message: handleError(error));
    }
  }
}
