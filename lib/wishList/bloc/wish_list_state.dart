import 'package:meta/meta.dart';
import 'package:safalsathi/wishList/model/wishListResponse.dart';

@immutable
abstract class WishListState {}

class InitialWishListState extends WishListState {}

class WishListLoadingState extends WishListState {}

class WishListLoadedState extends WishListState {
  final WishListResponse wishListResponse;

  WishListLoadedState(this.wishListResponse);
}


class WishListLoadingErrorState extends WishListState {
  final String message;

  WishListLoadingErrorState(this.message);
}

class WIshListNetworkErrorState extends WishListState {}
