import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:safalsathi/common/color.dart';
import 'package:safalsathi/common/commonMethod.dart';
import 'package:safalsathi/common/common_ui.dart';
import 'package:safalsathi/common/model/products.dart';
import 'package:safalsathi/product_detail/productDetail.dart';
import 'package:safalsathi/wishList/bloc/bloc.dart';
import 'package:safalsathi/wishList/model/wishListResponse.dart';

class WishList extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.PRIMARY_COLOR_DARK,
      body: SafeArea(
        child: Column(
          children: <Widget>[
            CustomAppBar(),
            Expanded(
              child: WishListBody(),
            ),
          ],
        ),
      ),
    );
  }
}

class WishListBody extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return WishListState();
  }
}

class WishListState extends State<WishListBody> {
  WishListBloc _wishListBloc = WishListBloc();
  WishListResponse _wishListResponse;

  @override
  void initState() {
    super.initState();
    _wishListBloc.add(LoadWishListEvent());
  }

  @override
  Widget build(BuildContext context) {
    return BlocListener(
      bloc: _wishListBloc,
      listener: (context, state) {
        if (state is WishListLoadedState) {
          _wishListResponse = state.wishListResponse;
        }
      },
      child: Container(
        color: AppColors.WHITE,
        width: MediaQuery.of(context).size.width,
        child: BlocBuilder(
          bloc: _wishListBloc,
          builder: (context, state) {
            if (state is WishListLoadingState) {
              return buildLoadingWidget(context);
            }
            if (state is WishListLoadingErrorState) {
              return Text("ERRROr ${state.message}");
            }
            if (_wishListResponse.wishlist == null ||
                _wishListResponse.wishlist.length == 0) {
              return buildEmptyWidget(context);
            }
            return _wishListWidget(_wishListResponse.wishlist, context);
          },
        ),
      ),
    );
  }
}

Widget _wishListWidget(List<Products> wishList, BuildContext context) {
  return ListView.builder(
      shrinkWrap: true,
      itemCount: wishList.length,
      itemBuilder: (context, position) {
        return InkWell(
          onTap: () => Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ProductDetail(
                products: wishList[position],
              ),
            ),
          ),
          child: Card(
            child: Row(
              children: <Widget>[
                Expanded(
                  child: Stack(
                    children: <Widget>[
                      Image.network(wishList[position].imgs[0].mediumUrl),
                      Container(
                        color: AppColors.GREY,
                        padding: EdgeInsets.all(2.0),
                        child: Padding(
                          padding: const EdgeInsets.all(4.0),
                          child: Text(
                            discount(
                              wishList[position].productPrice,
                              wishList[position].salesPrice,
                            ),
                            style: Theme.of(context).textTheme.subhead.copyWith(
                                color: AppColors.WHITE, fontSize: 12.0),
//                          style: greenDiscountSmall,
                          ),
                        ),
                      ),
                    ],
                  ),
                  flex: 2,
                ),
                Expanded(
                  child: Container(
//                    height: double.infinity,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                          wishList[position].name,
                          maxLines: 2,
                        ),
                        Row(
                          children: <Widget>[
                            Container(
                              decoration: ShapeDecoration(
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(6.0),
                                    side: BorderSide(color: Colors.grey)),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Row(
                                  children: <Widget>[
                                    Icon(
                                      Icons.star,
                                      color: Colors.yellowAccent.shade400,
                                      size: 20.0,
                                    ),
                                    Text(
                                      "0 Star",
                                    ),
                                  ],
                                ),
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text(
                                  wishList[position].ratings.total.toString()),
                            ),
                          ],
                        ),
                        Row(
                          children: <Widget>[
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text(
                                "Rs. 16500",
//                              style: highligttedTextStyle,
                              ),
                            ),
                            Padding(
                              padding: const EdgeInsets.all(4.0),
                              child: Text(
                                "Rs. 2000",
//                          style: greySmallText,
                              ),
                            )
                          ],
                        ),
                        InkWell(
                          onTap: () => print("DELETE"),
//                            _wishListBloc.dispatch(
//                                DeleteWishListEvent(wishlist[position].id)),
                          child: Icon(
                            Icons.delete,
                            color: Colors.red,
                            size: 18.0,
                          ),
                        )
                      ],
                    ),
                  ),
                  flex: 5,
                ),
              ],
            ),
          ),
        );
      });
}
