import 'package:safalsathi/common/model/products.dart';

class WishListResponse {
  final List<Products> wishlist;

  WishListResponse.fromJsonMap(Map<String, dynamic> map)
      : wishlist = List<Products>.from(
            map["wishlist"].map((it) => Products.fromJsonMap(it)));

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['wishlist'] =
        wishlist != null ? this.wishlist.map((v) => v.toJson()).toList() : null;
    return data;
  }
}
