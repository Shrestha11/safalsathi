import 'package:safalsathi/home_main/model/categories.dart';
import 'package:flutter/material.dart';

import 'CategoriesDetail.dart';

class CategoryItemSecond extends StatefulWidget {
  final List<SubCategory> subCategoriyss;
  final String name;

  CategoryItemSecond(this.subCategoriyss, this.name);

  @override
  State<StatefulWidget> createState() {
    return _CategoryState(subCategoriyss, name);
  }
}

class _CategoryState extends State<CategoryItemSecond> {
  final List<SubCategory> subCategoryss;
  final String name;

  _CategoryState(this.subCategoryss, this.name);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text(name),
      ),
      body: Center(
        child: subCategoryss == null
            ? Text("EMPTY")
            : _categoryItem(subCategoryss, context),
      ),
    );
  }
}

Widget _categoryItem(List<SubCategory> subCategoryss, BuildContext context) {
  return GridView.count(
      crossAxisCount: 2,
      children: List.generate(subCategoryss.length, (index) {
        return Card(
          child: InkWell(
            onTap: () => Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => CategoriesDetail(
                    subCategoryss[index].id, subCategoryss[index].name),
              ),
            ),
            child: Stack(
              children: <Widget>[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 22.0),
                    child: Image.network(
                        subCategoryss[index] != null
                            ? subCategoryss[index].img
                            : Icon(
                                Icons.favorite,
                                color: Colors.red,
                                size: 30.0,
                              ),
                        fit: BoxFit.fitHeight),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      subCategoryss[index].name,
                      style: TextStyle(fontSize: 14, color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }));
}
