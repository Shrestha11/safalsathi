import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:safalsathi/ProductDetailPkg/productDetail.dart';
import 'package:safalsathi/common/common_ui.dart';

import 'package:safalsathi/common/model/products.dart';
import 'package:safalsathi/product_detail/productDetail.dart';

import '../app_styles.dart';

import 'bloc/categories_bloc.dart';
import 'bloc/categories_event.dart';
import 'bloc/categories_state.dart';
import 'model/CategoriesResponse.dart';
import 'model/brands.dart';

class CategoriesDetail extends StatefulWidget {
  final int categoriesId;
  final String name;

  CategoriesDetail(this.categoriesId, this.name);

  @override
  State<StatefulWidget> createState() {
    return _CategoriesDetailState(categoriesId, name);
  }
}

class _CategoriesDetailState extends State<CategoriesDetail> {
  int categoriesId;
  CategoriesBloc _categoriesBloc = CategoriesBloc();
  SearchResponse _categoriesResponse;
  String name;

  _CategoriesDetailState(this.categoriesId, this.name);

  @override
  void initState() {
    _categoriesBloc.add(LoadSearchEvent(categoriesId));
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(

          title: Text(name,style:TextStyle(color: Colors.white)),
          iconTheme: IconThemeData(color: Colors.white),
          backgroundColor: Colors.deepOrange,
        ),
        body: BlocListener(
          bloc: _categoriesBloc,
          listener: (context, _categoriesState) {
            if (_categoriesState is SortDialogDisplayState) {
              _showSortDialog(context, _categoriesBloc);
            }
            if (_categoriesState is FilterDialogDisplayState) {
              showModalBottomSheet(
                  shape: OutlineInputBorder(
                      borderRadius: BorderRadius.all(Radius.circular(4.0))),
                  isScrollControlled: false,
                  context: context,
                  builder: (BuildContext bc) {
                    return DynamicDialog(_categoriesBloc);
                  });
            }
          },
          child: BlocBuilder(
              bloc: _categoriesBloc,
              builder:
                  (BuildContext context, CategoriesState _categoriesState) {
                if (_categoriesState is InitialCategoriesState) {
                  return Container();
                }
                if (_categoriesState is LoadingSearch) {
                  return buildLoadingWidget(context);
                }
                if (_categoriesState is LoadedSearch) {
                  _categoriesResponse = _categoriesState.categoriesResponse;
                }
                return _filterSort(
                    _categoriesBloc, context, _categoriesResponse);
              }),
        ));
  }

  void _showSortDialog(BuildContext context, CategoriesBloc _categoriesBloc) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('Popularity'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(0));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('Price: High to Low'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(1));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    dense: true,
                    title: new Text('Price: Low to High'),
                    onTap: () {
                      _categoriesBloc.add(SortSelected(2));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('Newest'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(3));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('Oldest'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(4));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('A-Z'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(5));
                      Navigator.of(context).pop(true);
                    },
                  ),
                  new ListTile(
                    leading: new Icon(
                      Icons.radio_button_unchecked,
                      size: 18,
                    ),
                    title: new Text('Z-a'),
                    dense: true,
                    onTap: () {
                      _categoriesBloc.add(SortSelected(6));
                      Navigator.of(context).pop(true);
                      Navigator.of(context).pop(true);
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  _filterSort(CategoriesBloc _searchBlocBloc, BuildContext context,
      SearchResponse categoriesResponse) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            InkWell(
              onTap: () => _searchBlocBloc.add(SortDialogClick()),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                  child: Container(
                    width: (MediaQuery.of(context).size.width / 2) - 10.0,
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.sort,
                          size: 25.0,
                        ),
                        Expanded(
                          child: Center(
                            child: Text(
                              "Sort",
                              style: seconTextStyle,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
            InkWell(
              onTap: () => _searchBlocBloc.add(FilterDialogClick()),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(0.0, 8.0, 0.0, 8.0),
                  child: Container(
                    width: (MediaQuery.of(context).size.width / 2) - 10.0,
                    child: Row(
                      children: <Widget>[
                        Icon(
                          Icons.filter_list,
                          size: 25.0,
                        ),
                        Expanded(
                          child: Center(
                            child: Text(
                              "Filter",
                              style: seconTextStyle,
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
        Expanded(
          child: ListView.builder(
            shrinkWrap: true,
//          physics: ClampingScrollPhysics(),
            itemCount: categoriesResponse.productsList.products.length,
            itemBuilder: (context, position) {
              return _productListItem(
                  categoriesResponse.productsList.products[position],
                  context,
                  position);
            },
          ),
        ),
      ],
    );
  }

  _productListItem(Products product, BuildContext context, int position) {
    return InkWell(
     onTap: () => Navigator.push(
          context,
          MaterialPageRoute(
              builder: (context) => ProductDetail(
                    products: product,
                  ))),
      child: Card(
        child: Row(
          children: <Widget>[
            Image.network(
              product.imgs[0].mediumUrl,
              height: MediaQuery.of(context).size.width / 2.5,
            ),
            Expanded(
              child: Container(
                height: MediaQuery.of(context).size.width / 2.5,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      product.name,
                      softWrap: true,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 2,
                    ),
                    Text(
                      "${product.stockQuantity.toString()} "
                      "Remaining",
                      style: greyText,
                    ),
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Container(
                              decoration: new BoxDecoration(
                                  borderRadius: BorderRadius.circular(5.0),
                                  border: new Border.all(
                                      color: Colors.grey, width: 1.0)),
                              child: Padding(
                                padding: const EdgeInsets.all(2.0),
                                child: Row(
                                  children: <Widget>[
                                    Text(product.ratings.getAverage()),
                                    Icon(
                                      Icons.star,
                                      color: Colors.orangeAccent,
                                      size: 20.0,
                                    )
                                  ],
                                ),
                              )),
                        ),
                        Text("Out of "
                            "${product.ratings.total.toString()}"
                            " Ratings"),
                      ],
                    ),
                    Row(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            product.getsalePrice(),
                            style: seconTextStyle,
                          ),
                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text(
                            product.getPrice(),
                            style: strikethoughDiscount,
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DynamicDialog extends StatefulWidget {
  final CategoriesBloc categoriesBloc;

  DynamicDialog(this.categoriesBloc);

  @override
  State<StatefulWidget> createState() {
    return _DialogState(categoriesBloc);
  }
}

class _DialogState extends State<DynamicDialog> {
  final CategoriesBloc categoriesBloc;

  _DialogState(this.categoriesBloc);

  double minValue = 10.0, maxValue = 99.0;
  List<Brands> brandList = [];
  List<String> sizeList = [];
  List<String> colorList = [];
  List<bool> brandSelected = [];
  List<bool> sizeSelected = [];
  List<bool> colorSelected = [];

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return BlocBuilder(
        bloc: categoriesBloc,
        builder: (context, _categoriesState) {
          if (_categoriesState is FilterDialogDisplayState) {
            minValue = categoriesBloc.minRangePrice.toDouble();
            maxValue = categoriesBloc.maxRanagePrice.toDouble();
            brandList = categoriesBloc.brandList;
            sizeList = categoriesBloc.sizeList;
            colorList = categoriesBloc.colorList;
            brandSelected = categoriesBloc.brandSelected;
            sizeSelected = categoriesBloc.sizeSelected;
            colorSelected = categoriesBloc.colorSelected;
          }
          if (_categoriesState is PriceUpdated) {
            minValue = categoriesBloc.minRangePrice.toDouble();
            maxValue = categoriesBloc.maxRanagePrice.toDouble();
          }
          if (_categoriesState is SizeUpdated) {
            sizeSelected = _categoriesState.sizeSelected;
          }
          if (_categoriesState is BrandsUpdated) {
            brandSelected = _categoriesState.brandSelected;
          }
          if (_categoriesState is ColorUpdated) {
            colorSelected = _categoriesState.colorSelected;
          }
          return SingleChildScrollView(
              child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(8.0),
                child:
              Align(
                alignment: Alignment.topLeft,
                  child: Text(
                "Apply Filter",
                //style: redText,
                    style:TextStyle(
                      fontSize: 20
                    )
              ),),),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Center(
                            child: Text(
                          "Price",
                          style: greyText,
                        )),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                        ),
                      ],
                    ),
                    SizedBox(height: 4,),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        Text("MinPrice: \nRs.${minValue * 1000}",style: TextStyle(
                          fontSize: 14,
                        ),),
                        Text("MaxPrice: \nRs.${maxValue * 1000}",style: TextStyle(
                          fontSize: 14,
                        ),),
                      ],
                    ),
                    RangeSlider(
                        values: RangeValues(minValue, maxValue),
                        min: 10,
                        max: 100,
                        onChanged: (RangeValues rangeValues) {
                          categoriesBloc.add(PriceChanged(rangeValues));
                        }),
                  ],
                ),
              ), //Price
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Brands",
                          style: greyText,
                        ),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                        ),
                      ],
                    ),
                    brandList == null
                        ? Container()
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: brandList.length,
                            itemBuilder: (context, position) {
                              return Row(
                                children: <Widget>[
                                  Checkbox(
                                    value: brandSelected[position],
                                    onChanged: (bool value) {
                                      if (value) {
                                        categoriesBloc
                                            .add(BrandSelected(position));
                                      } else {
                                        categoriesBloc.add(
                                            BrandDeselected(position));
                                      }
                                    },
                                  ),
                                  Text(brandList[position].name),
                                ],
                              );
                            },
                          ),
                  ],
                ),
              ), //Brands
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Sizes",
                          style: greyText,
                        ),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                        ),
                      ],
                    ),
                    sizeList == null
                        ? Container()
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: sizeList.length,
                            itemBuilder: (context, position) {
                              return Row(
                                children: <Widget>[
                                  Checkbox(
                                    value: sizeSelected[position],
                                    onChanged: (bool value) {
                                      if (value) {
                                        categoriesBloc
                                            .add(SizeSelected(position));
//                                    Navigator.pop(context);
                                      } else {
                                        categoriesBloc
                                            .add(SizeDeselected(position));
//                                    Navigator.pop(context);
                                      }
                                    },
                                  ),
                                  Text(sizeList[position]),
                                ],
                              );
                            },
                          ),
                  ],
                ),
              ), //Sizes
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  children: <Widget>[
                    Row(
                      children: <Widget>[
                        Text(
                          "Color",
                          style: greyText,
                        ),
                        new Icon(
                          Icons.arrow_drop_down,
                          size: 18,
                        ),
                      ],
                    ),
                    colorList == null
                        ? Container()
                        : ListView.builder(
                            shrinkWrap: true,
                            physics: NeverScrollableScrollPhysics(),
                            itemCount: colorList.length,
                            itemBuilder: (context, position) {
                              return Row(
                                children: <Widget>[
                                  Checkbox(
                                      value: colorSelected[position],
                                      onChanged: (bool value) {
                                        if (value) {
                                          categoriesBloc.add(
                                              ColorSelected(position));
//                                        Navigator.pop(context);
                                        } else {
                                          categoriesBloc.add(
                                              ColorDeselected(position));
//                                        Navigator.pop(context);
                                        }
                                      }),
                                  Text(colorList[position]),
                                ],
                              );
                            },
                          ),
                  ],
                ),
              ),
              RaisedButton(
                child: Text("Filter Products"),
                onPressed: () => categoriesBloc.add(FilterApply()),
              )
            ],
          ));
        });
  }
}
