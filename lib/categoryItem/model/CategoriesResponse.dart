import 'package:safalsathi/categoryItem/model/products.dart';

import 'brands.dart';

class SearchResponse {
  final ProductList productsList;
  final List<Brands> brands;
  final List<String> colors;
  final List<String> sizes;

  SearchResponse.fromJsonMap(Map<String, dynamic> map)
      : productsList = ProductList.fromJsonMap(map["products"]),
        brands = List<Brands>.from(
                    map["brands"].map((it) => Brands.fromJsonMap(it))) ==
                null
            ? null
            : List<Brands>.from(
                map["brands"].map((it) => Brands.fromJsonMap(it))),
        colors = List<String>.from(map["colors"]) == null
            ? null
            : List<String>.from(map["colors"]),
        sizes = List<String>.from(map["sizes"]) == null
            ? null
            : List<String>.from(map["sizes"]);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['products'] = productsList == null ? null : productsList.toJson();
    data['brands'] =
        brands != null ? this.brands.map((v) => v.toJson()).toList() : null;
    data['colors'] = colors == null ? null : colors;
    data['sizes'] = sizes == null ? null : sizes;
    return data;
  }
}
