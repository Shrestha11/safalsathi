import 'package:safalsathi/categoryItem/model/FilteredResponse/productsList.dart';

class FilteredResponse {
  final ProductsList productsList;

  FilteredResponse.fromJsonMap(Map<String, dynamic> map)
      : productsList = ProductsList.fromJsonMap(map["products"]);

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['products'] = productsList == null ? null : productsList.toJson();
    return data;
  }
}
