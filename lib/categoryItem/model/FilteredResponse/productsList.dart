import 'package:safalsathi/common/model/products.dart';

class ProductsList {
  final int currentPage;
  final List<Products> data;
  final String firstPageUrl;
  final int from;
  final int lastPage;
  final String lastPageUrl;
  final String nextPageUrl;
  final String path;
  final int perPage;
  final Object perPageUrl;
  final int to;
  final int total;

  ProductsList.fromJsonMap(Map<String, dynamic> map)
      : currentPage = map["current_page"],
        data = List<Products>.from(
            map["data"].map((it) => Products.fromJsonMap(it))),
        firstPageUrl = map["first_page_url"],
        from = map["from"],
        lastPage = map["last_page"],
        lastPageUrl = map["last_page_url"],
        nextPageUrl = map["next_page_url"],
        path = map["path"],
        perPage = map["per_page"],
        perPageUrl = map["prev_page_url"],
        to = map["to"],
        total = map["total"];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> datas = new Map<String, dynamic>();
    datas['current_page'] = currentPage;
    datas['data'] =
        data != null ? this.data.map((v) => v.toJson()).toList() : null;
    datas['first_page_url'] = firstPageUrl;
    datas['from'] = from;
    datas['last_page'] = lastPage;
    datas['last_page_url'] = lastPageUrl;
    datas['next_page_url'] = nextPageUrl;
    datas['path'] = path;
    datas['per_page'] = perPage;
    datas['prev_page_url'] = perPageUrl;
    datas['to'] = to;
    datas['total'] = total;
    return datas;
  }
}
