import 'package:flutter/material.dart';
//import 'package:mazzako/home/model/sub_category.dart';
import 'package:safalsathi/home_main/model/categories.dart';
import 'CategoriesDetail.dart';

class CategoryItem extends StatefulWidget {
  final List<SubCategory> subCategoriesList;
  final String name;

  CategoryItem(this.subCategoriesList, this.name);

  @override
  State<StatefulWidget> createState() {
    return _CategoryState(subCategoriesList, name);
  }
}

class _CategoryState extends State<CategoryItem> {
  List<SubCategory> categoryList;
  String name;

  _CategoryState(this.categoryList, this.name);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: new AppBar(
        title: Text(name,style:TextStyle(color: Colors.white)),
        iconTheme: IconThemeData(color: Colors.white),
        backgroundColor: Colors.deepOrange,
      ),
      body: Center(
        child: categoryList == null
            ? Text("empty")
            : _categoryItem(categoryList, context),
      ),
    );
  }
}

Widget _categoryItem(List<SubCategory> subCategoryList, BuildContext context) {
  return GridView.count(
      crossAxisCount: 2,
      children: List.generate(subCategoryList.length, (index) {
        return Card(
          child: GestureDetector(
            onTap: () {
              Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (context) => CategoriesDetail(
                      subCategoryList[index].id,
                      subCategoryList[index].name),
                ),
              );
//              var bar = subCategoryList[index].subCategory.cast<SubCategory>();
//              subCategoryList.length != 0
//                  ? Navigator.push(
//                      context,
//                      MaterialPageRoute(
//                        builder: (context) =>
//                            CategoryItem(bar, subCategoryList[index].name),
//                      ),
//                    )
//                  :
            },
            child: Stack(
              children: <Widget>[
                Center(
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(0.0, 0.0, 0.0, 22.0),
                    child: Image.network(
                        subCategoryList[index].img != null
                            ? subCategoryList[index].img
                            : Icon(
                                Icons.favorite,
                                color: Colors.red,
                                size: 30.0,
                              ),
                        fit: BoxFit.fitHeight),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Align(
                    alignment: Alignment.bottomCenter,
                    child: Text(
                      subCategoryList[index].name,
                      style: TextStyle(fontSize: 14, color: Colors.black),
                    ),
                  ),
                ),
              ],
            ),
          ),
        );
      }));
}
