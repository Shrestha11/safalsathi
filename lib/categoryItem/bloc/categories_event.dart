import 'package:flutter/material.dart';
import 'package:meta/meta.dart';

@immutable
abstract class CategoriesEvent {}

class LoadSearchEvent extends CategoriesEvent {
  final int categoryId;

  LoadSearchEvent(this.categoryId);

  @override
  String toString() => 'Load Search Event';
}

class LoadNextPage extends CategoriesEvent {
  final int nextPage;

  LoadNextPage(this.nextPage);

  @override
  String toString() => 'Load Next Page';
}

class SortDialogClick extends CategoriesEvent {
  @override
  String toString() => 'Sort Click';
}

class SortSelected extends CategoriesEvent {
  final int id;

  SortSelected(this.id);

  @override
  String toString() => 'Sort Selected';
}

class FilterDialogClick extends CategoriesEvent {
  @override
  String toString() => 'FilterDialogClick';
}

class FilterDialogSelected extends CategoriesEvent {
  @override
  String toString() => 'Filter Dialog Selected';
}

class BrandSelected extends CategoriesEvent {
  final int position;

  BrandSelected(this.position);

  @override
  String toString() => 'Brand Selected';
}

class BrandDeselected extends CategoriesEvent {
  final int position;

  BrandDeselected(this.position);

  @override
  String toString() => 'Brand Deselected';
}

class ColorSelected extends CategoriesEvent {
  final int position;

  ColorSelected(this.position);

  @override
  String toString() => 'Color Selected';
}

class ColorDeselected extends CategoriesEvent {
  final int position;

  ColorDeselected(this.position);

  @override
  String toString() => 'Color Deselected';
}

class PriceChanged extends CategoriesEvent {
  final RangeValues rangeValues;

  PriceChanged(this.rangeValues);

  @override
  String toString() => 'Price Changed';
}

class SizeSelected extends CategoriesEvent {
  final int position;

  SizeSelected(this.position);

  @override
  String toString() => 'Size Selected';
}

class SizeDeselected extends CategoriesEvent {
  final int position;

  SizeDeselected(this.position);

  @override
  String toString() => 'Size Deselceted';
}

class FilterApply extends CategoriesEvent {
  @override
  String toString() => 'Filter Apply';
}
