import 'package:flutter/material.dart';
//import 'package:onesignal_flutter/onesignal_flutter.dart';

//import 'loading_screen/loading_screen.dart';
import 'package:safalsathi/loading_screen/loading_screen.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    /*OneSignal.shared.init("a12c1e7a-8260-4005-887b-e9df809b85e2", iOSSettings: {
      OSiOSSettings.autoPrompt: false,
      OSiOSSettings.inAppLaunchUrl: true
    });
//    setNotificationOnOff();
    OneSignal.shared
        .setInFocusDisplayType(OSNotificationDisplayType.notification);

    OneSignal.shared
        .setNotificationReceivedHandler((OSNotification notification) {
      // will be called whenever a notification is received
    });

    OneSignal.shared
        .setNotificationOpenedHandler((OSNotificationOpenedResult result) {
      // will be called whenever a notification is opened/button pressed.
    });

    OneSignal.shared.setPermissionObserver((OSPermissionStateChanges changes) {
      // will be called whenever the permission changes
      // (ie. user taps Allow on the permission prompt in iOS)
    });

    OneSignal.shared
        .setSubscriptionObserver((OSSubscriptionStateChanges changes) {
      // will be called whenever the subscription changes
      //(ie. user gets registered with OneSignal and gets a user ID)
    });

    OneSignal.shared.setEmailSubscriptionObserver(
        (OSEmailSubscriptionStateChanges emailChanges) {
      // will be called whenever then user's email subscription changes
      // (ie. OneSignal.setEmail(email) is called and the user gets registered
    });

// For each of the above functions, you can also pass in a
// reference to a function as well:

    void _handleNotificationReceived(OSNotification notification) {}

    void main() {
      OneSignal.shared
          .setNotificationReceivedHandler(_handleNotificationReceived);
    }*/

//    return MaterialApp(home: Bargain(1, "NEXT NEPAL"));
//    return MaterialApp(home: LoginRegister());
    return MaterialApp(home: LoadingScreen());
  }
}
